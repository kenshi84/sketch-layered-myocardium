#pragma once

#include "State.h"
#include "Core.h"

class StateSetS1 : public State
{
	static const double COLOR_POINTS [3];
	
	StateSetS1 (void) {}
	~StateSetS1 (void) {}
public:
	static StateSetS1* getInstance() {
		static StateSetS1 p;
		return &p;
	}

	// interface of [State] begin
	State* next();
	bool isReady() {
		return true;
	}
	void init();
	void draw();
	void postDraw(CWnd* hWnd, CDC* pDC);
	void OnLButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnMouseMove  (CView* view, UINT nFlags, CPoint& point);
	void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags);
	bool load(const std::string& fname);
	void undo();
	bool isUndoable();
	void save();
	bool isSavable();
	// end

protected:
	bool m_isCutting;
	bool m_isPutting;
	CPoint m_pointOld;
	bool m_isDrawFace;
};
