#include <limits>
#include <ostream>
#include <utility>

template <typename Value>
struct MinSelector {
    double score = std::numeric_limits<double>::max();
    double total_score = 0;
    size_t count = 0;
    Value value;
    MinSelector(const Value& init_value = Value())
        : value(init_value)
    {}
    bool update(double scoreNew, const Value& valueNew) {
        total_score += (double)scoreNew;
        ++count;
        if (scoreNew < score) {
            score = scoreNew;
            value = valueNew;
            return true;
        }
        return false;
    }
    double average_score() const { return total_score / count; }
};
