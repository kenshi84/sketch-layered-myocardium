#pragma once

#include "Drawer.h"
#include "EventHandler.h"
#include "State.h"

#include <KLIB/KOGL.h>
#include <KLIB/KGeometry.h>
#include <KLIB/KRBF.h>

#include <vector>
#include <map>

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>
#include <igl/min_quad_with_fixed.h>

using MatrixXd = Eigen::MatrixXd;
using MatrixXi = Eigen::MatrixXi;
using Matrix3d = Eigen::Matrix3d;
using Vector3d = Eigen::Vector3d;
using Vector2d = Eigen::Vector2d;
using Vector5d = Eigen::Matrix<double, 5, 1>;
using VectorXi = Eigen::VectorXi;
using SparseMatrixd = Eigen::SparseMatrix<double>;
using EigenSolver3d = Eigen::EigenSolver<Matrix3d>;
using RowVector3d = Eigen::RowVector3d;

template <typename TVector3_Out, typename TVector3_In>
inline TVector3_Out vector3_cast(const TVector3_In& v) {
	return TVector3_Out{ v[0], v[1], v[2] };
}

// Convert 5-vector to 3x3 traceless symmetric tensor
inline Matrix3d vector_to_tensor(const Vector5d& v) {
	double t00 = v[0];
	double t01 = v[1];
	double t02 = v[2];
	double t11 = v[3];
	double t12 = v[4];
	double t22 = -(t00 + t11);
	Matrix3d t;
	t <<
		t00, t01, t02,
		t01, t11, t12,
		t02, t12, t22;
	return t;
}

// Convert 3x3 traceless symmetric tensor to 5-vector
inline Vector5d tensor_to_vector(const Matrix3d& t) {
	VERIFY(std::abs(t.trace()) < 1.e-8);
	Vector5d v;
	v[0] = t(0, 0);
	v[1] = t(0, 1);
	v[2] = t(0, 2);
	v[3] = t(1, 1);
	v[4] = t(1, 2);
	return v;
}

inline Matrix3d get_eigenvectors(const Matrix3d& t) {
	EigenSolver3d eig_solver(t);

	const Vector3d eig_values_imag = eig_solver.eigenvalues().imag();
	const Vector3d eig_values_real = eig_solver.eigenvalues().real();
	VERIFY((eig_values_imag.array().abs() < 1.e-8).all());            // Eigenvalues should be real
	VERIFY(std::abs(eig_values_real.sum()) < 1.e-8);                  // Eigenvalues should sum to zero

	const Matrix3d eig_vectors_imag = eig_solver.eigenvectors().imag();
	const Matrix3d eig_vectors_real = eig_solver.eigenvectors().real();
	VERIFY((eig_vectors_imag.array().abs() < 1.e-8).all());           // Eigenvectors should be real

	std::map<double, Vector3d> eig_vectors_sorted;
	for (int i = 0; i < 3; ++i) {
		eig_vectors_sorted[eig_values_real[i]] = eig_vectors_real.col(i);
	}

	return (Matrix3d() <<
			eig_vectors_sorted.rbegin()->second,
			(++eig_vectors_sorted.rbegin())->second,
			(++++eig_vectors_sorted.rbegin())->second
		).finished();
}

inline Vector3d get_major_eigenvector(const Matrix3d& t) { return get_eigenvectors(t).col(0); }
inline Vector3d get_medium_eigenvector(const Matrix3d& t) { return get_eigenvectors(t).col(1); }
inline Vector3d get_minor_eigenvector(const Matrix3d& t) { return get_eigenvectors(t).col(2); }

class CMainFrame;

class Core
{
public:
	static const double GRADIENT_DELTA;
	static const double LAMBDA_EUCLID;
	static const double LAMBDA_DEPTH;
	static int VOL_SIZE;
	static const int LAYER_MAX;

protected:
	Core(void);
	~Core(void) {}
public:
	static Core* getInstance() {
		static Core p;
		return &p;
	}
	
	KOGL m_ogl;
	Drawer m_drawer;
	EventHandler m_handler;
	State* m_state;
	
	CMainFrame* p_mainFrm;
	
	enum ViewMode {
		VIEWMODE_CUT,
		VIEWMODE_LAYER
	} m_viewMode;
	
	// tetra model of the heart
	KTetraModel m_tetra;
	CString m_fname;
	/////////////////////////////
	//                         //
	// set by [StateLoadTetra] //
	//                         //
	/////////////////////////////
	
	// polygon model for displaying
	KPolygonModel m_poly, m_polyContour;
	void initPoly();
	void initPolyContour();
	// cutting
	std::vector<KVector3d> m_cutStroke;
	void calcPoly(const std::vector<KVector3d>& cutStroke);
	
	// tetra ID to which each voxel belongs
	std::vector<int>       m_volTetID;
	void calcVolTetID();
	
	// boundary tetra vertices
	std::vector<bool> m_isTetVtxBoundary;
	std::vector<KVector3d> m_tetVtxBoundaryNormal;
	void calcTetVtxBoundary();
	
	// pairs of point and value for depth field
	std::vector<KVector3d> m_rbfPoints;
	std::vector<double>    m_rbfValues;
	////////////////////////////
	//                        //
	// set by [StateSetDepth] //
	//                        //
	////////////////////////////
	
	// depth field represented as 3D-RBF
	KThinPlate3D m_rbfDepth;
	void calcRbfDepth();
	
	// depth field over the tetra vtx
	std::vector<double> m_vtxDepth;
	void calcVtxDepth();
	
	// layers (iso-surface of the depth field)
	std::vector<KPolygonModel> m_polyLayers;
	int m_currentLayer;
	void calcPolyLayers();
	
	// depth value assigned to each polygon vertex (for 1-D texturing)
	std::vector<double> m_polyVtxDepth;
	std::vector<std::vector<double> > m_polyLayersVtxDepth;
	void calcPolyVtxDepth();
	void calcPolyLayersVtxDepth();
	
	// depth value assigned to each voxel
	std::vector<double>    m_volDepth;
	void calcVolDepth();
	
	// Laplacian smoothing object (considering depth)
	SparseMatrixd m_cotmatrix;
	SparseMatrixd m_bilaplacian;
	void calcLaplacian();
	
	// strokes to specify vector field
	std::vector<std::vector<KVector3d> > m_strokes;			// Unused! Obsoleted by m_tetVtxConstraintDirection
	//////////////////////////////
	//                          //
	// set by [StateDrawStroke] //
	//                          //
	//////////////////////////////
	
	std::vector<std::pair<bool, Vector3d>> m_tetVtxConstraintDirection;		// true if constrained
	igl::min_quad_with_fixed_data<double> m_solver_data;

	// traceless symmetric tensor representing fiber orientation
	MatrixXd m_vtxFiber;
	void calcVtxFiber();
	
	// fiber orientation assigned to each voxel
	std::vector<Matrix3d> m_volFiber;						// 3 (column) major, medium, minor eigenvectors
	void calcVolFiber();
	
	// streamlines that represent fiber orientation
	bool m_showStreamLines;
	std::vector<std::vector<std::pair<KVector3d, double> > > m_streamLines;
	std::vector<std::vector<std::pair<KVector3d, double> > > m_streamLinesPoly;
	std::vector<std::vector<std::vector<std::pair<KVector3d, double> > > > m_streamLinesPolyLayers;
	unsigned int lineType = 2;	// 0:normal, 1:binormal, 2:fiber
	void calcStreamLines();
	void initStreamLinesPoly();
	void calcStreamLinesPoly(const std::vector<KVector3d>& cutStroke);
	void calcStreamLinesPolyLayers();
	
	// s1 points for the heart
	std::vector<KVector3d> m_s1Points;
	//////////////////////////////
	//                          //
	// set by [StateSetS1] //
	//                          //
	//////////////////////////////
	
	// parameters for FFD
	KVector3d m_deformOrigin;
	double    m_deformRadius;
	KVector3d m_deformOffset;
	///////////////////////////////
	//                           //
	// set by [StateDeformTetra] //
	//                           //
	///////////////////////////////
	
	std::vector<int>       m_deformVtxList;
	std::vector<double>    m_deformVtxWeight;
	std::vector<KVector3d> m_deformVtxOffset;
	void calcDeformVtxList  (const KVector3d& origin, double radius);
	void calcDeformVtxOffset(const KVector3d& offset);
	
	// tetra deformation (and corresponding transformation of depth & stroke)
	void deformTetra(const std::vector<KVector3d>& vtxPosNew);
	KVector3d transform(
		const KTetraModel& tetraOld,
		const KTetraModel& tetraNew,
		const KVector3d& pos);
	
	// export volume fiber data
	void exportVolData();
	
	void initEyePosition();

	static double i2d(int i) {
		const int& N = VOL_SIZE;
		if (i < 0 || i >= N) return -1.0;
		return (i + 0.5) / N;
	}
	static int d2i(double d) {
		const int& N = VOL_SIZE;
		if (d < 0 || d > 1) return -1;
		int i = (int)(d * N);
		if (i == N) --i;
		return i;
	}
	static int d2i_ceil(double d) {		// smallest cell index whose center is above d
		int i = d2i(d);
		if (i < 0) return -1;
		double d2 = i2d(i);
		if (d2 < d) ++i;
		return i;
	}
	static int d2i_floor(double d) {	// largest cell index whose center is below d
		int i = d2i(d);
		if (i < 0) return -1;
		double d2 = i2d(i);
		if (d2 > d) --i;
		return i;
	}
	static int getIndex(int ix, int iy, int iz) {
		const int& N = VOL_SIZE;
		if (ix < 0 || ix >= N) return -1;
		if (iy < 0 || iy >= N) return -1;
		if (iz < 0 || iz >= N) return -1;
		return ix + N * iy + N * N * iz;
	}
};
