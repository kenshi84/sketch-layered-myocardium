#include "StdAfx.h"
#include "KSparseVector.h"

#include <algorithm>

using namespace std;

void KSparseVector::init(int size, int initDataSize) {
	m_size = size;
	m_Ai.clear();
	m_Ax.clear();
	m_Ai.reserve(initDataSize);
	m_Ax.reserve(initDataSize);
}
double KSparseVector::getValue(int index) const {
	if (index >= m_size) throw "index out of bounds";
	typedef vector<int>::const_iterator iterator;
	iterator iterFirst = m_Ai.begin();
	iterator iterLast  = m_Ai.end();
	pair<iterator, iterator> range = equal_range(iterFirst, iterLast, index);
	if (range.first == range.second) return 0.;
	vector<double>::const_iterator iterVal = m_Ax.begin();
	advance(iterVal, (int)distance(m_Ai.begin(), range.first));
	return *iterVal;
}
void   KSparseVector::setValue(int index, double val) {
	if (index >= m_size) throw "index out of bounds";
	typedef vector<int>::iterator iterator;
	iterator iterFirst = m_Ai.begin();
	iterator iterLast  = m_Ai.end();
	pair<iterator, iterator> range = equal_range(iterFirst, iterLast, index);
	vector<double>::iterator iterVal = m_Ax.begin();
	advance(iterVal, (int)distance(m_Ai.begin(), range.first));
	if (range.first == range.second) {
		if (val == 0) return;
		m_Ai.insert(range.first, index);
		m_Ax.insert(iterVal, val);
	} else {
		if (val == 0) {
			m_Ai.erase(range.first);
			m_Ax.erase(iterVal);
		} else {
			*iterVal = val;
		}
	}
}
void   KSparseVector::addValue(int index, double val) {
	if (index >= m_size) throw "index out of bounds";
	if (val == 0) return;
	typedef vector<int>::iterator iterator;
	iterator iterFirst = m_Ai.begin();
	iterator iterLast  = m_Ai.end();
	pair<iterator, iterator> range = equal_range(iterFirst, iterLast, index);
	vector<double>::iterator iterVal = m_Ax.begin();
	advance(iterVal, (int)distance(m_Ai.begin(), range.first));
	if (range.first == range.second) {
		m_Ai.insert(range.first, index);
		m_Ax.insert(iterVal, val);
	} else {
		*iterVal += val;
	}
}
