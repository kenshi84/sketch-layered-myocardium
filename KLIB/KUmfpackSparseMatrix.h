#pragma once

#include <vector>
#include <iostream>

class KUmfpackSparseMatrix {
	int m_rowSize;
	int m_colSize;
	std::vector<int>    m_Ap;
	std::vector<int>    m_Ai;
	std::vector<double> m_Ax;
public:
	KUmfpackSparseMatrix();
	KUmfpackSparseMatrix(int rowSize, int colSize, int initDataSize = 128);
	void init(int rowSize, int colSize, int initDataSize = 128);
	int getRowSize() const { return m_rowSize; }
	int getColSize() const { return m_colSize; }
	std::vector<int>    getAp() const { return m_Ap; }
	std::vector<int>    getAi() const { return m_Ai; }
	std::vector<double> getAx() const { return m_Ax; }
	double getValue(int row, int col) const;
	void   setValue(int row, int col, double val);
	void   addValue(int row, int col, double val);
	std::vector<double>  mul(const std::vector<double>& vec)  const;
	KUmfpackSparseMatrix mul(const KUmfpackSparseMatrix& mat) const;
	KUmfpackSparseMatrix transpose() const;
	KUmfpackSparseMatrix transposedMultipliedBySelf() const;
	KUmfpackSparseMatrix selfMultipliedByTransposed() const;
};

std::ostream& operator<<(std::ostream& os, const KUmfpackSparseMatrix& mat);
