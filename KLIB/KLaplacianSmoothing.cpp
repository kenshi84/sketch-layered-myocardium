#include "StdAfx.h"
#include "KLaplacianSmoothing.h"

using namespace std;

void KLaplacianSmoothing::setC(const vector<KSparseVector>& C) {
	KUmfpackSparseMatrix tmpAtA = m_AtA;
	m_C = C;
	for (int i = 0; i < (int)m_C.size(); ++i) {
		const KSparseVector& c = C[i];
		if (c.getSize() != m_AtA.getRowSize()) throw "incompatible sizes";
		vector<int>&    Ai = c.getAi();
		vector<double>& Ax = c.getAx();
		for (int j = 0; j < (int)Ai.size(); ++j) {
			int    i0 = Ai[j];
			double x0 = Ax[j];
			tmpAtA.addValue(i0, i0, x0 * x0);
			for (int k = j + 1; k < (int)Ai.size(); ++k) {
				int    i1 = Ai[k];
				double x01 = x0 * Ax[k];
				tmpAtA.addValue(i0, i1, x01);
				tmpAtA.addValue(i1, i0, x01);
			}
		}
	}
	//vector<int>&    Ap = m_AtA.getAp();
	//vector<int>&    Ai = m_AtA.getAi();
	//vector<double>& Ax = m_AtA.getAx();
	m_Ap = tmpAtA.getAp();
	m_Ai = tmpAtA.getAi();
	m_Ax = tmpAtA.getAx();
	p_umfpack = new KUmfpack(m_AtA.getRowSize(), m_Ap, m_Ai, m_Ax);
}

void KLaplacianSmoothing::solve(std::vector<double>& x, const std::vector<double>& b) const {
	if (m_C.size() != b.size()) throw "incompatible sizes";
	vector<double> B(m_AtA.getRowSize(), 0);
	for (int i = 0; i < (int)m_C.size(); ++i) {
		const KSparseVector& c = m_C[i];
		vector<int>&    Ai = c.getAi();
		vector<double>& Ax = c.getAx();
		for (int j = 0; j < (int)Ai.size(); ++j) {
			int    k = Ai[j];
			double x = Ax[j];
			B[k] += b[i] * x;
		}
	}
	x.resize(m_AtA.getRowSize());
	p_umfpack->solve(x, B);
}
