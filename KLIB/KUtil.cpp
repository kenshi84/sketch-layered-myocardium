#include "StdAfx.h"
#include ".\kutil.h"

#include <cfloat>
#include <algorithm>

using namespace std;

void KUtil::getLineIntersection(const KVector2d& start0, const KVector2d& end0, const KVector2d& start1, const KVector2d& end1, 
								double& t_start0, double& t_end0, double& t_start1, double& t_end1)
{
	// fast version
	/*
		t_start0 * start0 + (1 - t_start0) * end0 = t_start1 * start1 + (1 - t_start1) * end1;
		| start0.x - end0.x  -start1.x + end1.x || t_start0 | = | -end0.x + end1.x |
		| start0.y - end0.y  -start1.y + end1.y || t_start1 | = | -end0.y + end1.y |
	*/
	KMatrix2d A(
		start0.x - end0.x, -start1.x + end1.x,
		start0.y - end0.y, -start1.y + end1.y);
	A.invert();
	KVector2d& result = A.mul(KVector2d(-end0.x + end1.x, -end0.y + end1.y));
	t_start0 = result.x;
	t_end0   = 1 - t_start0;
	t_start1 = result.y;
	t_end1   = 1 - t_start1;
	
	// slow version
	///*
	//	t_start0 * start0 + t_end0 * end0 = t_start1 * start1 + t_end1 * end1;
	//	t_start0 + t_end0 = 1;
	//	t_start1 + t_end1 = 1;
	//	<-->
	//	| start0.x  end0.x  -start1.x  -end1.x || t_start0 |   | 0 |
	//	| start0.y  end0.y  -start1.y  -end1.y || t_end0   | = | 0 |
	//	|   1         1         0          0   || t_start1 |   | 1 |
	//	|   0         0         1          1   || t_end1   |   | 1 |
	//*/
	//KMatrix4d A(
	//	start0.x, end0.x, -start1.x, -end1.x, 
	//	start0.y, end0.y, -start1.y, -end1.y, 
	//	       1,      1,         0,       0,
	//	       0,      0,         1,       1);
	//A.invert();
	//KVector4d& result = A.transform(KVector4d(0, 0, 1, 1));
	//t_start0 = result.x;
	//t_end0   = result.y;
	//t_start1 = result.z;
	//t_end1   = result.w;
}

void KUtil::normalizeTetraModelIntoUnitBox(KTetraModel& tetra, double margin_percent) {
	double xmin, ymin, zmin, xmax, ymax, zmax;
	getBoundingBox(tetra, xmin, ymin, zmin, xmax, ymax, zmax);
	KVector3d c(
		(xmin + xmax) * 0.5,
		(ymin + ymax) * 0.5,
		(zmin + zmax) * 0.5);
	double s = max(max(xmax - xmin, ymax - ymin), zmax - zmin);
	s *= 1.0 + margin_percent * 0.01;
	s = 1 / s;
	KVector3d o(0.5, 0.5, 0.5);
	for (int i = 0; i < (int)tetra.m_vertices.size(); ++i) {
		KVector3d& pos = tetra.m_vertices[i].m_pos;
		pos.sub(c);
		pos.scale(s);
		pos.add(o);
	}
}

void KUtil::normalizeTetraModelIntoUnitBox(KMultiTexTetraModel& tetra) {
	double xmin, ymin, zmin, xmax, ymax, zmax;
	getBoundingBox(tetra, xmin, ymin, zmin, xmax, ymax, zmax);
	KVector3d c(
		(xmin + xmax) * 0.5,
		(ymin + ymax) * 0.5,
		(zmin + zmax) * 0.5);
	double s = max(max(xmax - xmin, ymax - ymin), zmax - zmin);
	s = 1 / s;
	KVector3d o(0.5, 0.5, 0.5);
	for (int i = 0; i < (int)tetra.m_vertices.size(); ++i) {
		KVector3d& pos = tetra.m_vertices[i].m_pos;
		pos.sub(c);
		pos.scale(s);
		pos.add(o);
	}
}

bool KUtil::getIntersection(KVector3d& result, int& polyID, const KPolygonModel& poly, const KVector3d& start, const KVector3d& ori) {
	double distMin = DBL_MAX;
	polyID = -1;
	bool info = false;
	for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
		const KPolygon& p = poly.m_polygons[i];
		const KVector3d& v0 = poly.m_vertices[p.m_vtx[0]].m_pos;
		const KVector3d& v1 = poly.m_vertices[p.m_vtx[1]].m_pos;
		const KVector3d& v2 = poly.m_vertices[p.m_vtx[2]].m_pos;
		KVector3d& normal = KUtil::calcNormal(v0, v1, v2);
		if (normal.dot(ori) >= 0) continue;
		KMatrix4d mat(
			v0.x, v1.x, v2.x, -ori.x,
			v0.y, v1.y, v2.y, -ori.y,
			v0.z, v1.z, v2.z, -ori.z,
			1,    1,    1,    0);
		mat.invert();
		double b[4], x[4];
		b[0] = start.x;
		b[1] = start.y;
		b[2] = start.z;
		b[3] = 1;
		mat.mul(x, b);
		if (x[0] < 0 || x[1] < 0 || x[2] < 0) continue;
		if (x[3] < distMin) {
			distMin = x[3];
			result.set(start);
			result.addWeighted(ori, x[3]);
			polyID = i;
			info = true;
		}
	}
	return info;
}

bool KUtil::getIntersection(KVector3d& result, int& polyID, const KMultiTexPolygonModel& poly, const KVector3d& start, const KVector3d& ori) {
	double distMin = DBL_MAX;
	polyID = -1;
	bool info = false;
	for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
		const KMultiTexPolygon& p = poly.m_polygons[i];
		const KVector3d& v0 = poly.m_vertices[p.m_vtx[0]].m_pos;
		const KVector3d& v1 = poly.m_vertices[p.m_vtx[1]].m_pos;
		const KVector3d& v2 = poly.m_vertices[p.m_vtx[2]].m_pos;
		KVector3d& normal = KUtil::calcNormal(v0, v1, v2);
		if (normal.dot(ori) >= 0) continue;
		KMatrix4d mat(
			v0.x, v1.x, v2.x, -ori.x,
			v0.y, v1.y, v2.y, -ori.y,
			v0.z, v1.z, v2.z, -ori.z,
			1,    1,    1,    0);
		mat.invert();
		double b[4], x[4];
		b[0] = start.x;
		b[1] = start.y;
		b[2] = start.z;
		b[3] = 1;
		mat.mul(x, b);
		if (x[0] < 0 || x[1] < 0 || x[2] < 0) continue;
		if (x[3] < distMin) {
			distMin = x[3];
			result.set(start);
			result.addWeighted(ori, x[3]);
			polyID = i;
			info = true;
		}
	}
	return info;
}
void KUtil::getBarycentricCoord(
	double& d0, double& d1, double& d2,
	const KVector3d& v0, const KVector3d& v1, const KVector3d& v2,
	const KVector3d& w,
	const double coord_sum
) {
	/* 
	 * [input]
	 *   v0, v1, v2: 3 face vtx
	 *   w: target vector
	 * [output]
	 *   d0, d1, d2: barycentric coordinate (least-square)
	 *-----------------------------------------------
	 * d0 * v0 + d1 * v1 + d2 *v2 = w
	 * d0      + d1      + d2     = coord_sum
	 | v0 v1 v2 |   | d0 |   | w         |
	 | 1  1  1  | * | d1 | = | coord_sum |
                    | d2 |   
	 * 
     * v00 = |v0|^2 , v11 = |v1|^2 , v22 = |v2|^2
	 * v01 = v0 * v1, v12 = v1 * v2, v20 = v2 * v0
	 * v0w = v0 * w , v1w = v1 * w , v2w = v2 * w
	 * 
	 * | d0 |   | v00 + 1, v01 + 1, v20 + 1 |^-1   | v0w + coord_sum |
	 * | d1 | = | v01 + 1, v11 + 1, v12 + 1 |    * | v1w + coord_sum |
	 * | d2 |   | v20 + 1, v12 + 1, v22 + 1 |      | v2w + coord_sum |
	 * 
	 */
	double v00 = v0.lengthSquared();
	double v11 = v1.lengthSquared();
	double v22 = v2.lengthSquared();
	double v01 = v0.dot(v1);
	double v12 = v1.dot(v2);
	double v20 = v2.dot(v0);
	double v0w = v0.dot(w);
	double v1w = v1.dot(w);
	double v2w = v2.dot(w);
	KMatrix3d mat(
		v00 + 1, v01 + 1, v20 + 1, 
		v01 + 1, v11 + 1, v12 + 1, 
		v20 + 1, v12 + 1, v22 + 1
	);
	mat.invert();
	double x[3];
	double b[3] = {v0w + coord_sum, v1w + coord_sum, v2w + coord_sum};
	mat.mul(x, b);
	d0 = x[0];
	d1 = x[1];
	d2 = x[2];
}

double KUtil::getCurveLength(const vector<KVector2d>& curve) {
	double length = 0;
	for (size_t i = 1; i < curve.size(); ++i) {
		length += getDistance(curve[i - 1], curve[i]);
	}
	return length;
}

vector<KVector2d> KUtil::resampleCurve(const vector<KVector2d>& src_curve, bool is_loop) {
	return resampleCurve(src_curve, src_curve.size(), is_loop);
	//const int N = (int)src_curve.size();
	//std::vector<KVector2d> dst_curve = src_curve;
	//dst_curve.push_back(dst_curve.front());
	//std::vector<double> src_len_acc(N, 0);
	//for (int i = 1; i < N; ++i) {
	//	KVector2d dif = src_curve[i];
	//	dif.sub(src_curve[i - 1]);
	//	src_len_acc[i] = src_len_acc[i - 1] + dif.length();
	//}
	//double dst_len_segment = src_len_acc.back() / (N - 1.);
	//int src_i = 0;
	//int dst_i = 0;
	//double dst_len_acc = 0;
	//while (true) {
	//	while (dst_len_acc + dst_len_segment <= src_len_acc[src_i]) {
	//		dst_len_acc += dst_len_segment;
	//		++dst_i;
	//		printf("dst_i = %d\n", dst_i);
	//		double w1 = (dst_len_acc - src_len_acc[src_i - 1]) / (double)(src_len_acc[src_i] - src_len_acc[src_i - 1]);
	//		double w0 = 1 - w1;
	//		KVector2d p0 = src_curve[src_i - 1];
	//		KVector2d p1 = src_curve[src_i];
	//		p0.scale(w0);
	//		p1.scale(w1);
	//		dst_curve[dst_i] = p0;
	//		dst_curve[dst_i].add(p1);
	//		if (dst_i == N - 2) break;
	//	}
	//	if (dst_i == N - 2) break;
	//	while (src_len_acc[src_i] <= dst_len_acc + dst_len_segment) {
	//		++src_i;
	//		printf("src_i = %d\n", src_i);
	//	}
	//}
	//dst_curve.pop_back();
	//return dst_curve;
}

vector<KVector2d> KUtil::resampleCurve(const vector<KVector2d>& src_curve, unsigned int num_tgt_vtx, bool is_loop) {
	if (src_curve.size() < 3) return src_curve;
	std::vector<KVector2d> src_curve2 = src_curve;
	if (is_loop) {
		src_curve2.push_back(src_curve.front());
		++num_tgt_vtx;
	}
	const int N = (int)src_curve2.size();
	std::vector<KVector2d> dst_curve;
	std::vector<double> src_len_acc(N, 0);
	for (int i = 1; i < N; ++i) {
		KVector2d dif = src_curve2[i];
		dif.sub(src_curve2[i - 1]);
		src_len_acc[i] = src_len_acc[i - 1] + dif.length();
	}
	double dst_len_segment = src_len_acc.back() / (num_tgt_vtx - 1.);
	int src_i = 0;
	double dst_len_acc = 0;
	dst_curve.push_back(src_curve2.front());
	while (true) {
		while (dst_len_acc + dst_len_segment <= src_len_acc[src_i]) {
			dst_len_acc += dst_len_segment;
			double w1 = (dst_len_acc - src_len_acc[src_i - 1]) / (double)(src_len_acc[src_i] - src_len_acc[src_i - 1]);
			double w0 = 1 - w1;
			KVector2d p0 = src_curve2[src_i - 1];
			KVector2d p1 = src_curve2[src_i];
			p0.scale(w0);
			p0.addWeighted(p1, w1);
			dst_curve.push_back(p0);
			if (dst_curve.size() == num_tgt_vtx - 1) break;
		}
		if (dst_curve.size() == num_tgt_vtx - 1) break;
		while (src_len_acc[src_i] <= dst_len_acc + dst_len_segment) {
			++src_i;
		}
	}
	if (!is_loop) dst_curve.push_back(src_curve2.back());
	return dst_curve;
}
vector<KVector2d> KUtil::reverseCurve(const vector<KVector2d>& curve, bool is_loop) {
	vector<KVector2d> result(curve);
	if (!is_loop) {
		reverse(result.begin(), result.end());
	} else {
		for (int i = 1; i < curve.size(); ++i) {
			result[i] = curve[curve.size() - i];
		}
	}
	return result;
}
