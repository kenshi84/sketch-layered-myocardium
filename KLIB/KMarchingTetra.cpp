#include "StdAfx.h"
#include "KMarchingTetra.h"

using namespace std;

KPolygonModel KMarchingTetra::calcMarchingTetra(const KTetraModel& tetra, const std::vector<double>& vtxValue, const double threshold, vector<int>& polygons_oncutplane) {
	polygons_oncutplane.clear();
	static const int mapping_pattern[][4] = {
		{-1, -1, -1, -1},	//0
		{0, 1, 2, 3},	//1
		{1, 2, 0, 3},	//2
		{0, 1, 2, 3},	//3
		{2, 0, 1, 3},	//4
		{0, 2, 3, 1},	//5
		{1, 2, 0, 3},	//6
		{3, 2, 1, 0},	//7
		{3, 2, 1, 0},	//8
		{0, 3, 1, 2},	//9
		{1, 3, 2, 0},	//10
		{2, 0, 1, 3},	//11
		{2, 3, 0, 1},	//12
		{1, 2, 0, 3},	//13
		{0, 1, 2, 3},	//14
		{0, 1, 2, 3}	//15
	};
	
	vector<KVertex>  vertices;
	vector<KPolygon> polygons;
	map<int, int> vtxMap;
	
	int vtxSize = (int)tetra.m_vertices.size();
	int tetSize = (int)tetra.m_tetras.size();
	for (int i = 0; i < tetSize; ++i) {
		const KTetra& tet = tetra.m_tetras[i];
		int code = 0;
		double val[4];
		for (int j = 0; j < 4; ++j) {
			val[j] = vtxValue[tet.m_vtx[j]];
			if (val[j] < threshold)
				code += (1 << j);
		}
		if (code == 0) continue;
		const int* mapping = mapping_pattern[code];
		KVector3d vPos[4];
		int       vKey[4];
		for (int j = 0; j < 4; ++j) {
			int index = tet.m_vtx[mapping[j]];
			const KVector3d& pos = tetra.m_vertices[index].m_pos;
			vPos[j] = pos;
			vKey[j] = vtxSize * index + index;
		}
		if (code == 15) {
			for (int j = 0; j < 4; ++j) {
				if (tet.m_neighbor[j] != -1) continue;
				const int& index0 = KTetraModel::FACE_VTX_ORDER[j][0];
				const int& index1 = KTetraModel::FACE_VTX_ORDER[j][1];
				const int& index2 = KTetraModel::FACE_VTX_ORDER[j][2];
				addPolygon(
					vertices, polygons, vtxMap,
					vPos[index0], vPos[index1], vPos[index2],
					vKey[index0], vKey[index1], vKey[index2]); 
			}
		} else {
			switch (code) {
				case 1:
				case 2:
				case 4:
				case 8:
					{
						KVector3d wPos[3];
						int       wKey[3];
						for (int j = 0; j < 3; ++j) {
							double u = (threshold - val[mapping[0]]) / (val[mapping[j + 1]] - val[mapping[0]]);
							wPos[j] = vPos[0];
							wPos[j].scale(1 - u);
							wPos[j].addWeighted(vPos[j + 1], u);
							int index0 = min(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							int index1 = max(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							wKey[j] = vtxSize * index0 + index1;
						}
						polygons_oncutplane.push_back(polygons.size());
						addPolygon(
							vertices, polygons, vtxMap,
							wPos[0], wPos[1], wPos[2],
							wKey[0], wKey[1], wKey[2]);
						for (int j = 1; j <= 3; ++j) {
							if (tet.m_neighbor[mapping[j]] != -1) continue;
							addPolygon(
								vertices, polygons, vtxMap,
								wPos[j % 3], vPos[0], wPos[(j + 1) % 3],
								wKey[j % 3], vKey[0], wKey[(j + 1) % 3]);
						}
					}
					break;
				case 3:
				case 5:
				case 6:
				case 9:
				case 10:
				case 12:
					{
						static int pairs[][2] = {{0, 3}, {0, 2}, {1, 2}, {1, 3}};
						KVector3d wPos[4];
						int       wKey[4];
						for (int j = 0; j < 4; ++j) {
							const int& i0 = pairs[j][0];
							const int& i1 = pairs[j][1];
							double u = (threshold - val[mapping[i0]]) / (val[mapping[i1]] - val[mapping[i0]]);
							wPos[j] = vPos[i0];
							wPos[j].scale(1 - u);
							wPos[j].addWeighted(vPos[i1], u);
							int index0 = min(tet.m_vtx[mapping[i0]], tet.m_vtx[mapping[i1]]);
							int index1 = max(tet.m_vtx[mapping[i0]], tet.m_vtx[mapping[i1]]);
							wKey[j] = vtxSize * index0 + index1;
						}
						polygons_oncutplane.push_back(polygons.size());
						addPolygon(
							vertices, polygons, vtxMap,
							wPos[0], wPos[2], wPos[1],
							wKey[0], wKey[2], wKey[1]);
						polygons_oncutplane.push_back(polygons.size());
						addPolygon(
							vertices, polygons, vtxMap,
							wPos[0], wPos[3], wPos[2],
							wKey[0], wKey[3], wKey[2]);
						if (tet.m_neighbor[mapping[0]] == -1)
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[1], wPos[2], wPos[3],
								vKey[1], wKey[2], wKey[3]);
						if (tet.m_neighbor[mapping[1]] == -1)
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[0], wPos[0], wPos[1],
								vKey[0], wKey[0], wKey[1]);
						if (tet.m_neighbor[mapping[2]] == -1) {
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[1], wPos[3], wPos[0],
								vKey[1], wKey[3], wKey[0]);
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[1], wPos[0], vPos[0],
								vKey[1], wKey[0], vKey[0]);
						}
						if (tet.m_neighbor[mapping[3]] == -1) {
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[0], wPos[1], wPos[2],
								vKey[0], wKey[1], wKey[2]);
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[0], wPos[2], vPos[1],
								vKey[0], wKey[2], vKey[1]);
						}
					}
					break;
				case 7:
				case 11:
				case 13:
				case 14:
					{
						KVector3d wPos[3];
						int       wKey[3];
						for (int j = 0; j < 3; ++j) {
							double u = (threshold - val[mapping[0]]) / (val[mapping[j + 1]] - val[mapping[0]]);
							wPos[j] = vPos[0];
							wPos[j].scale(1 - u);
							wPos[j].addWeighted(vPos[j + 1], u);
							int index0 = min(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							int index1 = max(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							wKey[j] = vtxSize * index0 + index1;
						}
						polygons_oncutplane.push_back(polygons.size());
						addPolygon(
							vertices, polygons, vtxMap,
							wPos[2], wPos[1], wPos[0],
							wKey[2], wKey[1], wKey[0]);
						for (int j = 1; j <= 3; ++j) {
							if (tet.m_neighbor[mapping[j]] != -1) continue;
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[j % 3 + 1], wPos[j % 3], wPos[(j + 1) % 3],
								vKey[j % 3 + 1], wKey[j % 3], wKey[(j + 1) % 3]);
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[j % 3 + 1], wPos[(j + 1) % 3], vPos[(j + 1) % 3 + 1],
								vKey[j % 3 + 1], wKey[(j + 1) % 3], vKey[(j + 1) % 3 + 1]);
						}
						if (tet.m_neighbor[mapping[0]] == -1)
							addPolygon(
								vertices, polygons, vtxMap,
								vPos[1], vPos[2], vPos[3],
								vKey[1], vKey[2], vKey[3]);
					}
					break;
			}
		}
	}
	
	KPolygonModel result;
	result.m_vertices = vertices;
	result.m_polygons = polygons;
	
	return result;
}

void KMarchingTetra::addPolygon(
		vector<KVertex>& vertices, vector<KPolygon>& polygons,
		map<int, int>& vtxMap,
		const KVector3d& vPos0, const KVector3d& vPos1, const KVector3d& vPos2, 
		const int&       vKey0, const int&       vKey1, const int&       vKey2
) {
	int vID[3];
	const KVector3d* vPos_p[3] = {&vPos0, &vPos1, &vPos2};
	const int      * vKey_p[3] = {&vKey0, &vKey1, &vKey2};
	
	for (int i = 0; i < 3; ++i) {
		map<int, int>::iterator findPos = vtxMap.find(*vKey_p[i]);
		if (findPos != vtxMap.end()) {
			vID[i] = findPos->second;
		} else {
			vID[i] = (int)vtxMap.size();
			vertices.push_back(KVertex(*vPos_p[i]));
			vtxMap.insert(pair<int, int>(*vKey_p[i], vID[i]));
		}
	}
	polygons.push_back(KPolygon(vID[0], vID[1], vID[2]));
}

KMultiTexPolygonModel KMarchingTetra::calcMultiTexMarchingTetra(vector<int>& mapPolygonToTetra, const KMultiTexTetraModel& tetra, const vector<double>& vtxValue, const double threshold, vector<int>& polygons_oncutplane) {
	polygons_oncutplane.clear();
	mapPolygonToTetra.clear();
	static const int mapping_pattern[][4] = {
		{-1, -1, -1, -1},	//0
		{0, 1, 2, 3},	//1
		{1, 2, 0, 3},	//2
		{0, 1, 2, 3},	//3
		{2, 0, 1, 3},	//4
		{0, 2, 3, 1},	//5
		{1, 2, 0, 3},	//6
		{3, 2, 1, 0},	//7
		{3, 2, 1, 0},	//8
		{0, 3, 1, 2},	//9
		{1, 3, 2, 0},	//10
		{2, 0, 1, 3},	//11
		{2, 3, 0, 1},	//12
		{1, 2, 0, 3},	//13
		{0, 1, 2, 3},	//14
		{0, 1, 2, 3}	//15
	};
	
	vector<KVertex>  vertices;
	vector<KMultiTexPolygon> polygons;
	map<int, int> vtxMap;
	
	int vtxSize = (int)tetra.m_vertices.size();
	for (int i = 0; i < (int)tetra.m_tetras.size(); ++i) {
		const KMultiTexTetra& tet = tetra.m_tetras[i];
		const vector<KTetraTexCoord>& texCoords = tet.m_texCoords;
		const vector<int>& texIDs = tet.m_texIDs;
		int code = 0;
		double val[4];
		for (int j = 0; j < 4; ++j) {
			val[j] = vtxValue[tet.m_vtx[j]];
			if (val[j] < threshold)
				code += (1 << j);
		}
		if (code == 0) continue;
		const int* mapping = mapping_pattern[code];
		KVector3d vPos[4];
		int       vKey[4];
		vector<KVector3d> vCoord[4];
		for (int j = 0; j < 4; ++j) {
			int index = tet.m_vtx[mapping[j]];
			const KVector3d& pos = tetra.m_vertices[index].m_pos;
			vPos[j] = pos;
			vKey[j] = vtxSize * index + index;
			for (int k = 0; k < (int)texCoords.size(); ++k)
				vCoord[j].push_back(texCoords[k].m_coord[mapping[j]]);
		}
		if (code == 15) {
			for (int j = 0; j < 4; ++j) {
				if (tet.m_neighbor[j] != -1) continue;
				const int& index0 = KTetraModel::FACE_VTX_ORDER[j][0];
				const int& index1 = KTetraModel::FACE_VTX_ORDER[j][1];
				const int& index2 = KTetraModel::FACE_VTX_ORDER[j][2];
				addMultiTexPolygon(
					vertices, polygons, vtxMap,
					vPos[index0], vPos[index1], vPos[index2],
					vKey[index0], vKey[index1], vKey[index2],
					vCoord[index0], vCoord[index1], vCoord[index2],
					texIDs); 
				mapPolygonToTetra.push_back(i);
			}
		} else {
			switch (code) {
				case 1:
				case 2:
				case 4:
				case 8:
					{
						KVector3d wPos[3];
						int       wKey[3];
						vector<KVector3d> wCoord[3];
						for (int j = 0; j < 3; ++j) {
							double u = (threshold - val[mapping[0]]) / (val[mapping[j + 1]] - val[mapping[0]]);
							wPos[j] = vPos[0];
							wPos[j].scale(1 - u);
							wPos[j].addWeighted(vPos[j + 1], u);
							int index0 = min(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							int index1 = max(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							wKey[j] = vtxSize * index0 + index1;
							for (int k = 0; k < (int)texCoords.size(); ++k) {
								KVector3d tex = vCoord[0][k];
								tex.scale(1 - u);
								tex.addWeighted(vCoord[j + 1][k], u);
								wCoord[j].push_back(tex);
							}
						}
						polygons_oncutplane.push_back(polygons.size());
						addMultiTexPolygon(
							vertices, polygons, vtxMap,
							wPos[0], wPos[1], wPos[2],
							wKey[0], wKey[1], wKey[2],
							wCoord[0], wCoord[1], wCoord[2],
							texIDs); 
						mapPolygonToTetra.push_back(i);
						for (int j = 1; j <= 3; ++j) {
							if (tet.m_neighbor[mapping[j]] != -1) continue;
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								wPos[j % 3], vPos[0], wPos[(j + 1) % 3],
								wKey[j % 3], vKey[0], wKey[(j + 1) % 3],
								wCoord[j % 3], vCoord[0], wCoord[(j + 1) % 3],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
					}
					break;
				case 3:
				case 5:
				case 6:
				case 9:
				case 10:
				case 12:
					{
						static int pairs[][2] = {{0, 3}, {0, 2}, {1, 2}, {1, 3}};
						KVector3d wPos[4];
						int       wKey[4];
						vector<KVector3d> wCoord[4];
						for (int j = 0; j < 4; ++j) {
							const int& i0 = pairs[j][0];
							const int& i1 = pairs[j][1];
							double u = (threshold - val[mapping[i0]]) / (val[mapping[i1]] - val[mapping[i0]]);
							wPos[j] = vPos[i0];
							wPos[j].scale(1 - u);
							wPos[j].addWeighted(vPos[i1], u);
							int index0 = min(tet.m_vtx[mapping[i0]], tet.m_vtx[mapping[i1]]);
							int index1 = max(tet.m_vtx[mapping[i0]], tet.m_vtx[mapping[i1]]);
							wKey[j] = vtxSize * index0 + index1;
							for (int k = 0; k < (int)texCoords.size(); ++k) {
								KVector3d tex(vCoord[i0][k]);
								tex.scale(1 - u);
								tex.addWeighted(vCoord[i1][k], u);
								wCoord[j].push_back(tex);
							}
						}
						polygons_oncutplane.push_back(polygons.size());
						addMultiTexPolygon(
							vertices, polygons, vtxMap,
							wPos[0], wPos[2], wPos[1],
							wKey[0], wKey[2], wKey[1],
							wCoord[0], wCoord[2], wCoord[1],
							texIDs);
						mapPolygonToTetra.push_back(i);
						polygons_oncutplane.push_back(polygons.size());
						addMultiTexPolygon(
							vertices, polygons, vtxMap,
							wPos[0], wPos[3], wPos[2],
							wKey[0], wKey[3], wKey[2],
							wCoord[0], wCoord[3], wCoord[2],
							texIDs);
						mapPolygonToTetra.push_back(i);
						if (tet.m_neighbor[mapping[0]] == -1) {
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[1], wPos[2], wPos[3],
								vKey[1], wKey[2], wKey[3],
								vCoord[1], wCoord[2], wCoord[3],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
						if (tet.m_neighbor[mapping[1]] == -1) {
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[0], wPos[0], wPos[1],
								vKey[0], wKey[0], wKey[1],
								vCoord[0], wCoord[0], wCoord[1],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
						if (tet.m_neighbor[mapping[2]] == -1) {
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[1], wPos[3], wPos[0],
								vKey[1], wKey[3], wKey[0],
								vCoord[1], wCoord[3], wCoord[0],
								texIDs);
							mapPolygonToTetra.push_back(i);
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[1], wPos[0], vPos[0],
								vKey[1], wKey[0], vKey[0],
								vCoord[1], wCoord[0], vCoord[0],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
						if (tet.m_neighbor[mapping[3]] == -1) {
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[0], wPos[1], wPos[2],
								vKey[0], wKey[1], wKey[2],
								vCoord[0], wCoord[1], wCoord[2],
								texIDs);
							mapPolygonToTetra.push_back(i);
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[0], wPos[2], vPos[1],
								vKey[0], wKey[2], vKey[1],
								vCoord[0], wCoord[2], vCoord[1],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
					}
					break;
				case 7:
				case 11:
				case 13:
				case 14:
					{
						KVector3d wPos[3];
						int       wKey[3];
						vector<KVector3d> wCoord[3];
						for (int j = 0; j < 3; ++j) {
							double u = (threshold - val[mapping[0]]) / (val[mapping[j + 1]] - val[mapping[0]]);
							wPos[j] = vPos[0];
							wPos[j].scale(1 - u);
							wPos[j].addWeighted(vPos[j + 1], u);
							int index0 = min(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							int index1 = max(tet.m_vtx[mapping[0]], tet.m_vtx[mapping[j + 1]]);
							wKey[j] = vtxSize * index0 + index1;
							for (int k = 0; k < (int)texCoords.size(); ++k) {
								KVector3d tex(vCoord[0][k]);
								tex.scale(1 - u);
								tex.addWeighted(vCoord[j + 1][k], u);
								wCoord[j].push_back(tex);
							}
						}
						polygons_oncutplane.push_back(polygons.size());
						addMultiTexPolygon(
							vertices, polygons, vtxMap,
							wPos[2], wPos[1], wPos[0],
							wKey[2], wKey[1], wKey[0],
							wCoord[2], wCoord[1], wCoord[0],
							texIDs);
						mapPolygonToTetra.push_back(i);
						for (int j = 1; j <= 3; ++j) {
							if (tet.m_neighbor[mapping[j]] != -1) continue;
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[j % 3 + 1], wPos[j % 3], wPos[(j + 1) % 3],
								vKey[j % 3 + 1], wKey[j % 3], wKey[(j + 1) % 3],
								vCoord[j % 3 + 1], wCoord[j % 3], wCoord[(j + 1) % 3],
								texIDs);
							mapPolygonToTetra.push_back(i);
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[j % 3 + 1], wPos[(j + 1) % 3], vPos[(j + 1) % 3 + 1],
								vKey[j % 3 + 1], wKey[(j + 1) % 3], vKey[(j + 1) % 3 + 1],
								vCoord[j % 3 + 1], wCoord[(j + 1) % 3], vCoord[(j + 1) % 3 + 1],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
						if (tet.m_neighbor[mapping[0]] == -1) {
							addMultiTexPolygon(
								vertices, polygons, vtxMap,
								vPos[1], vPos[2], vPos[3],
								vKey[1], vKey[2], vKey[3],
								vCoord[1], vCoord[2], vCoord[3],
								texIDs);
							mapPolygonToTetra.push_back(i);
						}
					}
					break;
			}
		}
	}
	
	KMultiTexPolygonModel result;
	result.m_vertices = vertices;
	result.m_polygons = polygons;
	
	return result;
}

void KMarchingTetra::addMultiTexPolygon(
	vector<KVertex>& vertices, vector<KMultiTexPolygon>& polygons,
	std::map<int, int>& vtxMap,
	const KVector3d& vPos0, const KVector3d& vPos1, const KVector3d& vPos2,
	const int&       vKey0, const int&       vKey1, const int&       vKey2,
	const vector<KVector3d>& vCoord0, const vector<KVector3d>& vCoord1, const vector<KVector3d>& vCoord2,
	const vector<int>& texIDs
) {
	int vID[3];
	const KVector3d* vPos_p[3] = {&vPos0, &vPos1, &vPos2};
	const int      * vKey_p[3] = {&vKey0, &vKey1, &vKey2};
	
	for (int i = 0; i < 3; ++i) {
		map<int, int>::iterator findPos = vtxMap.find(*vKey_p[i]);
		if (findPos != vtxMap.end()) {
			vID[i] = findPos->second;
		} else {
			vID[i] = (int)vtxMap.size();
			vertices.push_back(KVertex(*vPos_p[i]));
			vtxMap.insert(pair<int, int>(*vKey_p[i], vID[i]));
		}
	}
	KMultiTexPolygon poly(vID[0], vID[1], vID[2]);
	for (int i = 0; i < (int)vCoord0.size(); ++i)
		poly.m_texCoords.push_back(KPolygonTexCoord(vCoord0[i], vCoord1[i], vCoord2[i]));
	poly.m_texIDs = texIDs;
	polygons.push_back(poly);
}
