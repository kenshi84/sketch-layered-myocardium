#pragma once

#include "KMath.h"
#include "KGeometry.h"
#include "KMultiTexGeometry.h"

#include <vector>
#include <map>

class KMarchingTetra
{
	KMarchingTetra(void);
	~KMarchingTetra(void);
public:
	static KPolygonModel calcMarchingTetra(const KTetraModel& tetra, const std::vector<double>& vtxValue, const double threshold) {
		return calcMarchingTetra(tetra, vtxValue, threshold, std::vector<int>());
	}
	static KPolygonModel calcMarchingTetra(const KTetraModel& tetra, const std::vector<double>& vtxValue, const double threshold, std::vector<int>& polygons_oncutplane);
	static KMultiTexPolygonModel calcMultiTexMarchingTetra(std::vector<int>& mapPolygonToTetra, const KMultiTexTetraModel& tetra, const std::vector<double>& vtxValue, const double threshold) {
		return calcMultiTexMarchingTetra(mapPolygonToTetra, tetra, vtxValue, threshold, std::vector<int>());
	}
	static KMultiTexPolygonModel calcMultiTexMarchingTetra(std::vector<int>& mapPolygonToTetra, const KMultiTexTetraModel& tetra, const std::vector<double>& vtxValue, const double threshold, std::vector<int>& polygons_oncutplane);
protected:
	static void addPolygon(
		std::vector<KVertex>& vertices, std::vector<KPolygon>& polygons,
		std::map<int, int>& vtxMap,
		const KVector3d& vPos0, const KVector3d& vPos1, const KVector3d& vPos2, 
		const int&       vKey0, const int&       vKey1, const int&       vKey2);
	static void addMultiTexPolygon(
		std::vector<KVertex>& vertices, std::vector<KMultiTexPolygon>& polygons,
		std::map<int, int>& vtxMap,
		const KVector3d& vPos0, const KVector3d& vPos1, const KVector3d& vPos2,
		const int&       vKey0, const int&       vKey1, const int&       vKey2,
		const std::vector<KVector3d>& vCoord0, const std::vector<KVector3d>& vCoord1, const std::vector<KVector3d>& vCoord2,
		const std::vector<int>& tetIDs
	);
};
