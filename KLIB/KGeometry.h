#pragma once

#include <vector>
#include <string>
#include "KMath.h"

class KVertex
{
public:
	KVertex(void) : m_pos(KVector3d()) {}
	KVertex(const KVector3d& pos) : m_pos(pos) {}
	~KVertex(void) {}
	KVector3d m_pos;
	std::vector<int> m_neighbor;
};

class KTetra
{
public:
	KTetra(void) {
		init(-1, -1, -1, -1);
	}
	KTetra(int v0, int v1, int v2, int v3) {
		init(v0, v1, v2, v3);
	}
	int m_vtx[4];
	KVector3d m_normal[4];
	int m_neighbor[4];
protected:
	void init(int v0, int v1, int v2, int v3) {
		m_vtx[0] = v0;
		m_vtx[1] = v1;
		m_vtx[2] = v2;
		m_vtx[3] = v3;
		m_neighbor[0] = m_neighbor[1] = m_neighbor[2] = m_neighbor[3] = -1;
	}
};

class KPolygon {
public:
	int m_vtx[3];
	KVector3d m_normal[3];
	int m_neighbor[3];
public:
	KPolygon(void) {
		init(-1, -1, -1);
	}
	KPolygon(int v0, int v1, int v2) {
		init(v0, v1, v2);
	}
protected:
	void init(int v0, int v1, int v2) {
		m_vtx[0] = v0;
		m_vtx[1] = v1;
		m_vtx[2] = v2;
	}
};
class KEdge {
public:
	int m_vtx[2];
	KEdge() {}
	KEdge(int vtx0, int vtx1) {
		m_vtx[0] = vtx0;
		m_vtx[1] = vtx1;
	}
};

class KPolygonModel {
public:
	std::vector<KVertex>  m_vertices;
	std::vector<KPolygon> m_polygons;
	std::vector<KEdge> m_edges;
	//std::vector<std::vector<int> > m_vtxNeighbors;
	KPolygonModel(void) {}
	~KPolygonModel(void) {}
	
	bool loadFile(const std::string& fname);
	bool saveFile(const std::string& fname) const;
	
	bool loadObjFile(const std::string& fname);
	bool loadMqoFile(const std::string& fname);
	bool saveObjFile(const std::string& fname) const;
	bool saveMqoFile(const std::string& fname) const;
	void calcSmoothNormals();
	void calcSmoothNormalsWithSharpEdges(double threshold);
	void calcEdges();
	void calcNeighbors();
};

/*
v1-v2-v3は外から見て左回り
     v0--v2
    / \ .|
   /  .\ |
  / .   \|
v1.______v3
 */
class KTetraModel
{
public:
	static const int FACE_VTX_ORDER[4][3];
	KTetraModel(void) {}
	~KTetraModel(void) {}
	bool loadFile(const std::string& fname);
	void print();
	void calcNormal();
	void calcNeighbor();
	KPolygonModel toPolygonModel() const;
	std::vector<KVertex> m_vertices;
	std::vector<KTetra>  m_tetras;
	void saveEleNodeFile(const std::string& fname) const;
};

//-----------------------------
// 2D mesh class
//-----------------------------
class KHalfEdge2D;
class KVertex2D
{
public:
	KVertex2D(void) : m_pos(KVector2d()), m_isBoundary(false), mp_halfedge(NULL) {}
	KVertex2D(const KVector2d& pos) : m_pos(pos), m_isBoundary(false), mp_halfedge(NULL) {}
	~KVertex2D(void) {}
	KVector2d m_pos;
	KHalfEdge2D* mp_halfedge;
	bool m_isBoundary;
	operator const KVector2d& () const { return m_pos; }
	operator KVector2d& () { return m_pos; }
};

class KPolygon2D {
public:
	int m_vtx[3];
	int m_neighbor[3];
	KVector2d m_texCoord[3];
	KHalfEdge2D* mp_halfedge;
public:
	KPolygon2D(void) {
		init(-1, -1, -1);
	}
	KPolygon2D(int v0, int v1, int v2) {
		init(v0, v1, v2);
	}
protected:
	void init(int v0, int v1, int v2) {
		m_vtx[0] = v0;
		m_vtx[1] = v1;
		m_vtx[2] = v2;
		m_neighbor[0] = m_neighbor[1] = m_neighbor[2] = -1;
		mp_halfedge = NULL;
	}
};

//     halfedge[1]
//        =====
//vtx[0] o-----o vtx[1]
//        =====
//     halfedge[0]

class KEdge2D {
public:
	KEdge2D () {
		init(NULL, NULL);
	}
	KEdge2D (KVertex2D* p_vtx0, KVertex2D* p_vtx1) {
		init(p_vtx0, p_vtx1);
	}
	KVertex2D* mp_vtx[2];
	KHalfEdge2D* mp_halfedge[2];
private:
	void init(KVertex2D* p_vtx0, KVertex2D* p_vtx1) {
		mp_vtx[0] = p_vtx0;
		mp_vtx[1] = p_vtx1;
		mp_halfedge[0] = mp_halfedge[1] = NULL;
	}
};

//prev \   face  / next
//      \       /
//       ------o vtx
//  edge =======
//       o-pair-
//      /       \
//     /         \
//

class KHalfEdge2D {
public:
	KHalfEdge2D(void)
		: mp_vtx(NULL), mp_pair(NULL), mp_face(NULL), mp_next(NULL), mp_prev(NULL) {}
	KHalfEdge2D (KVertex2D* p_vtx, KHalfEdge2D* p_pair, KPolygon2D* p_face, KHalfEdge2D* p_next, KHalfEdge2D* p_prev)
		: mp_vtx(p_vtx), mp_pair(p_pair), mp_face(p_face), mp_next(p_next), mp_prev(p_prev) {}
	~KHalfEdge2D () {}
	KVertex2D  * mp_vtx;
	KHalfEdge2D* mp_pair;
	KPolygon2D * mp_face;
	KHalfEdge2D* mp_next;
	KHalfEdge2D* mp_prev;
	KEdge2D    * mp_edge;
};

class KPolygonModel2D {
public:
	std::vector<KVertex2D>  m_vertices;
	std::vector<KPolygon2D> m_polygons;
	std::vector<KHalfEdge2D> m_halfedges;
	std::vector<KEdge2D> m_edges;
public:
	KPolygonModel2D(void) {}
	~KPolygonModel2D(void) {}
	bool loadFile(const char* fname);
	bool saveFile(const char* fname) const;
	//----------------------------------------
	// file format (similar to .obj)
	//----------------------------------------
	// # list of vtx position
	// v %f %f     
	// v %f %f
	// ...
	// # list of face indices (1-origin)
	// f %d %d %d
	// f %d %d %d
	// ...
	// [EOF]
	void calcFaceNeighbor();
	double getArea() const;
	bool isInside( const KVector2d& pos, unsigned int& polygon_id, double& barycoord_p0, double& barycoord_p1, double& barycoord_p2) const;
	void calcEdge();
	void calcVtxNeighbor();
	void calcHalfEdge();
};

