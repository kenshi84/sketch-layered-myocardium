#include "StdAfx.h"
#include "KOGL.h"

#include <vector>

using namespace std;

KOGL::KOGL(void) :
m_ButtonDown(false),
m_pWnd(0),
m_pDC(0),
m_ButtonDownMotionType(BUTTONDOWN_ROTATE),
m_eyePoint   (-5, 0, 0),
m_focusPoint (0 , 0, 0),
m_upDirection(0 , 1, 0),
m_fovy(30),
m_zNear(0.01),
m_zFar(10000)
{
	m_clearColor[0] = m_clearColor[1] = m_clearColor[2] = m_clearColor[3] = 1.0f;
}

bool KOGL::OnCreate(CWnd* pWnd) {
	m_pWnd = pWnd ;
	m_pDC = new CClientDC(pWnd);
	if (!m_pDC) return false;
	
	if (!SetupPixelFormat()) return false;
	
	m_hRC = wglCreateContext(m_pDC->GetSafeHdc());
	if (!m_hRC) return false;
	
	if (!wglMakeCurrent(m_pDC->GetSafeHdc(), m_hRC)) return false;
	
	initOpenGL();
	return true;
}
void KOGL::OnDraw_Begin() {
	m_isDrawing = true;
	makeOpenGLCurrent() ;
	glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2], m_clearColor[3]);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );
	glMatrixMode( GL_MODELVIEW ) ;
}
void KOGL::OnDraw_End() {
	glFinish();
	SwapBuffers(m_pDC->GetSafeHdc());
	wglMakeCurrent(0, 0);
	m_isDrawing = false ;
}
void KOGL::OnDestroy() {
	makeOpenGLCurrent() ;
	wglMakeCurrent(0, 0);
	wglDeleteContext(m_hRC);
	if (m_pDC) delete m_pDC;
}
void KOGL::OnSize(int cx, int cy) {
	makeOpenGLCurrent() ;
	
	if (cx <= 0 || cy <= 0)	return;
	
	glViewport(0, 0, cx, cy);
	
	// select the viewing volume
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective(m_fovy, cx / (double)cy , m_zNear, m_zFar);
	
	glMatrixMode( GL_MODELVIEW ) ;
	glLoadIdentity();
	gluLookAt(
		m_eyePoint.x,
		m_eyePoint.y,
		m_eyePoint.z,
		m_focusPoint.x,
		m_focusPoint.y,
		m_focusPoint.z,
		m_upDirection.x,
		m_upDirection.y,
		m_upDirection.z);
}
void KOGL::ButtonDownForZoom     (const CPoint& pos) {
	m_ButtonDown = true;
	m_prevpos = pos;
	m_pWnd->SetCapture();
	m_ButtonDownMotionType = BUTTONDOWN_ZOOM;
}
void KOGL::ButtonDownForRotate   (const CPoint& pos) {
	m_ButtonDown = true;
	m_prevpos = pos;
	m_pWnd->SetCapture();
	m_ButtonDownMotionType = BUTTONDOWN_ROTATE;
}
void KOGL::ButtonDownForTranslate(const CPoint& pos) {
	m_ButtonDown = true;
	m_prevpos = pos;
	m_pWnd->SetCapture();
	m_ButtonDownMotionType = BUTTONDOWN_TRANSLATE;
}
void KOGL::MouseMove(const CPoint& pos) {
	if(m_ButtonDown && m_pWnd == m_pWnd->GetCapture()){
		CRect rect;
		m_pWnd->GetClientRect(&rect);
		switch( m_ButtonDownMotionType ){
		case BUTTONDOWN_ROTATE:
			{
				double thetaX = 2 * M_PI * (pos.x - m_prevpos.x) / getWidth();
				double thetaY = 2 * M_PI * (pos.y - m_prevpos.y) / getHeight();
				
				KVector3d eye, leftDirection;
				KMatrix4d m;
				m.setIdentity();
				
				// Horizontal rotation
				eye.sub(m_eyePoint, m_focusPoint);
				m.setRotationFromAxisAngle(m_upDirection, -thetaX);
				eye = m.transform(eye);
				
				// Vertical rotation
				leftDirection.cross(eye, m_upDirection);
				m.setRotationFromAxisAngle(leftDirection, thetaY);
				eye = m.transform(eye);
				m_upDirection = m.transform(m_upDirection);
				
				m_eyePoint.add(eye, m_focusPoint);
				
				updateEyePosition();
			}
			break;
		case BUTTONDOWN_TRANSLATE:
			{
				KVector3d eye, leftDirection;
				
				eye.sub(m_eyePoint, m_focusPoint);
				leftDirection.cross(eye, m_upDirection);
				
				double len = eye.length();
				KVector3d transX(leftDirection);
				KVector3d transY(m_upDirection);
				transX.normalize();
				transY.normalize();
				
				transX.scale(len * (pos.x - m_prevpos.x) / (double)getWidth());
				transY.scale(len * (pos.y - m_prevpos.y) / (double)getHeight());
				
				m_eyePoint.add(transX);
				m_eyePoint.add(transY);
				m_focusPoint.add(transX);
				m_focusPoint.add(transY);
				
				updateEyePosition();
			}
			break;
		case BUTTONDOWN_ZOOM:
			{
				KVector3d eyeDirection;
				eyeDirection.sub(m_focusPoint, m_eyePoint);
				eyeDirection.scale((pos.y - m_prevpos.y) / (double)getHeight());
				
				m_eyePoint.add(eyeDirection);
				
				updateEyePosition();
			}
			break;
		}
		m_prevpos = pos;
		RedrawWindow();
	}
}
void KOGL::ButtonUp() {
	if(m_pWnd == m_pWnd->GetCapture()){
		m_ButtonDown = false;
		ReleaseCapture();
	}
}
void KOGL::updateEyePosition() {
	makeOpenGLCurrent();
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	gluLookAt(
		m_eyePoint.x,
		m_eyePoint.y,
		m_eyePoint.z,
		m_focusPoint.x,
		m_focusPoint.y,
		m_focusPoint.z,
		m_upDirection.x,
		m_upDirection.y,
		m_upDirection.z);
	//wglMakeCurrent(NULL,NULL);
}
bool KOGL::outputBitmap(char* filename) {
	RECT rect ;
	m_pWnd->GetClientRect( &rect ) ;
	int o = 4-((rect.right*3) % 4) ;
	o = (o==4?0:o) ;

	const int imgsize = (rect.right*3+o)*rect.bottom ;
	vector<char> imagebits(imgsize);
	makeOpenGLCurrent() ;
	glReadPixels( 0,0,rect.right,rect.bottom,GL_BGR_EXT
		,GL_UNSIGNED_BYTE,&imagebits[0] ) ;
	
	BITMAPFILEHEADER bmfh ;		ZeroMemory( &bmfh,sizeof(BITMAPFILEHEADER) ) ;
	BITMAPINFOHEADER bmih ;			ZeroMemory( &bmih,sizeof(BITMAPINFOHEADER) ) ;
	bmfh.bfType = 0x4d42;  // 'BM'
	bmfh.bfSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+imgsize ;
	bmfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) ;

	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biWidth = rect.right ;
	bmih.biHeight = rect.bottom ;
	bmih.biPlanes = 1;
	bmih.biBitCount = 24 ;
	bmih.biCompression = BI_RGB ;
	bmih.biSizeImage = imgsize ;

	FILE* fp = fopen( filename,"wb" ) ;
	if( !fp
		|| fwrite( &bmfh,sizeof( BITMAPFILEHEADER ),1,fp ) < 1
		|| fwrite( &bmih,sizeof( BITMAPINFOHEADER ),1,fp ) < 1
		|| fwrite( &imagebits[0],1,imgsize,fp ) < (unsigned)imgsize ) 
	{
		return false;
	} else {
		return true;
	}
}
void KOGL::project(double objx, double objy, double objz, double& winx, double& winy, double& winz) {
	makeOpenGLCurrent();
	int vp[4] ;
	double model[16], proj[16] ;
	glGetIntegerv(GL_VIEWPORT, vp) ;
	glGetDoublev(GL_MODELVIEW_MATRIX, model) ;
	glGetDoublev(GL_PROJECTION_MATRIX, proj) ;
	gluProject(objx, objy, objz, model, proj, vp, &winx, &winy, &winz);
	wglMakeCurrent(NULL, NULL);
}
void KOGL::project(const KVector3d& inVec, KVector3d& outVec) {
	project(inVec.x, inVec.y, inVec.z, outVec.x, outVec.y, outVec.z);
}
void KOGL::unProject(double win_x, double win_y, double win_z, double& objx, double& objy, double& objz) {
	int vp[4] ;
	double model[16],proj[16] ;
	makeOpenGLCurrent();
	glGetIntegerv(GL_VIEWPORT,vp) ;
	glGetDoublev(GL_MODELVIEW_MATRIX,model) ;
	glGetDoublev(GL_PROJECTION_MATRIX,proj) ;
	gluUnProject( win_x , vp[3]-win_y-1 , win_z , model , proj,vp,&objx,&objy,&objz );
	wglMakeCurrent(NULL, NULL);
}
void KOGL::unProject(const KVector3d& inVec, KVector3d& outVec) {
	unProject(inVec.x, inVec.y, inVec.z, outVec.x, outVec.y, outVec.z);
}
void KOGL::initOpenGL() {
	glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2], m_clearColor[3]);
	glClearDepth( 1.0f );
	glEnable( GL_DEPTH_TEST );
	//αブレンドでViewDependent-Textureを張るための
	//Tips
	//glDepthFunc( GL_LEQUAL ) ;
	//glEnable( GL_BLEND );
	//glDisable( GL_BLEND );
	//glBlendFunc( GL_ONE_MINUS_SRC_ALPHA,GL_SRC_ALPHA );
	glBlendFunc( GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA );
	//glBlendFunc( GL_SRC_ALPHA,GL_DST_ALPHA );
	//glBlendFunc( GL_ONE_MINUS_DST_ALPHA,GL_DST_ALPHA );

	GLfloat lightpos[] = { 0, 0, 1, 0 };
	//GLfloat lightpos[] = { 1000,1000,-50000,1 };
	//Shading
	GLfloat spec[] = { 0, 0, 0, 1 };
	GLfloat diff[] = { 0.8f, 0.8f, 0.8f, 1 };
	GLfloat amb[]  = { 0.2f, 0.2f, 0.2f, 1 };
	//GLfloat spec[] = { 0,0,0.05f,1 } ;
	//GLfloat diff[] = { 0.8f,0.8f,0.8f,0.5f };
	//GLfloat amb[]  = { 0.3f,0.3f,0.3f,0.5f };

	GLfloat shininess = 0;	//1.5f ;
	
	glMaterialfv( GL_FRONT, GL_SPECULAR,  spec );
	glMaterialfv( GL_FRONT, GL_DIFFUSE,   diff );
	glMaterialfv( GL_FRONT, GL_AMBIENT,   amb );
	glMaterialfv( GL_FRONT, GL_SHININESS, &shininess );
	glLightfv(GL_LIGHT0, GL_POSITION, lightpos);

	GLfloat light_Ambient0[] = { 0, 0, 0, 1};
	GLfloat light_Diffuse0[] = { 0.8f, 0.8f, 0.8f, 1};
	GLfloat light_Specular0[]= { 1, 1, 1, 1};
	//GLfloat light_Ambient0[] = { 0.5f , 0.5f , 0.5f , 1};
	//GLfloat light_Diffuse0[] = { 0.8f, 0.8f, 0.8f, 0.8f};
	//GLfloat light_Specular0[]= { 0.1f , 0.1f , 0.1f , 1};
	glLightfv(GL_LIGHT0,GL_AMBIENT,light_Ambient0);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light_Diffuse0);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light_Specular0);

	GLfloat light_Ambient1[] = { 0,0,0,1};
	GLfloat light_Diffuse1[] = { 0.5f , 0.5f , 0.5f , 1};
	GLfloat light_Specular1[]= { 0,0,0,1};
	glLightfv(GL_LIGHT1,GL_AMBIENT,light_Ambient1);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,light_Diffuse1);
	glLightfv(GL_LIGHT1,GL_SPECULAR,light_Specular1);
	
	GLfloat light_Ambient2[] = { 0,0,0, 1};
	GLfloat light_Diffuse2[] = { 0.5f , 0.5f , 0.5f , 1};
	GLfloat light_Specular2[]= { 0,0,0,1};
	glLightfv(GL_LIGHT2,GL_AMBIENT,light_Ambient2);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,light_Diffuse2);
	glLightfv(GL_LIGHT2,GL_SPECULAR,light_Specular2);
	
	glEnable( GL_LIGHT0 );
	glEnable( GL_LIGHT1 );
	//glEnable( GL_LIGHT2 );
	//glEnable( GL_LIGHTING ) ;
	//glEnable( GL_TEXTURE_2D );

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	
	//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	
	glShadeModel( GL_SMOOTH ) ;
	//glShadeModel( GL_FLAT ) ;
	glPixelStorei( GL_UNPACK_ALIGNMENT,4 ) ;

	glCullFace( GL_BACK ) ;
	glEnable( GL_CULL_FACE ) ;
	
	glPolygonMode( GL_FRONT,GL_FILL ) ;
	//glEnable( GL_NORMALIZE ) ;
}
void KOGL::getScreenCoordToGlobalLine(int cx,int cy, KVector3d& start, KVector3d& ori) {
	makeOpenGLCurrent() ;
	double modelMat[16],projMat[16] ;int vp[4];
	glGetDoublev(GL_MODELVIEW_MATRIX,modelMat) ;
	glGetDoublev(GL_PROJECTION_MATRIX,projMat) ;
	glGetIntegerv(GL_VIEWPORT,vp) ;
	start.set(m_eyePoint);
	gluUnProject(cx, vp[3] - cy, 0.01, modelMat, projMat, vp, &ori.x, &ori.y, &ori.z) ;
	ori.sub(start);
	ori.normalize();
	wglMakeCurrent(NULL,NULL);
}
bool KOGL::SetupPixelFormat() {
	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),  // size of this pfd
			1,                              // version number
			PFD_DRAW_TO_WINDOW |            // support window
			PFD_SUPPORT_OPENGL |          // support OpenGL
			PFD_DOUBLEBUFFER,             // double buffered
			PFD_TYPE_RGBA,                  // RGBA type
			32,                             // 24-bit color depth
			0, 0, 0, 0, 0, 0,               // color bits ignored
			0,                              // no alpha buffer
			0,                              // shift bit ignored
			0,                              // no accumulation buffer
			0, 0, 0, 0,                     // accum bits ignored
			//        32,                             // 32-bit z-buffer
			16,	// NOTE: better performance with 16-bit z-buffer
			0,                              // no stencil buffer
			0,                              // no auxiliary buffer
			PFD_MAIN_PLANE,                 // main layer
			0,                              // reserved
			0, 0, 0                         // layer masks ignored
	};
	
	int pixelformat;
	
	pixelformat = ChoosePixelFormat(m_pDC->GetSafeHdc(), &pfd);
	
	if ( !pixelformat  )	return false;
	
	if ( !SetPixelFormat(m_pDC->GetSafeHdc(), pixelformat, &pfd) )
		return false;
	
	return true;
}
