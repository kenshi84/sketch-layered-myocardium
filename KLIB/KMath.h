#pragma once

#define _USE_MATH_DEFINES
#include <cmath>

#include <math.h>
#include <vector>
#include <iostream>
#include <algorithm>

using std::min;
using std::max;

class KVector3d;



/////////////////////////////////////////////////////////////////
//
// KVector2d
//
/////////////////////////////////////////////////////////////////

class KVector2d {
public:
	double x;
	double y;
	KVector2d(void) : x(0), y(0) {}						// zero
	KVector2d(double _x, double _y) : x(_x), y(_y) {}	// standard
	KVector2d(double _xy) : x(_xy), y(_xy) {}			// same x & y
	KVector2d(const KVector2d& p0, const KVector2d& p1) 
		: x(p1.x - p0.x), y(p1.y - p0.y) {}				// diff vector from pos0 to pos1
	//KVector2d(const KVector3d& vec3d) : x(vec3d.x), y(vec3d.y) {};					// implicit conversion from 3D to 2D (take xy of xyz)
	~KVector2d(void) {}
	void set(const KVector2d& v) {
		x = v.x;
		y = v.y;
	}
	void set(double _x, double _y) {
		x = _x;
		y = _y;
	}
	void set(double _xy) {
		x = y = _xy;
	}
	void setZero() { set(0); }
	void add(const KVector2d& v) {
		x += v.x;
		y += v.y;
	}
	void add(double _x, double _y) {
		x += _x;
		y += _y;
	}
	void sub(const KVector2d& v) {
		x -= v.x;
		y -= v.y;
	}
	void sub(double _x, double _y) {
		x -= _x;
		y -= _y;
	}
	void scale(double s) {
		x *= s;
		y *= s;
	}
	void scale(double sx, double sy) {
		x *= sx;
		y *= sy;
	}
	void addWeighted(const KVector2d& v, double weight) {
		x += v.x * weight;
		y += v.y * weight;
	}
	double lengthSquared() const {
		return x * x + y * y;
	}
	double length() const {
		return sqrt(lengthSquared());
	}
	void normalize() {
		double len = length();
		if (len == 0) return;
		double _len = 1 / len;
		x *= _len;
		y *= _len;
	}
	KVector2d add_return(const KVector2d& v) const {
		KVector2d ret(*this);
		ret.add(v);
		return ret;
	}
	KVector2d add_return(double _x, double _y) const {
		KVector2d ret(*this);
		ret.add(_x, _y);
		return ret;
	}
	KVector2d sub_return(const KVector2d& v) const {
		KVector2d ret(*this);
		ret.sub(v);
		return ret;
	}
	KVector2d sub_return(double _x, double _y) const {
		KVector2d ret(*this);
		ret.sub(_x, _y);
		return ret;
	}
	KVector2d scale_return(double s) const {
		KVector2d ret(*this);
		ret.scale(s);
		return ret;
	}
	KVector2d scale_return(double sx, double sy) const {
		KVector2d ret(*this);
		ret.scale(sx, sy);
		return ret;
	}
	KVector2d addWeighted_return(const KVector2d& v, double weight) const {
		KVector2d ret(*this);
		ret.addWeighted(v, weight);
		return ret;
	}
	KVector2d normalize_return() const {
		KVector2d ret(*this);
		ret.normalize();
		return ret;
	}
	
	double dot (const KVector2d& v) const {
		return x * v.x + y * v.y;
	}
	double cross (const KVector2d& v) const {
		return x * v.y - y * v.x;
	}
	double* getPtr() const {
		return (double*)(this);
	}
	operator double const *() const {
		return (double const *)(this);
	}
	operator double *() {
		return (double*)(this);
	}
	friend KVector2d min2d(const KVector2d& v0, const KVector2d& v1);
	friend KVector2d max2d(const KVector2d& v0, const KVector2d& v1);
	friend KVector2d operator+(const KVector2d& v0, const KVector2d& v1);
	friend KVector2d operator-(const KVector2d& v0, const KVector2d& v1);
	friend KVector2d operator*(const KVector2d& v, double s);
	friend KVector2d operator*(double s, const KVector2d& v0);
};

inline KVector2d min2d(const KVector2d& v0, const KVector2d& v1) {
	return KVector2d(min(v0.x, v1.x), min(v0.y, v1.y));
}

inline KVector2d max2d(const KVector2d& v0, const KVector2d& v1) {
	return KVector2d(max(v0.x, v1.x), max(v0.y, v1.y));
}

inline KVector2d operator+(const KVector2d& v0, const KVector2d& v1) {
	return KVector2d(v0.x + v1.x, v0.y + v1.y);
}

inline KVector2d operator-(const KVector2d& v0, const KVector2d& v1) {
	return KVector2d(v0.x - v1.x, v0.y - v1.y);
}

inline KVector2d operator*(const KVector2d& v, double s) {
	return KVector2d(v.x * s, v.y * s);
}

inline KVector2d operator*(double s, const KVector2d& v) {
	return KVector2d(v.x * s, v.y * s);
}



/////////////////////////////////////////////////////////////////
//
// KVector3d
//
/////////////////////////////////////////////////////////////////

class KVector3d
{
public:
	double x;
	double y;
	double z;
	KVector3d(void) : x(0), y(0), z(0) {}
	KVector3d(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}
	KVector3d(double _xyz) : x(_xyz), y(_xyz), z(_xyz) {}
	~KVector3d(void) {}
	
	void set(const KVector3d& v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}
	void set(double _x, double _y, double _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	void set(double _xyz) {
		x = _xyz;
		y = _xyz;
		z = _xyz;
	}
	void setZero() { set(0); }
	double& operator[](int index) {
		return *((double*)this + index);
	}
	bool operator==(const KVector3d& v) {
		return (x == v.x && y == v.y && z == v.z);
	}
	double* getPtr() const {
		return (double*)(this);
	}
	
	void addWeighted(const KVector3d& v, double w) {
		x += w * v.x;
		y += w * v.y;
		z += w * v.z;
	}

	KVector3d addWeighted_return(const KVector3d& v, double w) const {
		KVector3d result(x, y, z);
		result.x += w * v.x;
		result.y += w * v.y;
		result.z += w * v.z;
		return result;
	}
	
	double dot(const KVector3d& v) const {
		return x * v.x + y * v.y + z * v.z;
	}
	void cross(const KVector3d& v0, const KVector3d& v1) {
		x = v0.y * v1.z - v0.z * v1.y;
		y = v0.z * v1.x - v0.x * v1.z;
		z = v0.x * v1.y - v0.y * v1.x;
	}
	void add(const KVector3d& v) {
		x += v.x;
		y += v.y;
		z += v.z;
	}
	void add(const KVector3d& v, const KVector3d& w) {
		KVector3d result(x, y, z);
		x = v.x + w.x;
		y = v.y + w.y;
		z = v.z + w.z;
	}
	void add(double _x, double _y, double _z) {
		x += _x;
		y += _y;
		z += _z;
	}
	KVector3d add_return(const KVector3d& v) const {
		KVector3d result(x, y, z);
		result.x += v.x;
		result.y += v.y;
		result.z += v.z;
		return result;
	}
	KVector3d add_return(const KVector3d& v, const KVector3d& w) const {
		KVector3d result(x, y, z);
		result.x = v.x + w.x;
		result.y = v.y + w.y;
		result.z = v.z + w.z;
		return result;
	}
	KVector3d add_return(double _x, double _y, double _z) const {
		KVector3d result(x, y, z);
		result.x += _x;
		result.y += _y;
		result.z += _z;
		return result;
	}
	void sub(const KVector3d& v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
	}
	void sub(const KVector3d& v, const KVector3d& w) {
		x = v.x - w.x;
		y = v.y - w.y;
		z = v.z - w.z;
	}
	void sub(double _x, double _y, double _z) {
		x -= _x;
		y -= _y;
		z -= _z;
	}
	KVector3d sub(const KVector3d& v) const {
		KVector3d result(x, y, z);
		result.x -= v.x;
		result.y -= v.y;
		result.z -= v.z;
		return result;
	}
	KVector3d sub(const KVector3d& v, const KVector3d& w) const {
		KVector3d result(x, y, z);
		result.x = v.x - w.x;
		result.y = v.y - w.y;
		result.z = v.z - w.z;
		return result;
	}
	KVector3d sub(double _x, double _y, double _z) const {
		KVector3d result(x, y, z);
		result.x -= _x;
		result.y -= _y;
		result.z -= _z;
		return result;
	}
	void scale(double s) {
		x *= s;
		y *= s;
		z *= s;
	}
	KVector3d scale_return(double s) const {
		KVector3d result(x, y, z);
		result.x *= s;
		result.y *= s;
		result.z *= s;
		return result;
	}
	double lengthSquared() const {
		return x * x + y * y + z * z;
	}
	double length() const {
		return sqrt(lengthSquared());
	}
	void normalize() {
		double len = length();
		if (len == 0) return;
		double _len = 1 / len;
		x *= _len;
		y *= _len;
		z *= _len;
	}
	KVector3d normalize_return() const {
		KVector3d result(x, y, z);
		result.normalize();
		return result;
	}
	bool isZero() { return x == 0 && y == 0 && z == 0; }
	double sum() const { return x + y + z; }
	operator double const *() const {
		return (double const *)(this);
	}
	operator double *() {
		return (double*)(this);
	}
	friend std::ostream& operator<<(std::ostream& os, const KVector3d& vec);
	friend KVector3d min3d(const KVector3d& v0, const KVector3d& v1);
	friend KVector3d max3d(const KVector3d& v0, const KVector3d& v1);
	friend KVector3d operator+(const KVector3d& v0, const KVector3d& v1);
	friend KVector3d operator-(const KVector3d& v0, const KVector3d& v1);
	friend KVector3d operator*(const KVector3d& v, double s);
	friend KVector3d operator*(double s, const KVector3d& v);
};

inline std::ostream& operator<<(std::ostream& os, const KVector3d& vec) {
	return ( os << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")" );
}

inline KVector3d min3d(const KVector3d& v0, const KVector3d& v1) {
	return KVector3d(min(v0.x, v1.x), min(v0.y, v1.y), min(v0.z, v1.z));
}

inline KVector3d max3d(const KVector3d& v0, const KVector3d& v1) {
	return KVector3d(max(v0.x, v1.x), max(v0.y, v1.y), max(v0.z, v1.z));
}

inline KVector3d operator+(const KVector3d& v0, const KVector3d& v1) {
	return KVector3d(v0.x + v1.x, v0.y + v1.y, v0.z + v1.z);
}

inline KVector3d operator-(const KVector3d& v0, const KVector3d& v1) {
	return KVector3d(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z);
}

inline KVector3d operator*(const KVector3d& v, double s) {
	return KVector3d(v.x * s, v.y * s, v.z * s);
}

inline KVector3d operator*(double s, const KVector3d& v) {
	return KVector3d(v.x * s, v.y * s, v.z * s);
}



/////////////////////////////////////////////////////////////////
//
// KVector4d
//
/////////////////////////////////////////////////////////////////

class KVector4d {
public:
	double x, y, z, w;
	KVector4d()
		: x(0), y(0), z(0), w(0) {
	}
	KVector4d(double _x, double _y, double _z, double _w)
		: x(_x), y(_y), z(_z), w(_w) {
	}
	double* getPtr() const {
		return (double*)this;
	}
	void set(const KVector4d& v) {
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;
	}
	void set(double _x, double _y, double _z, double _w) {
		x = _x;
		y = _y;
		z = _z;
		w = _w;
	}
	void set(double _xyzw) {
		x = _xyzw;
		y = _xyzw;
		z = _xyzw;
		w = _xyzw;
	}
	void setZero() { set(0); }
	double operator[](int index) {
		return *((double*)this + index);
	}
	bool operator==(const KVector4d& v) {
		return (x == v.x && y == v.y && z == v.z && w == v.w);
	}
	void addWeighted(const KVector4d& v, double weight) {
		x += weight * v.x;
		y += weight * v.y;
		z += weight * v.z;
		w += weight * v.w;
	}
	
	double dot(const KVector4d& v) const {
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}
	void add(const KVector4d& v) {
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;
	}
	void sub(const KVector4d& v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;
	}
	void sub(const KVector4d& v1, const KVector4d& v2) {
		x = v1.x - v2.x;
		y = v1.y - v2.y;
		z = v1.z - v2.z;
		w = v1.w - v2.w;
	}
	void scale(double s) {
		x *= s;
		y *= s;
		z *= s;
		w *= s;
	}
	double lengthSquared() const {
		return x * x + y * y + z * z + w * w;
	}
	double length() const {
		return sqrt(lengthSquared());
	}
	void normalize() {
		double len = length();
		if (len == 0) return;
		double _len = 1 / len;
		x *= _len;
		y *= _len;
		z *= _len;
		w *= _len;
	}
};

class KMatrix2d {
public:
	double m00, m01;
	double m10, m11;
	
	KMatrix2d(
		double _m00, double _m01,
		double _m10, double _m11) :
		m00(_m00), m01(_m01),
		m10(_m10), m11(_m11) {
	}
	KMatrix2d() :
		m00(0), m01(0),
		m10(0), m11(0) {
	}
	~KMatrix2d() {}
	
	double operator[](int index) {
		return *((double*)this + index);
	}
	double* getPtr() const {
		return (double*)this;
	}
	void set(
		double _m00, double _m01,
		double _m10, double _m11)
	{
		m00 = _m00;		m01 = _m01;
		m10 = _m10;		m11 = _m11;
	}
	void scale(double s) {
		m00 *= s;		m01 *= s;
		m10 *= s;		m11 *= s;
	}
	double determinant() const {
		return m00 * m11 - m01 * m10;
	}
	bool invert() {
		double s = determinant();
		if (s == 0.0) return false;
		s = 1/s;
		// alias-safe way.
		// less *,+,- calculation than expanded expression.
		set(
			 m11, -m01,
			-m10,  m00);
		scale(s);
		return true;
	}
	void mul(const KMatrix2d& m) {
		mul(*this, m);
	}
	void mul(const KMatrix2d& m1, const KMatrix2d& m2) {
		// alias-safe way.
		set(
			m1.m00*m2.m00 + m1.m01*m2.m10,
			m1.m00*m2.m01 + m1.m01*m2.m11,

			m1.m10*m2.m00 + m1.m11*m2.m10,
			m1.m10*m2.m01 + m1.m11*m2.m11);
	}
	KVector2d mul(const KVector2d& vec) const {
		return KVector2d(
			m00 * vec.x + m01 * vec.y, 
			m10 * vec.x + m11 * vec.y);
	}
	void transpose() {
		std::swap(m01, m10);
	}
};

class KMatrix3d {
public:
	double m00, m01, m02;
	double m10, m11, m12;
	double m20, m21, m22;
	
	KMatrix3d(
		double _m00, double _m01, double _m02,
		double _m10, double _m11, double _m12,
		double _m20, double _m21, double _m22) :
		m00(_m00), m01(_m01), m02(_m02),
		m10(_m10), m11(_m11), m12(_m12), 
		m20(_m20), m21(_m21), m22(_m22) {
	}
	KMatrix3d() :
		m00(0), m01(0), m02(0),
		m10(0), m11(0), m12(0),
		m20(0), m21(0), m22(0) {
	}
	~KMatrix3d() {}
	
	double operator[](int index) {
		return *((double*)this + index);
	}
	double* getPtr() const {
		return (double*)this;
	}
	void setIdentity() {
		set(1, 0, 0, 0, 1, 0, 0, 0, 1);
	}
	void setZero() {
		set(0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	void set(
		double _m00, double _m01, double _m02,
		double _m10, double _m11, double _m12,
		double _m20, double _m21, double _m22)
	{
		m00 = _m00;		m01 = _m01;		m02 = _m02;
		m10 = _m10;		m11 = _m11;		m12 = _m12;
		m20 = _m20;		m21 = _m21;		m22 = _m22;
	}
	void scale(double s) {
		m00 *= s;		m01 *= s;		m02 *= s;
		m10 *= s;		m11 *= s;		m12 *= s;
		m20 *= s;		m21 *= s;		m22 *= s;
	}
	double determinant() const {
		return
			m00*(m11*m22 - m21*m12)
			-m01*(m10*m22 - m20*m12)
			+m02*(m10*m21 - m20*m11);
	}
	bool invert() {
		double s = determinant();
		if (s == 0.0) return false;
		s = 1/s;
		// alias-safe way.
		// less *,+,- calculation than expanded expression.
		set(
			m11*m22 - m12*m21, m02*m21 - m01*m22, m01*m12 - m02*m11,
			m12*m20 - m10*m22, m00*m22 - m02*m20, m02*m10 - m00*m12,
			m10*m21 - m11*m20, m01*m20 - m00*m21, m00*m11 - m01*m10);
		scale(s);
		return true;
	}
	void mul(const KMatrix3d& m) {
		mul(*this, m);
	}
	void mul(const KMatrix3d& m1, const KMatrix3d& m2) {
		// alias-safe way.
		set(
			m1.m00*m2.m00 + m1.m01*m2.m10 + m1.m02*m2.m20,
			m1.m00*m2.m01 + m1.m01*m2.m11 + m1.m02*m2.m21,
			m1.m00*m2.m02 + m1.m01*m2.m12 + m1.m02*m2.m22,

			m1.m10*m2.m00 + m1.m11*m2.m10 + m1.m12*m2.m20,
			m1.m10*m2.m01 + m1.m11*m2.m11 + m1.m12*m2.m21,
			m1.m10*m2.m02 + m1.m11*m2.m12 + m1.m12*m2.m22,

			m1.m20*m2.m00 + m1.m21*m2.m10 + m1.m22*m2.m20,
			m1.m20*m2.m01 + m1.m21*m2.m11 + m1.m22*m2.m21,
			m1.m20*m2.m02 + m1.m21*m2.m12 + m1.m22*m2.m22);
	}
	void mul(double scalar) {
		m00 *= scalar; m01 *= scalar;  m02 *= scalar;
		m10 *= scalar; m11 *= scalar;  m12 *= scalar;
		m20 *= scalar; m21 *= scalar;  m22 *= scalar;
	}
	void mul(double * result, const double* vec) const {
		result[0] = m00 * vec[0] + m01 * vec[1] + m02 * vec[2];
		result[1] = m10 * vec[0] + m11 * vec[1] + m12 * vec[2];
		result[2] = m20 * vec[0] + m21 * vec[1] + m22 * vec[2];
	}
	void mul(KVector3d& result, const KVector3d& vec) const {
		result.x = m00 * vec.x + m01 * vec.y + m02 * vec.z;
		result.y = m10 * vec.x + m11 * vec.y + m12 * vec.z;
		result.z = m20 * vec.x + m21 * vec.y + m22 * vec.z;
	}
	KVector3d transform(const KVector3d& v) const {
		return KVector3d(
			m00 * v.x + m01 * v.y + m02 * v.z,
			m10 * v.x + m11 * v.y + m12 * v.z,
			m20 * v.x + m21 * v.y + m22 * v.z);
	}
	void transpose() {
		std::swap(m01, m10);
		std::swap(m02, m20);
		std::swap(m12, m21);
	}
};

class KMatrix4d {
public:
	double m00, m01, m02, m03;
	double m10, m11, m12, m13;
	double m20, m21, m22, m23;
	double m30, m31, m32, m33;
	KMatrix4d(
		double _m00, double _m01, double _m02, double _m03,
		double _m10, double _m11, double _m12, double _m13,
		double _m20, double _m21, double _m22, double _m23,
		double _m30, double _m31, double _m32, double _m33) :
		m00(_m00), m01(_m01), m02(_m02), m03(_m03), 
		m10(_m10), m11(_m11), m12(_m12), m13(_m13), 
		m20(_m20), m21(_m21), m22(_m22), m23(_m23), 
		m30(_m30), m31(_m31), m32(_m32), m33(_m33) {
	}
	KMatrix4d() :
		m00(0), m01(0), m02(0), m03(0), 
		m10(0), m11(0), m12(0), m13(0), 
		m20(0), m21(0), m22(0), m23(0), 
		m30(0), m31(0), m32(0), m33(0) {
	}
	~KMatrix4d() {}
	
	double operator[](int index) {
		return *((double*)this + index);
	}
	double* getPtr() const {
		return (double*)this;
	}
	void set(
		double _m00, double _m01, double _m02, double _m03, 
		double _m10, double _m11, double _m12, double _m13, 
		double _m20, double _m21, double _m22, double _m23, 
		double _m30, double _m31, double _m32, double _m33) 
	{
		m00 = _m00;		m01 = _m01;		m02 = _m02;		m03 = _m03;
		m10 = _m10;		m11 = _m11;		m12 = _m12;		m13 = _m13;
		m20 = _m20;		m21 = _m21;		m22 = _m22;		m23 = _m23;
		m30 = _m30;		m31 = _m31;		m32 = _m32;		m33 = _m33;
	}
	void scale(double s) {
		m00 *= s;		m01 *= s;		m02 *= s;		m03 *= s;
		m10 *= s;		m11 *= s;		m12 *= s;		m13 *= s;
		m20 *= s;		m21 *= s;		m22 *= s;		m23 *= s;
		m30 *= s;		m31 *= s;		m32 *= s;		m33 *= s;
	}
	KVector3d transform(const KVector3d& v) const {
		double x = m00 * v.x + m01 * v.y + m02 * v.z + m03;
		double y = m10 * v.x + m11 * v.y + m12 * v.z + m13;
		double z = m20 * v.x + m21 * v.y + m22 * v.z + m23;
		double w = m30 * v.x + m31 * v.y + m32 * v.z + m33;
		KVector3d result(x, y, z);
		result.scale(1 / w);
		return result;
	}
	KVector4d transform(const KVector4d& v) const {
		return KVector4d(
			m00 * v.x + m01 * v.y + m02 * v.z + m03 * v.w,
			m10 * v.x + m11 * v.y + m12 * v.z + m13 * v.w,
			m20 * v.x + m21 * v.y + m22 * v.z + m23 * v.w,
			m30 * v.x + m31 * v.y + m32 * v.z + m33 * v.w);
	}
	double determinant() const {
		return
			(m00*m11 - m01*m10)*(m22*m33 - m23*m32)
			-(m00*m12 - m02*m10)*(m21*m33 - m23*m31)
			+(m00*m13 - m03*m10)*(m21*m32 - m22*m31)
			+(m01*m12 - m02*m11)*(m20*m33 - m23*m30)
			-(m01*m13 - m03*m11)*(m20*m32 - m22*m30)
			+(m02*m13 - m03*m12)*(m20*m31 - m21*m30);
	}
	bool invert() {
		double s = determinant();
		if (s == 0.0) return false;
		s = 1/s;
		// alias-safe way.
		// less *,+,- calculation than expanded expression.
		set(
			m11*(m22*m33 - m23*m32) + m12*(m23*m31 - m21*m33) + m13*(m21*m32 - m22*m31),
			m21*(m02*m33 - m03*m32) + m22*(m03*m31 - m01*m33) + m23*(m01*m32 - m02*m31),
			m31*(m02*m13 - m03*m12) + m32*(m03*m11 - m01*m13) + m33*(m01*m12 - m02*m11),
			m01*(m13*m22 - m12*m23) + m02*(m11*m23 - m13*m21) + m03*(m12*m21 - m11*m22),

			m12*(m20*m33 - m23*m30) + m13*(m22*m30 - m20*m32) + m10*(m23*m32 - m22*m33),
			m22*(m00*m33 - m03*m30) + m23*(m02*m30 - m00*m32) + m20*(m03*m32 - m02*m33),
			m32*(m00*m13 - m03*m10) + m33*(m02*m10 - m00*m12) + m30*(m03*m12 - m02*m13),
			m02*(m13*m20 - m10*m23) + m03*(m10*m22 - m12*m20) + m00*(m12*m23 - m13*m22),

			m13*(m20*m31 - m21*m30) + m10*(m21*m33 - m23*m31) + m11*(m23*m30 - m20*m33),
			m23*(m00*m31 - m01*m30) + m20*(m01*m33 - m03*m31) + m21*(m03*m30 - m00*m33),
			m33*(m00*m11 - m01*m10) + m30*(m01*m13 - m03*m11) + m31*(m03*m10 - m00*m13),
			m03*(m11*m20 - m10*m21) + m00*(m13*m21 - m11*m23) + m01*(m10*m23 - m13*m20),

			m10*(m22*m31 - m21*m32) + m11*(m20*m32 - m22*m30) + m12*(m21*m30 - m20*m31),
			m20*(m02*m31 - m01*m32) + m21*(m00*m32 - m02*m30) + m22*(m01*m30 - m00*m31),
			m30*(m02*m11 - m01*m12) + m31*(m00*m12 - m02*m10) + m32*(m01*m10 - m00*m11),
			m00*(m11*m22 - m12*m21) + m01*(m12*m20 - m10*m22) + m02*(m10*m21 - m11*m20));
		scale(s);
		return true;
	}
	void mul(const KMatrix4d& m) {
		mul(*this, m);
	}
	void mul(const KMatrix4d& m1, const KMatrix4d& m2) {
		// alias-safe way
		set(
			m1.m00*m2.m00 + m1.m01*m2.m10 + m1.m02*m2.m20 + m1.m03*m2.m30,
			m1.m00*m2.m01 + m1.m01*m2.m11 + m1.m02*m2.m21 + m1.m03*m2.m31,
			m1.m00*m2.m02 + m1.m01*m2.m12 + m1.m02*m2.m22 + m1.m03*m2.m32,
			m1.m00*m2.m03 + m1.m01*m2.m13 + m1.m02*m2.m23 + m1.m03*m2.m33,
			
			m1.m10*m2.m00 + m1.m11*m2.m10 + m1.m12*m2.m20 + m1.m13*m2.m30,
			m1.m10*m2.m01 + m1.m11*m2.m11 + m1.m12*m2.m21 + m1.m13*m2.m31,
			m1.m10*m2.m02 + m1.m11*m2.m12 + m1.m12*m2.m22 + m1.m13*m2.m32,
			m1.m10*m2.m03 + m1.m11*m2.m13 + m1.m12*m2.m23 + m1.m13*m2.m33,
			
			m1.m20*m2.m00 + m1.m21*m2.m10 + m1.m22*m2.m20 + m1.m23*m2.m30,
			m1.m20*m2.m01 + m1.m21*m2.m11 + m1.m22*m2.m21 + m1.m23*m2.m31,
			m1.m20*m2.m02 + m1.m21*m2.m12 + m1.m22*m2.m22 + m1.m23*m2.m32,
			m1.m20*m2.m03 + m1.m21*m2.m13 + m1.m22*m2.m23 + m1.m23*m2.m33,
			
			m1.m30*m2.m00 + m1.m31*m2.m10 + m1.m32*m2.m20 + m1.m33*m2.m30,
			m1.m30*m2.m01 + m1.m31*m2.m11 + m1.m32*m2.m21 + m1.m33*m2.m31,
			m1.m30*m2.m02 + m1.m31*m2.m12 + m1.m32*m2.m22 + m1.m33*m2.m32,
			m1.m30*m2.m03 + m1.m31*m2.m13 + m1.m32*m2.m23 + m1.m33*m2.m33);
	}
	void mul(double scalar) {
		m00 *= scalar; m01 *= scalar;  m02 *= scalar; m03 *= scalar;
		m10 *= scalar; m11 *= scalar;  m12 *= scalar; m13 *= scalar;
		m20 *= scalar; m21 *= scalar;  m22 *= scalar; m23 *= scalar;
		m30 *= scalar; m31 *= scalar;  m32 *= scalar; m33 *= scalar;
	}

	void mul(double * result, const double* vec) const {
		result[0] = m00 * vec[0] + m01 * vec[1] + m02 * vec[2] + m03 * vec[3];
		result[1] = m10 * vec[0] + m11 * vec[1] + m12 * vec[2] + m13 * vec[3];
		result[2] = m20 * vec[0] + m21 * vec[1] + m22 * vec[2] + m23 * vec[3];
		result[3] = m30 * vec[0] + m31 * vec[1] + m32 * vec[2] + m33 * vec[3];
	}
	void transpose() {
		std::swap(m01, m10);
		std::swap(m02, m20);
		std::swap(m03, m30);
		std::swap(m12, m21);
		std::swap(m13, m31);
		std::swap(m23, m32);
	}
	void setIdentity() {
		m00 = 1;	m01 = 0;	m02 = 0;	m03 = 0;
		m10 = 0;	m11 = 1;	m12 = 0;	m13 = 0;
		m20 = 0;	m21 = 0;	m22 = 1;	m23 = 0;
		m30 = 0;	m31 = 0;	m32 = 0;	m33 = 1;
	}
	void rotX(double angle) {
		double c = cos(angle);
		double s = sin(angle);
		m00 = 1.0; m01 = 0.0; m02 = 0.0; m03 = 0.0;
		m10 = 0.0; m11 = c;   m12 = -s;  m13 = 0.0;
		m20 = 0.0; m21 = s;   m22 = c;   m23 = 0.0;
		m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 1.0; 
	}
	void rotY(double angle)  {
		double c = cos(angle);
		double s = sin(angle);
		m00 = c;   m01 = 0.0; m02 = s;   m03 = 0.0;
		m10 = 0.0; m11 = 1.0; m12 = 0.0; m13 = 0.0;
		m20 = -s;  m21 = 0.0; m22 = c;   m23 = 0.0;
		m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 1.0; 
    }
	void rotZ(double angle)  {
		double c = cos(angle);
		double s = sin(angle);
		m00 = c;   m01 = -s;  m02 = 0.0; m03 = 0.0;
		m10 = s;   m11 = c;   m12 = 0.0; m13 = 0.0;
		m20 = 0.0; m21 = 0.0; m22 = 1.0; m23 = 0.0;
		m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 1.0; 
    }
    void setTranslation(const KVector3d& trans) {
		m03 = trans.x;
        m13 = trans.y;  
		m23 = trans.z;
    }
    void setRotationScale(const KMatrix3d& m1) {
		m00 = m1.m00; m01 = m1.m01; m02 = m1.m02;
		m10 = m1.m10; m11 = m1.m11; m12 = m1.m12;
		m20 = m1.m20; m21 = m1.m21; m22 = m1.m22;
    }
    void setZero() {
        m00 = 0.0; m01 = 0.0; m02 = 0.0; m03 = 0.0;
        m10 = 0.0; m11 = 0.0; m12 = 0.0; m13 = 0.0;
        m20 = 0.0; m21 = 0.0; m22 = 0.0; m23 = 0.0;
        m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 0.0;
    }
    void setRotationFromAxisAngle(const KVector3d& axis, double angle) {
		setZero();
		KVector3d a(axis);
		a.normalize();
		double c = cos(angle);
		double s = sin(angle);
		double omc = 1.0 - c;
		m00 = c + a.x * a.x * omc;
		m11 = c + a.y * a.y * omc;
		m22 = c + a.z * a.z * omc;
		
		double tmp1 = a.x * a.y * omc;
		double tmp2 = a.z * s;
		m01 = tmp1 - tmp2;
		m10 = tmp1 + tmp2;
		
		tmp1 = a.x * a.z * omc;
		tmp2 = a.y * s;
		m02 = tmp1 + tmp2;
		m20 = tmp1 - tmp2;
		
		tmp1 = a.y * a.z * omc;
		tmp2 = a.x * s;
		m12 = tmp1 - tmp2;
		m21 = tmp1 + tmp2;
		
		m33 = 1;
    }
};

//class KVectorG {
//protected:
//	std::vector<double> m_data;
//	int m_size;
//public:
//	KVectorG() {}
//	KVectorG(const std::vector<double>& data) {
//		set(data);
//	}
//	KVectorG(int size) {
//		m_data.resize(size, 0);
//		m_size = size;
//	}
//	~KVectorG(void) {}
//	
//	int getSize() { return m_size; }
//	
//	void set(const std::vector<double>& data) {
//		m_data = data;
//		m_size = (int)m_data.size();
//	}
//	void sub(const KVectorG& v) {
//		if (v.m_size != m_size) throw "Incompatible sizes!";
//		for (int i = 0; i < m_size; ++i)
//			m_data[i] -= v.m_data[i];
//	}
//	double lengthSquared() const {
//		double result = 0;
//		for (int i = 0; i < m_size; ++i) {
//			result += m_data[i] * m_data[i];
//		}
//		return result;
//	}
//	double length() const {
//		return sqrt(lengthSquared());
//	}
//	void normalize() {
//		double len = length();
//		if (len == 0) return;
//		double _len = 1 / len;
//		for (int i = 0; i < m_size; ++i)
//			m_data[i] /= _len;
//	}
//	void scale(double s) {
//		for (int i = 0; i < m_size; ++i)
//			m_data[i] *;
//	}
//	void add(const KVector2d& v) {
//		x += v.x;
//		y += v.y;
//	}
//	void add(double _x, double _y) {
//		x += _x;
//		y += _y;
//	}
//	double dot (const KVector2d& v) {
//		return x * v.x + y * v.y;
//	}
//}
//
//class KMatrixG {
//protected:
//	std::vector<double> m_data;
//	int m_nRow, m_nCol;
//public:
//	
//	KMatrixG() {
//		set(0, 0);
//	}
//	KMatrixG(int nRow, int nCol) {
//		set(nRow, nCol);
//	}
//	KMatrixG(int nRow, int nCol, const std::vector<double>& data) {
//		set(nRow, nCol, data);
//	}
//	~KMatrixG() {}
//	
//	int getNumRow() { return m_nRow; }
//	int getNumCol() { return m_nCol; }
//
//	double operator[](int index) {
//		return m_data[index];
//	}
//	double* getPtr() const {
//		return &m_data[0];
//	}
//	void set(int nRow, int nCol, const std::vector<double>& data) {
//		m_nRow = nRow;
//		m_nCol = nCol;
//		m_data = data;
//	}
//	void set(int nRow, int nCol) {
//		m_nRow = nRow;
//		m_nCol = nCol;
//		m_data.clear();
//		m_data.resize(m_nRow * m_nCol, 0);
//	}
//	void scale(double s) {
//		for (int i = 0; i < (int)m_data.size(); ++i)
//			m_data[i] *= s;
//	}
//	KVector3d transform(const KVector3d& v) const {
//		double x = m00 * v.x + m01 * v.y + m02 * v.z + m03;
//		double y = m10 * v.x + m11 * v.y + m12 * v.z + m13;
//		double z = m20 * v.x + m21 * v.y + m22 * v.z + m23;
//		double w = m30 * v.x + m31 * v.y + m32 * v.z + m33;
//		KVector3d result(x, y, z);
//		result.scale(1 / w);
//		return result;
//	}
//	KVector4d transform(const KVector4d& v) const {
//		return KVector4d(
//			m00 * v.x + m01 * v.y + m02 * v.z + m03 * v.w,
//			m10 * v.x + m11 * v.y + m12 * v.z + m13 * v.w,
//			m20 * v.x + m21 * v.y + m22 * v.z + m23 * v.w,
//			m30 * v.x + m31 * v.y + m32 * v.z + m33 * v.w);
//	}
//	double determinant() const {
//		return
//			(m00*m11 - m01*m10)*(m22*m33 - m23*m32)
//			-(m00*m12 - m02*m10)*(m21*m33 - m23*m31)
//			+(m00*m13 - m03*m10)*(m21*m32 - m22*m31)
//			+(m01*m12 - m02*m11)*(m20*m33 - m23*m30)
//			-(m01*m13 - m03*m11)*(m20*m32 - m22*m30)
//			+(m02*m13 - m03*m12)*(m20*m31 - m21*m30);
//	}
//	bool invert() {
//		double s = determinant();
//		if (s == 0.0) return false;
//		s = 1/s;
//		// alias-safe way.
//		// less *,+,- calculation than expanded expression.
//		set(
//			m11*(m22*m33 - m23*m32) + m12*(m23*m31 - m21*m33) + m13*(m21*m32 - m22*m31),
//			m21*(m02*m33 - m03*m32) + m22*(m03*m31 - m01*m33) + m23*(m01*m32 - m02*m31),
//			m31*(m02*m13 - m03*m12) + m32*(m03*m11 - m01*m13) + m33*(m01*m12 - m02*m11),
//			m01*(m13*m22 - m12*m23) + m02*(m11*m23 - m13*m21) + m03*(m12*m21 - m11*m22),
//
//			m12*(m20*m33 - m23*m30) + m13*(m22*m30 - m20*m32) + m10*(m23*m32 - m22*m33),
//			m22*(m00*m33 - m03*m30) + m23*(m02*m30 - m00*m32) + m20*(m03*m32 - m02*m33),
//			m32*(m00*m13 - m03*m10) + m33*(m02*m10 - m00*m12) + m30*(m03*m12 - m02*m13),
//			m02*(m13*m20 - m10*m23) + m03*(m10*m22 - m12*m20) + m00*(m12*m23 - m13*m22),
//
//			m13*(m20*m31 - m21*m30) + m10*(m21*m33 - m23*m31) + m11*(m23*m30 - m20*m33),
//			m23*(m00*m31 - m01*m30) + m20*(m01*m33 - m03*m31) + m21*(m03*m30 - m00*m33),
//			m33*(m00*m11 - m01*m10) + m30*(m01*m13 - m03*m11) + m31*(m03*m10 - m00*m13),
//			m03*(m11*m20 - m10*m21) + m00*(m13*m21 - m11*m23) + m01*(m10*m23 - m13*m20),
//
//			m10*(m22*m31 - m21*m32) + m11*(m20*m32 - m22*m30) + m12*(m21*m30 - m20*m31),
//			m20*(m02*m31 - m01*m32) + m21*(m00*m32 - m02*m30) + m22*(m01*m30 - m00*m31),
//			m30*(m02*m11 - m01*m12) + m31*(m00*m12 - m02*m10) + m32*(m01*m10 - m00*m11),
//			m00*(m11*m22 - m12*m21) + m01*(m12*m20 - m10*m22) + m02*(m10*m21 - m11*m20));
//		scale(s);
//		return true;
//	}
//	void mul(const KMatrix4d& m) {
//		mul(*this, m);
//	}
//	void mul(const KMatrix4d& m1, const KMatrix4d& m2) {
//		// alias-safe way
//		set(
//			m1.m00*m2.m00 + m1.m01*m2.m10 + m1.m02*m2.m20 + m1.m03*m2.m30,
//			m1.m00*m2.m01 + m1.m01*m2.m11 + m1.m02*m2.m21 + m1.m03*m2.m31,
//			m1.m00*m2.m02 + m1.m01*m2.m12 + m1.m02*m2.m22 + m1.m03*m2.m32,
//			m1.m00*m2.m03 + m1.m01*m2.m13 + m1.m02*m2.m23 + m1.m03*m2.m33,
//			
//			m1.m10*m2.m00 + m1.m11*m2.m10 + m1.m12*m2.m20 + m1.m13*m2.m30,
//			m1.m10*m2.m01 + m1.m11*m2.m11 + m1.m12*m2.m21 + m1.m13*m2.m31,
//			m1.m10*m2.m02 + m1.m11*m2.m12 + m1.m12*m2.m22 + m1.m13*m2.m32,
//			m1.m10*m2.m03 + m1.m11*m2.m13 + m1.m12*m2.m23 + m1.m13*m2.m33,
//			
//			m1.m20*m2.m00 + m1.m21*m2.m10 + m1.m22*m2.m20 + m1.m23*m2.m30,
//			m1.m20*m2.m01 + m1.m21*m2.m11 + m1.m22*m2.m21 + m1.m23*m2.m31,
//			m1.m20*m2.m02 + m1.m21*m2.m12 + m1.m22*m2.m22 + m1.m23*m2.m32,
//			m1.m20*m2.m03 + m1.m21*m2.m13 + m1.m22*m2.m23 + m1.m23*m2.m33,
//			
//			m1.m30*m2.m00 + m1.m31*m2.m10 + m1.m32*m2.m20 + m1.m33*m2.m30,
//			m1.m30*m2.m01 + m1.m31*m2.m11 + m1.m32*m2.m21 + m1.m33*m2.m31,
//			m1.m30*m2.m02 + m1.m31*m2.m12 + m1.m32*m2.m22 + m1.m33*m2.m32,
//			m1.m30*m2.m03 + m1.m31*m2.m13 + m1.m32*m2.m23 + m1.m33*m2.m33);
//	}
//	void mul(double scalar) {
//		m00 *= scalar; m01 *= scalar;  m02 *= scalar; m03 *= scalar;
//		m10 *= scalar; m11 *= scalar;  m12 *= scalar; m13 *= scalar;
//		m20 *= scalar; m21 *= scalar;  m22 *= scalar; m23 *= scalar;
//		m30 *= scalar; m31 *= scalar;  m32 *= scalar; m33 *= scalar;
//	}
//
//	void mul(double * result, const double* vec) const {
//		result[0] = m00 * vec[0] + m01 * vec[1] + m02 * vec[2] + m03 * vec[3];
//		result[1] = m10 * vec[0] + m11 * vec[1] + m12 * vec[2] + m13 * vec[3];
//		result[2] = m20 * vec[0] + m21 * vec[1] + m22 * vec[2] + m23 * vec[3];
//		result[3] = m30 * vec[0] + m31 * vec[1] + m32 * vec[2] + m33 * vec[3];
//	}
//	void transpose() {
//		std::swap(m01, m10);
//		std::swap(m02, m20);
//		std::swap(m03, m30);
//		std::swap(m12, m21);
//		std::swap(m13, m31);
//		std::swap(m23, m32);
//	}
//	void setIdentity() {
//		m00 = 1;	m01 = 0;	m02 = 0;	m03 = 0;
//		m10 = 0;	m11 = 1;	m12 = 0;	m13 = 0;
//		m20 = 0;	m21 = 0;	m22 = 1;	m23 = 0;
//		m30 = 0;	m31 = 0;	m32 = 0;	m33 = 1;
//	}
//	void rotX(double angle) {
//		double c = cos(angle);
//		double s = sin(angle);
//		m00 = 1.0; m01 = 0.0; m02 = 0.0; m03 = 0.0;
//		m10 = 0.0; m11 = c;   m12 = -s;  m13 = 0.0;
//		m20 = 0.0; m21 = s;   m22 = c;   m23 = 0.0;
//		m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 1.0; 
//	}
//	void rotY(double angle)  {
//		double c = cos(angle);
//		double s = sin(angle);
//		m00 = c;   m01 = 0.0; m02 = s;   m03 = 0.0;
//		m10 = 0.0; m11 = 1.0; m12 = 0.0; m13 = 0.0;
//		m20 = -s;  m21 = 0.0; m22 = c;   m23 = 0.0;
//		m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 1.0; 
//    }
//	void rotZ(double angle)  {
//		double c = cos(angle);
//		double s = sin(angle);
//		m00 = c;   m01 = -s;  m02 = 0.0; m03 = 0.0;
//		m10 = s;   m11 = c;   m12 = 0.0; m13 = 0.0;
//		m20 = 0.0; m21 = 0.0; m22 = 1.0; m23 = 0.0;
//		m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 1.0; 
//    }
//    void setTranslation(const KVector3d& trans) {
//		m03 = trans.x;
//        m13 = trans.y;  
//		m23 = trans.z;
//    }
//    void setRotationScale(const KMatrix3d& m1) {
//		m00 = m1.m00; m01 = m1.m01; m02 = m1.m02;
//		m10 = m1.m10; m11 = m1.m11; m12 = m1.m12;
//		m20 = m1.m20; m21 = m1.m21; m22 = m1.m22;
//    }
//    void setZero() {
//        m00 = 0.0; m01 = 0.0; m02 = 0.0; m03 = 0.0;
//        m10 = 0.0; m11 = 0.0; m12 = 0.0; m13 = 0.0;
//        m20 = 0.0; m21 = 0.0; m22 = 0.0; m23 = 0.0;
//        m30 = 0.0; m31 = 0.0; m32 = 0.0; m33 = 0.0;
//    }
//    void setRotationFromAxisAngle(const KVector3d& axis, double angle) {
//		KVector3d a(axis);
//		a.normalize();
//		double c = cos(angle);
//		double s = sin(angle);
//		double omc = 1.0 - c;
//		m00 = c + a.x * a.x * omc;
//		m11 = c + a.y * a.y * omc;
//		m22 = c + a.z * a.z * omc;
//		
//		double tmp1 = a.x * a.y * omc;
//		double tmp2 = a.z * s;
//		m01 = tmp1 - tmp2;
//		m10 = tmp1 + tmp2;
//		
//		tmp1 = a.x * a.z * omc;
//		tmp2 = a.y * s;
//		m02 = tmp1 + tmp2;
//		m20 = tmp1 - tmp2;
//		
//		tmp1 = a.y * a.z * omc;
//		tmp2 = a.x * s;
//		m12 = tmp1 - tmp2;
//		m21 = tmp1 + tmp2;
//    }
//};
