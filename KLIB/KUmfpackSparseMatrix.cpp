#include "stdafx.h"
#include ".\kumfpacksparsematrix.h"
#include <algorithm>

using namespace std;

KUmfpackSparseMatrix::KUmfpackSparseMatrix() {
	init(0, 0, 0);
}

KUmfpackSparseMatrix::KUmfpackSparseMatrix(int rowSize, int colSize, int initDataSize) {
	init(rowSize, colSize, initDataSize);
}

void KUmfpackSparseMatrix::init(int rowSize, int colSize, int initDataSize) {
	m_rowSize = rowSize;
	m_colSize = colSize;
	m_Ap.clear();
	m_Ai.clear();
	m_Ax.clear();
	m_Ap.resize(m_colSize + 1, 0);
	m_Ai.reserve(initDataSize);
	m_Ax.reserve(initDataSize);
}

KUmfpackSparseMatrix KUmfpackSparseMatrix::transposedMultipliedBySelf() const {
	KUmfpackSparseMatrix result(m_colSize, m_colSize, (int)m_Ai.size());
	vector<vector<int>    > Ai(m_colSize);
	vector<vector<double> > Ax(m_colSize);
	for (int col1 = 0; col1 < m_colSize; ++col1) {
		int indexBegin1 = m_Ap[col1];
		int indexEnd1   = m_Ap[col1 + 1];
		for (int col2 = col1; col2 < m_colSize; ++col2) {
			int indexBegin2 = m_Ap[col2];
			int indexEnd2   = m_Ap[col2 + 1];
			double sum = 0;
			for (int index1 = indexBegin1, index2 = indexBegin2; index1 < indexEnd1 && index2 < indexEnd2; ) {
				int    row1 = m_Ai[index1];
				int    row2 = m_Ai[index2];
				double val1 = m_Ax[index1];
				double val2 = m_Ax[index2];
				if (row1 == row2) {
					sum += val1 * val2;
					++index1;
					++index2;
				} else if (row1 < row2) {
					++index1;
				} else {
					++index2;
				}
			}
			if (sum == 0) continue;
			Ai[col1].push_back(col2);
			Ax[col1].push_back(sum);
			//result.setValue(col1, col2, sum);
			if (col1 == col2) continue;
			Ai[col2].push_back(col1);
			Ax[col2].push_back(sum);
			//result.setValue(col2, col1, sum);
		}
	}
	int total = 0;
	for (int i = 0; i < m_colSize; ++i) {
		result.m_Ap[i] = total;
		total += (int)Ai[i].size();
	}
	result.m_Ap[m_colSize] = total;
	result.m_Ai.reserve(total);
	result.m_Ax.reserve(total);
	for (int i = 0; i < m_colSize; ++i) {
		for (int j = 0; j < (int)Ai[i].size(); ++j) {
			result.m_Ai.push_back(Ai[i][j]);
			result.m_Ax.push_back(Ax[i][j]);
		}
	}
	return result;
}

KUmfpackSparseMatrix KUmfpackSparseMatrix::transpose() const {
	KUmfpackSparseMatrix result(m_colSize, m_rowSize, (int)m_Ai.size());
	vector<vector<int>    > Ai(m_rowSize);
	vector<vector<double> > Ax(m_rowSize);
	for (int col = 0; col < m_colSize; ++col) {
		int indexBegin = m_Ap[col];
		int indexEnd   = m_Ap[col + 1];
		for (int index = indexBegin; index < indexEnd; ++index) {
			int    row = m_Ai[index];
			double val = m_Ax[index];
			Ai[row].push_back(col);
			Ax[row].push_back(val);
			//result.setValue(col, row, val);
		}
	}
	int total = 0;
	for (int i = 0; i < m_rowSize; ++i) {
		result.m_Ap[i] = total;
		total += (int)Ai[i].size();
	}
	result.m_Ap[m_rowSize] = total;
	result.m_Ai.reserve(total);
	result.m_Ax.reserve(total);
	for (int i = 0; i < m_rowSize; ++i) {
		for (int j = 0; j < (int)Ai[i].size(); ++j) {
			result.m_Ai.push_back(Ai[i][j]);
			result.m_Ax.push_back(Ax[i][j]);
		}
	}
	return result;
}

KUmfpackSparseMatrix KUmfpackSparseMatrix::mul(const KUmfpackSparseMatrix& mat) const {
	KUmfpackSparseMatrix result(m_rowSize, mat.m_colSize, (int)m_Ai.size());
	for (int col = 0; col < result.m_colSize; ++col) {
		int indexBegin = mat.m_Ap[col];
		int indexEnd   = mat.m_Ap[col + 1];
		for (int row = 0; row < m_rowSize; ++row) {
			double sum = 0;
			for (int index = indexBegin; index < indexEnd; ++index) {
				int    matRow = mat.m_Ai[index];
				double matVal = mat.m_Ax[index];
				sum += matVal * getValue(row, matRow);
			}
			if (sum == 0) continue;
			result.m_Ai.push_back(row);
			result.m_Ax.push_back(sum);
			for (int i = col + 1; i <= result.m_colSize; ++i)
				++result.m_Ap[i];
		}
	}
	return result;
}

vector<double> KUmfpackSparseMatrix::mul(const std::vector<double>& vec)  const {
	vector<double> result(m_rowSize, 0);
	for (int col = 0; col < m_colSize; ++col) {
		int indexBegin = m_Ap[col];
		int indexEnd   = m_Ap[col + 1];
		for (int index = indexBegin; index < indexEnd; ++index) {
			int    row = m_Ai[index];
			double val = m_Ax[index];
			result[row] += val * vec[col];
		}
	}
	return result;
}

void KUmfpackSparseMatrix::addValue(int row, int col, double val) {
	if (val == 0) return;
	vector<int>::iterator iterFirst = m_Ai.begin();
	vector<int>::iterator iterLast  = m_Ai.begin();
	advance(iterFirst, m_Ap[col]);
	advance(iterLast , m_Ap[col + 1]);
	pair<vector<int>::iterator, vector<int>::iterator> range = equal_range(iterFirst, iterLast, row);
	vector<double>::iterator iterVal = m_Ax.begin();
	advance(iterVal, (int)distance(m_Ai.begin(), range.first));
	if (range.first == range.second) {
		m_Ai.insert(range.first, row);
		m_Ax.insert(iterVal, val);
		for (int i = col + 1; i <= m_colSize; ++i)
			++m_Ap[i];
	} else {
		*iterVal += val;
	}
}

void KUmfpackSparseMatrix::setValue(int row, int col, double val) {
	vector<int>::iterator iterFirst = m_Ai.begin();
	vector<int>::iterator iterLast  = m_Ai.begin();
	advance(iterFirst, m_Ap[col]);
	advance(iterLast , m_Ap[col + 1]);
	pair<vector<int>::iterator, vector<int>::iterator> range = equal_range(iterFirst, iterLast, row);
	vector<double>::iterator iterVal = m_Ax.begin();
	advance(iterVal, (int)distance(m_Ai.begin(), range.first));
	if (range.first == range.second) {
		if (val == 0) return;
		m_Ai.insert(range.first, row);
		m_Ax.insert(iterVal, val);
		for (int i = col + 1; i <= m_colSize; ++i)
			++m_Ap[i];
	} else {
		*iterVal = val;
	}
}

double KUmfpackSparseMatrix::getValue(int row, int col) const {
	vector<int>::const_iterator iterFirst = m_Ai.begin();
	vector<int>::const_iterator iterLast  = m_Ai.begin();
	advance(iterFirst, m_Ap[col]);
	advance(iterLast , m_Ap[col + 1]);
	pair<vector<int>::const_iterator, vector<int>::const_iterator> range = equal_range(iterFirst, iterLast, row);
	if (range.first == range.second) return 0.;
	vector<double>::const_iterator iterVal = m_Ax.begin();
	advance(iterVal, (int)distance(m_Ai.begin(), range.first));
	return *iterVal;
}

ostream& operator<<(ostream& os, const KUmfpackSparseMatrix& mat) {
	for (int i = 0; i < mat.getRowSize(); ++i) {
		for (int j = 0; j < mat.getColSize(); ++j) {
			os << mat.getValue(i, j) << " ";
		}
		os << endl;
	}
	return os;
}
