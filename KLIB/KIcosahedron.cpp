#include "StdAfx.h"
#include "KIcosahedron.h"

#include <map>
#include <cmath>

using namespace std;

KIcosahedron::KIcosahedron(double radius)
: m_radius(radius)
{
	double a = 1. / sqrt(5.);
	double b = (1 - a) / 2;
	double c = (1 + a) / 2;
	double d = sqrt(b);
	double e = sqrt(c);
	KVector3d pos[12] = {
		KVector3d( 0,  1,  0 ),
		KVector3d( 0,  a,  2 * a),
		KVector3d( e,  a,  b ),
		KVector3d( d,  a, -c ),
		KVector3d(-d,  a, -c ),
		KVector3d(-e,  a,  b ),
		KVector3d( d, -a,  c ),
		KVector3d( e, -a, -b ),
		KVector3d( 0, -a, -2 * a),
		KVector3d(-e, -a, -b ),
		KVector3d(-d, -a,  c ),
		KVector3d( 0, -1,  0 )
	};
	for (int i = 0; i < 12; ++i)
		pos[i].scale(m_radius);
	m_vertices.reserve(12);
	for (int i = 0; i < 12; ++i)
		m_vertices.push_back(KVertex(pos[i]));
	KPolygon polygons[20] = {
		KPolygon(0, 1, 2),
		KPolygon(0, 2, 3),
		KPolygon(0, 3, 4),
		KPolygon(0, 4, 5),
		KPolygon(0, 5, 1),
		KPolygon(1, 6, 2),
		KPolygon(2, 7, 3),
		KPolygon(3, 8, 4),
		KPolygon(4, 9, 5),
		KPolygon(5, 10, 1),
		KPolygon(1, 10, 6),
		KPolygon(2, 6, 7),
		KPolygon(3, 7, 8),
		KPolygon(4, 8, 9),
		KPolygon(5, 9, 10),
		KPolygon(11, 6, 10),
		KPolygon(11, 7, 6),
		KPolygon(11, 8, 7),
		KPolygon(11, 9, 8),
		KPolygon(11, 10, 9)
	};
	m_polygons.reserve(20);
	for (int i = 0; i < 20; ++i)
		m_polygons.push_back(polygons[i]);
}

void KIcosahedron::subdivide() {
	vector<KVertex>  vertices = m_vertices;
	vector<KPolygon> polygons;
	map<int, int> mapEdgeID2Vtx;
	int N = (int)m_vertices.size();
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KPolygon& p = m_polygons[i];
		int vtxID[3] = {p.m_vtx[0], p.m_vtx[1], p.m_vtx[2]};
		int vtxID_new[3];
		for (int j = 0; j < 3; ++j) {
			int v0 = p.m_vtx[j];
			int v1 = p.m_vtx[(j + 1) % 3];
			int edgeID = N * min(v0, v1) + max(v0, v1);
			map<int, int>::iterator findPos = mapEdgeID2Vtx.find(edgeID);
			if (findPos == mapEdgeID2Vtx.end()) {
				vtxID_new[j] = (int)vertices.size();
				mapEdgeID2Vtx.insert(pair<int, int>(edgeID, vtxID_new[j]));
				KVector3d& p0 = m_vertices[v0].m_pos;
				KVector3d& p1 = m_vertices[v1].m_pos;
				KVector3d pos(p0);
				pos.add(p1);
				pos.normalize();
				pos.scale(m_radius);
				vertices.push_back(KVertex(pos));
			} else {
				vtxID_new[j] = findPos->second;
			}
		}
		polygons.push_back(KPolygon(vtxID[0], vtxID_new[0], vtxID_new[2]));
		polygons.push_back(KPolygon(vtxID[1], vtxID_new[1], vtxID_new[0]));
		polygons.push_back(KPolygon(vtxID[2], vtxID_new[2], vtxID_new[1]));
		polygons.push_back(KPolygon(vtxID_new[0], vtxID_new[1], vtxID_new[2]));
	}
	m_vertices = vertices;
	m_polygons = polygons;
}


