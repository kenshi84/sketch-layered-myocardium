#include "StdAfx.h"
#include "KRBF.h"

//#include <boost/numeric/ublas/triangular.hpp>
//#include <boost/numeric/ublas/lu.hpp>

#include <iostream>

#include <Eigen/Dense>

using namespace std;
using namespace Eigen;
//using namespace boost::numeric;

////////////////////////////////////////////////////////////////////////////////////////////
//
// ThinPlate3DConstant
//
////////////////////////////////////////////////////////////////////////////////////////////
void KThinPlate3DConstant::setPoints(const vector<KVector3d>& points) {
	m_points = points;
	int N = (int)m_points.size();
	int L = N + 1;
	m_A.clear();
	m_A.resize(L * L, 0);
	for (int i = 0; i < N; ++i) {
		for (int j = i + 1; j < N; ++j) {
			double d = phi(m_points[i], m_points[j]);
			m_A[i * L + j] = m_A[j * L + i] = d;
		}
		m_A[i * L + N] = m_A[N * L + i] = 1;
	}
}

double KThinPlate3DConstant::getValue(const double x, const double y, const double z) const {
	double sum = 0;
	for (int i = 0; i < (int)m_points.size(); ++i) {
		sum += m_coefficients[i] * phi(m_points[i].x, m_points[i].y, m_points[i].z, x, y, z);
	}
	sum += m_coefficients.back();
	return sum;
}

void KThinPlate3DConstant::setValues(const std::vector<double>& values) {
	int N = (int)m_points.size();
	int L = N + 1;
	m_coefficients.clear();
	m_coefficients.resize(L, 0);
	for (int i = 0; i < N; ++i)
		m_coefficients[i] = values[i];

	//long n = L, nrhs = 1, lda = L, ldb = L,info;
	//vector<long> ipiv(L);
	//dgesv_(&n, &nrhs, &m_A[0], &lda, &ipiv[0], &m_coefficients[0], &ldb, &info);

	ColPivHouseholderQR<MatrixXd> solver(Map<MatrixXd>(m_A.data(), L, L));
	VectorXd x = solver.solve(Map<VectorXd>(m_coefficients.data(), L));
	for (int i = 0; i < L; ++i)
		m_coefficients[i] = x[i];
}






////////////////////////////////////////////////////////////////////////////////////////////
//
// ThinPlate3D
//
////////////////////////////////////////////////////////////////////////////////////////////
void KThinPlate3D::setPoints(const vector<KVector3d>& points) {
	m_points = points;
	int N = (int)m_points.size();
	int L = N + 4;
	//m_A = dmatrix(L, L);
	m_A.clear();
	m_A.resize(L * L, 0);
	for (int i = 0; i < N; ++i) {
		KVector3d& pi = m_points[i];
		for (int j = i + 1; j < N; ++j) {
			KVector3d& pj = m_points[j];
			double d = phi(pi, pj);
			m_A[i * L + j] = m_A[j * L + i] = d;
			//m_A(i, j) = m_A(j, i) = d;
		}
		m_A[i * L + N    ] = m_A[ N      * L + i] = 1;
		m_A[i * L + N + 1] = m_A[(N + 1) * L + i] = pi.x;
		m_A[i * L + N + 2] = m_A[(N + 2) * L + i] = pi.y;
		m_A[i * L + N + 3] = m_A[(N + 3) * L + i] = pi.z;
		//m_A(i, N    ) = m_A(N    , i) = 1;
		//m_A(i, N + 1) = m_A(N + 1, i) = pi.x;
		//m_A(i, N + 2) = m_A(N + 2, i) = pi.x;
		//m_A(i, N + 3) = m_A(N + 3, i) = pi.x;
	}
	//for (int i = 0; i < 4; ++i)
	//	for (int j = i; j < 4; ++j)
	//		m_A(N + i, N + j) = m_A(N + j, N + i) = 0;
	//ublas::permutation_matrix<> pm(L);
	//std::cout << "lu_factorize...";
	//ublas::lu_factorize(m_A, pm);
	//std::cout << "done." << std::endl;
}

void KThinPlate3D::setValues(const std::vector<double>& values) {
	if (m_points.size() != values.size()) throw "Inconsistent sizes!";
	int N = (int)m_points.size();
	int L = N + 4;
	m_coefficients.clear();
	m_coefficients.resize(L, 0);
	//m_coefficients = dvector(L);
	for (int i = 0; i < N; ++i)
		m_coefficients[i] = values[i];
	//for (int i = 0; i < 4; ++i)
	//	m_coefficients(N + i) = 0;
	//ublas::permutation_matrix<> pm(m_A.size1());
	//ublas::lu_factorize(m_A, pm);
	//ublas::lu_substitute(m_A, pm, m_coefficients);

	//long int n = (long int)L;
	//long int nrhs = 1;
	//long int lda = (long int)L;
	//long int ldb = (long int)L;
	//long int info;
	//vector<long int> ipiv(L);
	//dgesv_(&n, &nrhs, &m_A[0], &lda, &ipiv[0], &m_coefficients[0], &ldb, &info);

	ColPivHouseholderQR<MatrixXd> solver(Map<MatrixXd>(m_A.data(), L, L));
	VectorXd x = solver.solve(Map<VectorXd>(m_coefficients.data(), L));
	for (int i = 0; i < L; ++i)
		m_coefficients[i] = x[i];

	m_isReady = true;
	//info = info;
}

double KThinPlate3D::getValue(const double x, const double y, const double z) const {
	int N = (int)m_points.size();
	double sum = 0;
	for (int i = 0; i < N; ++i) {
		sum += m_coefficients[i] * phi(m_points[i].x, m_points[i].y, m_points[i].z, x, y, z);
	}
	sum += m_coefficients[N];
	sum += m_coefficients[N + 1] * x;
	sum += m_coefficients[N + 2] * y;
	sum += m_coefficients[N + 3] * z;
	return sum;
}


////////////////////////////////////////////////////////////////////////////////////////////
//
// ThinPlate2D
//
////////////////////////////////////////////////////////////////////////////////////////////
void KThinPlate2D::setPoints(const vector<KVector2d>& points) {
	m_points = points;
	int N = (int)m_points.size();
	int L = N + 3;
	m_A.clear();
	m_A.resize(L * L, 0);
	for (int i = 0; i < N; ++i) {
		KVector2d& pi = m_points[i];
		for (int j = i + 1; j < N; ++j) {
			KVector2d& pj = m_points[j];
			double d = phi(pi, pj);
			m_A[i * L + j] = m_A[j * L + i] = d;
		}
		m_A[i * L + N    ] = m_A[ N      * L + i] = 1;
		m_A[i * L + N + 1] = m_A[(N + 1) * L + i] = pi.x;
		m_A[i * L + N + 2] = m_A[(N + 2) * L + i] = pi.y;
	}
}

void KThinPlate2D::setValues(const std::vector<double>& values) {
	int N = (int)m_points.size();
	int L = N + 3;
	m_coefficients.clear();
	m_coefficients.resize(L, 0);
	for (int i = 0; i < N; ++i)
		m_coefficients[i] = values[i];

	//long int n = (long int)L;
	//long int nrhs = 1;
	//long int lda = (long int)L;
	//long int ldb = (long int)L;
	//long int info;
	//vector<long int> ipiv(L);
	//dgesv_(&n, &nrhs, &m_A[0], &lda, &ipiv[0], &m_coefficients[0], &ldb, &info);

	ColPivHouseholderQR<MatrixXd> solver(Map<MatrixXd>(m_A.data(), L, L));
	VectorXd x = solver.solve(Map<VectorXd>(m_coefficients.data(), L));
	for (int i = 0; i < L; ++i)
		m_coefficients[i] = x[i];
}

double KThinPlate2D::getValue(const double x, const double y) const {
	int N = (int)m_points.size();
	double sum = 0;
	for (int i = 0; i < N; ++i) {
		sum += m_coefficients[i] * phi(m_points[i].x, m_points[i].y, x, y);
	}
	sum += m_coefficients[N];
	sum += m_coefficients[N + 1] * x;
	sum += m_coefficients[N + 2] * y;
	return sum;
}


////////////////////////////////////////////////////////////////////////////////////////////
//
// ThinPlate2DConstant
//
////////////////////////////////////////////////////////////////////////////////////////////
void KThinPlate2DConstant::setPoints(const vector<KVector2d>& points) {
	m_points = points;
	int N = (int)m_points.size();
	int L = N + 1;
	m_A.clear();
	m_A.resize(L * L, 0);
	for (int i = 0; i < N; ++i) {
		KVector2d& pi = m_points[i];
		for (int j = i + 1; j < N; ++j) {
			KVector2d& pj = m_points[j];
			double d = phi(pi, pj);
			m_A[i * L + j] = m_A[j * L + i] = d;
		}
		m_A[i * L + N    ] = m_A[ N      * L + i] = 1;
	}
}

void KThinPlate2DConstant::setValues(const std::vector<double>& values) {
	int N = (int)m_points.size();
	int L = N + 1;
	m_coefficients.clear();
	m_coefficients.resize(L, 0);
	for (int i = 0; i < N; ++i)
		m_coefficients[i] = values[i];

	//long int n = (long int)L;
	//long int nrhs = 1;
	//long int lda = (long int)L;
	//long int ldb = (long int)L;
	//long int info;
	//vector<long int> ipiv(L);
	//vector<double> A_tmp = m_A;
	//dgesv_(&n, &nrhs, &A_tmp[0], &lda, &ipiv[0], &m_coefficients[0], &ldb, &info);

	ColPivHouseholderQR<MatrixXd> solver(Map<MatrixXd>(m_A.data(), L, L));
	VectorXd x = solver.solve(Map<VectorXd>(m_coefficients.data(), L));
	for (int i = 0; i < L; ++i)
		m_coefficients[i] = x[i];
}

double KThinPlate2DConstant::getValue(const double x, const double y) const {
	int N = (int)m_points.size();
	double sum = 0;
	for (int i = 0; i < N; ++i) {
		sum += m_coefficients[i] * phi(m_points[i].x, m_points[i].y, x, y);
	}
	sum += m_coefficients[N];
	return sum;
}
