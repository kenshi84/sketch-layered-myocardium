#pragma once
#pragma warning(disable: 4018)

#include <vector>
#include <map>
#include <cfloat>
#include <string>

#include "KMath.h"
#include "KGeometry.h"
#include "KMultiTexGeometry.h"

class KUtil {
	KUtil() {};
	~KUtil() {};
public:
	static void getLineIntersection(const KVector2d& start0, const KVector2d& end0, const KVector2d& start1, const KVector2d& end1, 
								double& t_start0, double& t_end0, double& t_start1, double& t_end1);
	static double getLoopArea(const std::vector<KVector2d>& loop) {
		double area = 0;
		for (int i = 0; i < loop.size(); ++i) {
			const KVector2d& v0 = loop[i];
			const KVector2d& v1 = loop[(i + 1) % loop.size()];
			area += 0.5 * v0.cross(v1);
		}
		return area;
	}
	static double angle(const KVector2d& a, const KVector2d& o, const KVector2d& b) {
		KVector2d oa(a);
		KVector2d ob(b);
		oa.sub(o);
		ob.sub(o);
		oa.normalize();
		ob.normalize();
		return acos(oa.dot(ob));
	}
	static double atan2_zero(double y, double x) {
		double theta = atan2(y, x);
		if (theta < 0) theta += 2 * M_PI;
		return theta;
	}
	static double vector_max(const std::vector<double>& vec) {
		double result = -DBL_MAX;
		for (int i = 0; i < vec.size(); ++i) if (result < vec[i]) result = vec[i];
		return result;
	}
	static double vector_min(const std::vector<double>& vec) {
		double result = DBL_MAX;
		for (int i = 0; i < vec.size(); ++i) if (result > vec[i]) result = vec[i];
		return result;
	}
	static double vector_avg(const std::vector<double>& vec) {
		double result = 0;
		for (int i = 0; i < vec.size(); ++i) result += vec[i];
		return result / vec.size();
	}
	static double vector_sum(const std::vector<double>& vec) {
		double result = 0;
		for (int i = 0; i < vec.size(); ++i) result += vec[i];
		return result;
	}
	static KVector2d vector_avg(const std::vector<KVector2d>& vec) {
		KVector2d result;
		for (int i = 0; i < vec.size(); ++i) result.add(vec[i]);
		result.scale(1. / vec.size());
		return result;
	}
	static void vector_add(std::vector<double>& vec, double val) {
		for (int i = 0; i < vec.size(); ++i) {
			vec[i] += val;
		}
	}
	static void vector_scale(std::vector<double>& vec, double scale) {
		for (int i = 0; i < vec.size(); ++i) {
			vec[i] *= scale;
		}
	}
	static void flip_bool(bool& b) { b = !b; }
	static double getCurveLength(const std::vector<KVector2d>& curve);
	static std::vector<KVector2d> resampleCurve(const std::vector<KVector2d>& curve, bool is_loop = false);
	static std::vector<KVector2d> resampleCurve(const std::vector<KVector2d>& curve, unsigned int num_tgt_vtx, bool is_loop = false);
	static std::vector<KVector2d> reverseCurve(const std::vector<KVector2d>& curve, bool is_loop = false);
	static double calcTrilinear(
		double v000, double v100, double v010, double v110,
		double v001, double v101, double v011, double v111,
		double tx, double ty, double tz)
	{
		return 
			(1 - tx) * (1 - ty) * (1 - tz) * v000 +
			     tx  * (1 - ty) * (1 - tz) * v100 +
			(1 - tx) *      ty  * (1 - tz) * v010 +
			     tx  *      ty  * (1 - tz) * v110 +
			(1 - tx) * (1 - ty) *      tz  * v001 +
			     tx  * (1 - ty) *      tz  * v101 +
			(1 - tx) *      ty  *      tz  * v011 +
			     tx  *      ty  *      tz  * v111;
	}
	static double calcBilinear(
		double v00, double v10, double v01, double v11,
		double tx, double ty)
	{
		return 
			(1 - tx) * (1 - ty) * v00 +
			     tx  * (1 - ty) * v10 +
			(1 - tx) *      ty  * v01 +
			     tx  *      ty  * v11;
	}
	static double bernstein(int n, int i, double t) {
		return combination(n, i) * pow((1 - t), n - i) * pow(t, i);
	}
	static int permutation(int n, int r) {
		int result = 1;
		for (int i = 0; i < r; ++i) {
			result *= n;
			--n;
		}
		return result;
	}
	static double combination(int n, int r) {
		double result = 1;
		for (int i = 0; i < r; ++i) {
			result *= n / (i + 1.);
			--n;
		}
		return result;
	}
	static void translateTetraModel(KTetraModel& tetra, const KVector3d& t) {
		for (int i = 0; i < (int)tetra.m_vertices.size(); ++i) {
			tetra.m_vertices[i].m_pos.add(t);
		}
	}
	static void scaleTetraModel(KTetraModel& tetra, double s) {
		for (int i = 0; i < (int)tetra.m_vertices.size(); ++i) {
			tetra.m_vertices[i].m_pos.scale(s);
		}
	}
	static void translatePolygonModel(KPolygonModel& poly, const KVector3d& t) {
		for (int i = 0; i < (int)poly.m_vertices.size(); ++i) {
			poly.m_vertices[i].m_pos.add(t);
		}
	}
	static void scalePolygonModel(KPolygonModel& poly, double s) {
		for (int i = 0; i < (int)poly.m_vertices.size(); ++i) {
			poly.m_vertices[i].m_pos.scale(s);
		}
	}
	static void normalizeTetraModelIntoUnitBox(KTetraModel& tetra, double margin_percent = 0.);
	static void normalizeTetraModelIntoUnitBox(KMultiTexTetraModel& tetra);
	static double getDistanceSquared(const KVector3d& pos0, const KVector3d& pos1) {
		double dx = pos0.x - pos1.x;
		double dy = pos0.y - pos1.y;
		double dz = pos0.z - pos1.z;
		return dx * dx + dy * dy + dz * dz;
	}
	static double getDistanceSquared(const KVector2d& pos0, const KVector2d& pos1) {
		double dx = pos0.x - pos1.x;
		double dy = pos0.y - pos1.y;
		return dx * dx + dy * dy;
	}
	static double getDistance(const KVector3d& pos0, const KVector3d& pos1) {
		return sqrt(getDistanceSquared(pos0, pos1));
	}
	static double getDistance(const KVector2d& pos0, const KVector2d& pos1) {
		return sqrt(getDistanceSquared(pos0, pos1));
	}
	static void getVectorRotatedAroundAxis(KVector3d& vec, const KVector3d& axis, double angle) {
		KVector3d axis1, axis2;
		axis2.cross(axis, vec);
		axis2.normalize();
		axis1.cross(axis2, axis);
		double w0 = vec.dot(axis);
		double r  = vec.dot(axis1);
		double w1 = r * cos(angle);
		double w2 = r * sin(angle);
		vec.set(0, 0, 0);
		vec.addWeighted(axis, w0);
		vec.addWeighted(axis1, w1);
		vec.addWeighted(axis2, w2);
	}
	static void getNearestPoint(int& resID, double& resDist, const std::vector<KVector2d>& points, const KVector2d& query) {
		resID = -1;
		resDist = DBL_MAX;
		for (int i = 0; i < (int)points.size(); ++i) {
			double dist = query.sub_return(points[i]).length();
			if (dist < resDist) {
				resID = i;
				resDist = dist;
			}
		}
	}
	static void getNearestTetraID(int& resID, double& resDist, const KMultiTexTetraModel& tetra, const KVector3d& pos) {
		resID = -1;
		resDist = DBL_MAX;
		for (int i = 0; i < (int)tetra.m_tetras.size(); ++i) {
			const KMultiTexTetra& tet = tetra.m_tetras[i];
			KVector3d c;
			for (int j = 0; j < 4; ++j) {
				const KVector3d& vtxPos = tetra.m_vertices[tet.m_vtx[j]].m_pos;
				c.addWeighted(vtxPos, 0.25);
			}
			c.sub(pos);
			double distSq = c.lengthSquared();
			if (distSq < resDist) {
				resDist = distSq;
				resID   = i;
			}
		}
	}
		static void getNearestTetraID(int& resID, double& resDist, const KTetraModel& tetra, const KVector3d& pos) {
		resID = -1;
		resDist = DBL_MAX;
		for (int i = 0; i < (int)tetra.m_tetras.size(); ++i) {
			const KTetra& tet = tetra.m_tetras[i];
			KVector3d c;
			for (int j = 0; j < 4; ++j) {
				const KVector3d& vtxPos = tetra.m_vertices[tet.m_vtx[j]].m_pos;
				c.addWeighted(vtxPos, 0.25);
			}
			c.sub(pos);
			double distSq = c.lengthSquared();
			if (distSq < resDist) {
				resDist = distSq;
				resID   = i;
			}
		}
	}
	static void getBoundingBox(const KMultiTexTetraModel& tetra, double& xMin, double& yMin, double& zMin, double& xMax, double& yMax, double& zMax) {
		xMin = yMin = zMin =  DBL_MAX;
		xMax = yMax = zMax = -DBL_MAX;
		for (int i = 0; i < (int)tetra.m_vertices.size(); ++i) {
			const KVector3d& pos = tetra.m_vertices[i].m_pos;
			xMin = min(xMin, pos.x);
			yMin = min(yMin, pos.y);
			zMin = min(zMin, pos.z);
			xMax = max(xMax, pos.x);
			yMax = max(yMax, pos.y);
			zMax = max(zMax, pos.z);
		}
	}
	static void getBoundingBox(const KTetraModel& tetra, double& xMin, double& yMin, double& zMin, double& xMax, double& yMax, double& zMax) {
		xMin = yMin = zMin =  DBL_MAX;
		xMax = yMax = zMax = -DBL_MAX;
		for (int i = 0; i < (int)tetra.m_vertices.size(); ++i) {
			const KVector3d& pos = tetra.m_vertices[i].m_pos;
			xMin = min(xMin, pos.x);
			yMin = min(yMin, pos.y);
			zMin = min(zMin, pos.z);
			xMax = max(xMax, pos.x);
			yMax = max(yMax, pos.y);
			zMax = max(zMax, pos.z);
		}
	}
	static void getBoundingBox(const KMultiTexPolygonModel& poly, double& xMin, double& yMin, double& zMin, double& xMax, double& yMax, double& zMax) {
		xMin = yMin = zMin =  DBL_MAX;
		xMax = yMax = zMax = -DBL_MAX;
		for (int i = 0; i < (int)poly.m_vertices.size(); ++i) {
			const KVector3d& pos = poly.m_vertices[i].m_pos;
			xMin = min(xMin, pos.x);
			yMin = min(yMin, pos.y);
			zMin = min(zMin, pos.z);
			xMax = max(xMax, pos.x);
			yMax = max(yMax, pos.y);
			zMax = max(zMax, pos.z);
		}
	}
	static void getBoundingBox(const KPolygonModel& poly, double& xMin, double& yMin, double& zMin, double& xMax, double& yMax, double& zMax) {
		xMin = yMin = zMin =  DBL_MAX;
		xMax = yMax = zMax = -DBL_MAX;
		for (int i = 0; i < (int)poly.m_vertices.size(); ++i) {
			const KVector3d& pos = poly.m_vertices[i].m_pos;
			xMin = min(xMin, pos.x);
			yMin = min(yMin, pos.y);
			zMin = min(zMin, pos.z);
			xMax = max(xMax, pos.x);
			yMax = max(yMax, pos.y);
			zMax = max(zMax, pos.z);
		}
	}
	static void getBoundingBox(const KPolygonModel2D& poly, double& xMin, double& yMin, double& xMax, double& yMax) {
		xMin = yMin =  DBL_MAX;
		xMax = yMax = -DBL_MAX;
		for (int i = 0; i < (int)poly.m_vertices.size(); ++i) {
			const KVector2d& pos = poly.m_vertices[i].m_pos;
			xMin = min(xMin, pos.x);
			yMin = min(yMin, pos.y);
			xMax = max(xMax, pos.x);
			yMax = max(yMax, pos.y);
		}
	}
	static void getBoundingBox(const KPolygonModel2D& poly, KVector2d& bbmin, KVector2d& bbmax) {
		getBoundingBox(poly, bbmin.x, bbmin.y, bbmax.x, bbmax.y);
	}
	static void getBoundingBox(const KPolygonModel& poly, KVector3d& bbmin, KVector3d& bbmax) {
		getBoundingBox(poly, bbmin.x, bbmin.y, bbmin.z, bbmax.x, bbmax.y, bbmax.z);
	}
	static double getBoundingBoxDiagonalLength(const KMultiTexTetraModel& tetra) {
		double xMin, yMin, zMin, xMax, yMax, zMax;
		getBoundingBox(tetra, xMin, yMin, zMin, xMax, yMax, zMax);
		KVector3d d(xMax - xMin, yMax - yMin, zMax - zMin);
		return d.length();
	}
	static double getBoundingBoxDiagonalLength(const KTetraModel& tetra) {
		double xMin, yMin, zMin, xMax, yMax, zMax;
		getBoundingBox(tetra, xMin, yMin, zMin, xMax, yMax, zMax);
		KVector3d d(xMax - xMin, yMax - yMin, zMax - zMin);
		return d.length();
	}
	static void getBarycentricCoord(
		double& d0, double& d1, double& d2, double& d3,
		const KVector3d& v0, const KVector3d& v1, const KVector3d& v2, const KVector3d& v3,
		const KVector3d& w,
		const double coord_sum = 1		// 1:position 0:vector
	) {
		/* 
		 * [input]
		 *   v0, v1, v2, v3: 4 tetra vtx
		 *   w: target vector
		 * [output]
		 *   d0, d1, d2, d3: barycentric coordinate
		 *-----------------------------------------------
		 * d0 * v0 + d1 * v1 + d2 *v2 + d3 *v3 = w
		 * d0      + d1      + d2     + d3     = coord_sum
		 * 
		 * | v0.x  v1.x  v2.x  v2.x |   | d0 |  | w.x       |
		 * | v0.y  v1.y  v2.y  v2.y | * | d1 |= | w.y       |
		 * | v0.z  v1.z  v2.z  v2.z |   | d2 |  | w.z       |
		 * |  1     1     1     1   |   | d3 |  | coord_sum |
		 * 
		 */
		KMatrix4d mat(
			v0.x, v1.x, v2.x, v3.x,
			v0.y, v1.y, v2.y, v3.y,
			v0.z, v1.z, v2.z, v3.z,
			1,    1,    1,    1
		);
		mat.invert();
		double x[4];
		double b[4] = {w.x, w.y, w.z, coord_sum};
		mat.mul(x, b);
		d0 = x[0];
		d1 = x[1];
		d2 = x[2];
		d3 = x[3];
	}
	static void getBarycentricCoord(
		double& d0, double& d1, double& d2,
		const KVector2d& v0, const KVector2d& v1, const KVector2d& v2,
		const KVector2d& w,
		const double coord_sum = 1		// 1:position 0:vector
	) {
		/* 
		 * [input]
		 *   v0, v1, v2: 3 face vtx (2D)
		 *   w: target 2D vector
		 * [output]
		 *   d0, d1, d2: barycentric coordinate
		 *-----------------------------------------------
		 * d0 * v0 + d1 * v1 + d2 * v2 = w
		 * d0      + d1      + d2      = coord_sum
		 * 
		 * | v0.x  v1.x  v2.x  |   | d0 |  | w.x       |
		 * | v0.y  v1.y  v2.y  | * | d1 |= | w.y       |
		 * |  1     1     1    |   | d2 |  | coord_sum |
		 * 
		 */
		KMatrix3d mat(
			v0.x, v1.x, v2.x,
			v0.y, v1.y, v2.y,
			1,    1,    1
		);
		mat.invert();
		double x[3];
		double b[3] = {w.x, w.y, coord_sum};
		mat.mul(x, b);
		d0 = x[0];
		d1 = x[1];
		d2 = x[2];
	}
	static void getBarycentricCoord(
		double& d0, double& d1, double& d2,
		const KVector3d& v0, const KVector3d& v1, const KVector3d& v2,
		const KVector3d& w,
		const double coord_sum = 1		// 1:position 0:vector
	);
	static double calcAngle(const KVector3d& v0, const KVector3d& v1) {
		double cosa = v0.dot(v1) / (v0.length() * v1.length());
		return acos(cosa);
	}
	static double calcArea(const KVector3d& v0, const KVector3d& v1, const KVector3d& v2) {
		KVector3d n01, n12, n20;
		n01.cross(v0, v1);
		n12.cross(v1, v2);
		n20.cross(v2, v0);
		n01.add(n12);
		n01.add(n20);
		return 0.5 * n01.length();
	}
	static double calcArea(const KVector2d& v0, const KVector2d& v1, const KVector2d& v2) {
		double a = getDistance(v0, v1);
		double b = getDistance(v1, v2);
		double c = getDistance(v2, v0);
		double s = 0.5 * (a + b + c);
		return sqrt(abs(s * (s - a) * (s - b) * (s - c)));
	}
	// identical to getBarycentricCoord()
	//static void getLinearCoefficient(
	//	double& w0, double& w1, double& w2, double& w3,
	//	const KVector3d& v0, const KVector3d& v1, const KVector3d& v2, const KVector3d& v3,
	//	const KVector3d& pos) {
	//	/*
	//	calculate weights w0, w1, w2, w3 which satisfies:
	//		w0 * v0 + w1 * v1 + w2 * v2 + w3 * v3 = pos
	//		w0 + w1 + w2 + w3 = 1
	//	*/
	//	KMatrix4d A(
	//		v0.x, v1.x, v2.x, v3.x, 
	//		v0.y, v1.y, v2.y, v3.y, 
	//		v0.z, v1.z, v2.z, v3.z,
	//		1,    1,    1,    1);
	//	A.invert();
	//	double b[4] = {pos.x, pos.y, pos.z, 1};
	//	double x[4];
	//	A.mul(x, b);
	//	w0 = x[0];
	//	w1 = x[1];
	//	w2 = x[2];
	//	w3 = x[3];
	//}
	static void getBarycenter(KVector3d& dst, const KTetraModel& tetra, const KTetra& tet) {
		const KVector3d& v0 = tetra.m_vertices[tet.m_vtx[0]].m_pos;
		const KVector3d& v1 = tetra.m_vertices[tet.m_vtx[1]].m_pos;
		const KVector3d& v2 = tetra.m_vertices[tet.m_vtx[2]].m_pos;
		const KVector3d& v3 = tetra.m_vertices[tet.m_vtx[3]].m_pos;
		getBarycenter(dst, v0, v1, v2, v3);
	}
	static void getBarycenter(KVector3d& dst, const KMultiTexTetraModel& tetra, const KMultiTexTetra& tet) {
		const KVector3d& v0 = tetra.m_vertices[tet.m_vtx[0]].m_pos;
		const KVector3d& v1 = tetra.m_vertices[tet.m_vtx[1]].m_pos;
		const KVector3d& v2 = tetra.m_vertices[tet.m_vtx[2]].m_pos;
		const KVector3d& v3 = tetra.m_vertices[tet.m_vtx[3]].m_pos;
		getBarycenter(dst, v0, v1, v2, v3);
	}
	static void getBarycenter(KVector3d& dst, const KMultiTexPolygonModel& poly, const KMultiTexPolygon& p) {
		const KVector3d& v0 = poly.m_vertices[p.m_vtx[0]].m_pos;
		const KVector3d& v1 = poly.m_vertices[p.m_vtx[1]].m_pos;
		const KVector3d& v2 = poly.m_vertices[p.m_vtx[2]].m_pos;
		getBarycenter(dst, v0, v1, v2);
	}
	static void getBarycenter(KVector3d& dst, const KVector3d& p0, const KVector3d& p1, const KVector3d& p2, const KVector3d& p3) {
		dst.set(p0);
		dst.add(p1);
		dst.add(p2);
		dst.add(p3);
		dst.scale(1 / 4.);
	}
	static void getBarycenter(KVector3d& dst, const KVector3d& p0, const KVector3d& p1, const KVector3d& p2) {
		dst.set(p0);
		dst.add(p1);
		dst.add(p2);
		dst.scale(1 / 3.);
	}
	static void getBarycenter(KVector2d& dst, const KVector2d& p0, const KVector2d& p1, const KVector2d& p2) {
		dst.set(p0);
		dst.add(p1);
		dst.add(p2);
		dst.scale(1 / 3.);
	}
	static double distanceFromLineToPoint(const KVector2d& start, const KVector2d& ori, const KVector2d& pos) {
		KVector2d d(ori);
		d.normalize();
		KVector2d pq(pos);
		pq.sub(start);
		double cc = pq.lengthSquared();
		double a = pq.dot(d);
		double b = sqrt(cc - a * a);
		return b;
	}
	static double distanceFromLineToPoint(const KVector3d& start, const KVector3d& ori, const KVector3d& pos) {
		KVector3d d(ori);
		d.normalize();
		KVector3d pq(pos);
		pq.sub(start);
		double cc = pq.lengthSquared();
		double a = pq.dot(d);
		double b = sqrt(cc - a * a);
		return b;
	}
	static void distanceFromLineToPoint(const KVector2d& start, const KVector2d& ori, const KVector2d& pos, double &resDist, double& resOriCoef) {
		KVector2d d(ori);
		d.normalize();
		KVector2d pq(pos);
		pq.sub(start);
		double cc = pq.lengthSquared();
		double a = pq.dot(d);
		resDist = sqrt(cc - a * a);
		resOriCoef = a / ori.length();
	}
	static void distanceFromLineToPoint(const KVector3d& start, const KVector3d& ori, const KVector3d& pos, double &resDist, double& resOriCoef) {
		KVector3d d(ori);
		d.normalize();
		KVector3d pq(pos);
		pq.sub(start);
		double cc = pq.lengthSquared();
		double a = pq.dot(d);
		resDist = sqrt(cc - a * a);
		resOriCoef = a / ori.length();
	}
	static double distanceSquared(const KVector2d& p0, const KVector2d& p1) {
		double dx = p1.x - p0.x;
		double dy = p1.y - p0.y;
		return dx * dx + dy * dy;
	}
	static double distance(const KVector2d& p0, const KVector2d& p1) {
		return sqrt(distanceSquared(p0, p1));
	}
	static double distanceSquared(const KVector3d& p0, const KVector3d& p1) {
		double dx = p1.x - p0.x;
		double dy = p1.y - p0.y;
		double dz = p1.z - p0.z;
		return dx * dx + dy * dy + dz * dz;
	}
	static double distance(const KVector3d& p0, const KVector3d& p1) {
		return sqrt(distanceSquared(p0, p1));
	}
	static KVector3d calcNormal(const KVector3d& v0, const KVector3d& v1, const KVector3d& v2) {
		static KVector3d v01;
		static KVector3d v02;
		v01.sub(v1, v0);
		v02.sub(v2, v0);
		KVector3d result;
		result.cross(v01, v02);
		result.normalize();
		return result;
	}
	static KVector3d calcNormal(const KMultiTexPolygon& p, const KMultiTexPolygonModel& poly) {
		return calcNormal(poly.m_vertices[p.m_vtx[0]].m_pos, poly.m_vertices[p.m_vtx[1]].m_pos, poly.m_vertices[p.m_vtx[2]].m_pos);
	}
	static bool getIntersection(KVector3d& result, int& polyID, const KPolygonModel& poly, const KVector3d& start, const KVector3d& ori);
	static bool getIntersection(KVector3d& result, int& polyID, const KMultiTexPolygonModel& poly, const KVector3d& start, const KVector3d& ori);
	static KMatrix4d getAffineTransform(const KVector3d original[4], const KVector3d transformed[4]) {
		/* obtain a 4x4 matrix T which transforms v(original) into w(transformed) */
		/*
			[input]
			v[i] (i = 0...3) : original coordinates
			w[i] (i = 0...3) : transformed coordinates
			[output]
				| t00 t01 t02 t03 |
			T = | t10 t11 t12 t13 |
				| t20 t21 t22 t23 |
				| t30 t31 t32 t33 |
			which satisfies T * v[i] = w[i] (i = 0...3)

				| vx0 vx1 vx2 vx3 |      | wx0 wx1 wx2 wx3 |
			V = | vy0 vy1 vy2 vy3 |, W = | wy0 wy1 wy2 wy3 |
				| vz0 vz1 vz2 vz3 |      | wz0 wz1 wz2 wz3 |
				|  1   1   1   1  |      |  1   1   1   1  |
			T * V = W
			Vt * Tt = Wt
		*/
		KMatrix4d Vt(
			original[0].x, original[0].y, original[0].z, 1,
			original[1].x, original[1].y, original[1].z, 1,
			original[2].x, original[2].y, original[2].z, 1,
			original[3].x, original[3].y, original[3].z, 1);
		Vt.invert();
		KMatrix4d Wt(
			transformed[0].x, transformed[0].y, transformed[0].z, 1,
			transformed[1].x, transformed[1].y, transformed[1].z, 1,
			transformed[2].x, transformed[2].y, transformed[2].z, 1,
			transformed[3].x, transformed[3].y, transformed[3].z, 1);
		Vt.mul(Wt);
		Vt.transpose();
		return Vt;
	}
	static std::string trimstring(const std::string& s) {
		if(s.length() == 0) return s;
		size_t b = s.find_first_not_of(" \t\r\n");
		size_t e = s.find_last_not_of(" \t\r\n");
		if(b == -1) return "";
		return std::string(s, b, e - b + 1);
	}
};
