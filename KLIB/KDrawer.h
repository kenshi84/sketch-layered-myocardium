#pragma once

#include "KMath.h"
#include "KMultiTexGeometry.h"
#include "gl/gl.h"

class KDrawer
{
	KDrawer(void);
	~KDrawer(void);
public:
	static void drawPie(const KVector2d& pos, double radius, double angle_begin, double angle_end, int numDivide = 6);
	static void drawCircle(const KVector2d& pos, double radius, int numDivide = 6);
	static void drawArc(const KVector2d& pos, double radius, double angle_begin, double angle_end, int numDivide = 6);
	static void drawTetraEdge(const KMultiTexTetraModel& tetra, const KMultiTexTetra& tet);
	static void drawTetraEdge(const KTetraModel& tetra, const KTetra& tet);
	static void drawWireBox(double x0, double y0, double z0, double x1, double y1, double z1);
	static void drawBox  (double x0, double y0, double z0, double x1, double y1, double z1);
	static void drawBoxTex3D(
		double x0, double y0, double z0, double x1, double y1, double z1,
		double s0, double t0, double r0, double s1, double t1, double r1);
	static void drawSphere  (const KVector3d& pos, double radius, int numDivide = 6);
	static void drawArrow   (const KVector3d& start, const KVector3d& ori);
	static void drawCylinder(const KVector3d& start, const KVector3d& ori, double radius, int numDivide = 6);
	static void drawCone    (const KVector3d& start, const KVector3d& ori, double radius, double height);
	static void drawPolygonModel(const KPolygonModel& poly);
	static void drawPolygonModel_edge(const KPolygonModel& poly);
	static void drawPolygonModel2D_edge(const KPolygonModel2D& poly);
	static void drawPolygonModel2D(const KPolygonModel2D& poly);
	static void drawAxis() {
		glBegin(GL_LINES);
		glColor3d (1, 0, 0);
		glVertex3d(1, 0, 0);
		glVertex3d(0, 0, 0);
		glColor3d (0, 1, 0);
		glVertex3d(0, 1, 0);
		glVertex3d(0, 0, 0);
		glColor3d (0, 0, 1);
		glVertex3d(0, 0, 1);
		glVertex3d(0, 0, 0);
		glEnd();
	}
};
