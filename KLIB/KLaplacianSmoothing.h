#pragma once

#include "KUmfpackSparseMatrix.h"
#include "KSparseVector.h"
#include "KUmfpack.h"

#include <vector>

class KLaplacianSmoothing
{
public:
	KLaplacianSmoothing(void) {}
	~KLaplacianSmoothing(void) {
		if (p_umfpack != NULL)
			delete p_umfpack;
	}
	void setA(const KUmfpackSparseMatrix& A) {
		if (A.getColSize() != A.getRowSize()) throw "A is not square";
		m_AtA = A.transposedMultipliedBySelf();
	}
	void setC(const std::vector<KSparseVector>& C);
	void solve(std::vector<double>& x, const std::vector<double>& b) const;
protected:
	KUmfpackSparseMatrix m_AtA;
	std::vector<KSparseVector> m_C;
	KUmfpack* p_umfpack;
	
	std::vector<int> m_Ap, m_Ai;
	std::vector<double> m_Ax;
};
