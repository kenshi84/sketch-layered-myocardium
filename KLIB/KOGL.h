#pragma once

#pragma warning (disable:4786)
#pragma warning (disable:4996)

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
//#pragma comment(lib, "glaux.lib")

#include <GL/gl.h>
#include <GL/glu.h>
//#include <GL/glaux.h>

#include "KMath.h"

class KOGL
{
public:
	CWnd* m_pWnd ;
	float m_clearColor[4];
	bool m_isDrawing;
	double m_fovy, m_zNear, m_zFar;
	KVector3d m_eyePoint;
	KVector3d m_focusPoint;
	KVector3d m_upDirection;
private:
	HGLRC m_hRC;
	CDC* m_pDC;
protected:
	bool m_ButtonDown;
	CPoint m_prevpos;
	enum {
		BUTTONDOWN_ROTATE,
		BUTTONDOWN_TRANSLATE,
		BUTTONDOWN_ZOOM
	} m_ButtonDownMotionType ;
	//const KVector3d m_initEyePoint, m_initFocusPoint,m_initUpDirection;
	
public:
	KOGL(void);
	~KOGL(void) {}
	bool OnCreate(CWnd* pWnd);
	void OnDraw_Begin();
	void OnDraw_End();
	void OnDestroy();
	void OnSize(int cx, int cy );
	void ButtonDownForZoom     (const CPoint& pos);
	void ButtonDownForRotate   (const CPoint& pos);
	void ButtonDownForTranslate(const CPoint& pos);
	void MouseMove(const CPoint& pos);
	void ButtonUp();
	void updateEyePosition();
	void setEyePosition(double eyeX, double eyeY, double eyeZ, double focusX, double focusY, double focusZ, double upX, double upY, double upZ) {
		m_eyePoint   .set(eyeX  , eyeY  , eyeZ  );
		m_focusPoint .set(focusX, focusY, focusZ);
		m_upDirection.set(upX   , upY   , upZ   );
	}
	void makeOpenGLCurrent() { wglMakeCurrent( m_pDC->GetSafeHdc(), m_hRC ); }
	bool outputBitmap(char* filename);
	CDC* getDC() {return m_pDC; }
	int getWidth (){ RECT rect; m_pWnd->GetClientRect(&rect); return rect.right;  }
	int getHeight(){ RECT rect; m_pWnd->GetClientRect(&rect); return rect.bottom; }
	void project(double objx, double objy, double objz, double& winx, double& winy, double& winz);
	void project(const KVector3d& inVec, KVector3d& outVec);
	void unProject(double win_x, double win_y, double win_z, double& objx, double& objy, double& objz);
	void unProject(const KVector3d& inVec, KVector3d& outVec);
	void RedrawWindow(){ if (m_pWnd) m_pWnd->RedrawWindow(); }
	void initOpenGL();
	void getScreenCoordToGlobalLine(int cx,int cy,KVector3d& start, KVector3d& ori);
private:
	bool SetupPixelFormat();
protected:
};
