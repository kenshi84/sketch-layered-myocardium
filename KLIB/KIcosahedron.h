#pragma once

#include "KGeometry.h"

class KIcosahedron : public KPolygonModel
{
public:
	KIcosahedron(double radius = 1);
	~KIcosahedron(void) {}
	void subdivide();
	const double m_radius;
};
