#pragma once

#include "KMath.h"
#include "KGeometry.h"

#include <vector>
#include <string>

class KPolygonTexCoord {
public:
	KVector3d m_coord[3];
public:
	KPolygonTexCoord(void) {}
	KPolygonTexCoord(const KVector3d& w0, const KVector3d& w1, const KVector3d& w2) {
		m_coord[0] = w0;
		m_coord[1] = w1;
		m_coord[2] = w2;
	}
};

class KMultiTexPolygon {
public:
	int m_vtx[3];
	KVector3d m_normal[3];
	std::vector<KPolygonTexCoord> m_texCoords;
	std::vector<int> m_texIDs;
	int m_neighbor[3];
public:
	KMultiTexPolygon(void) {
		init(-1, -1, -1);
	}
	KMultiTexPolygon(int v0, int v1, int v2) {
		init(v0, v1, v2);
	}
protected:
	void init(int v0, int v1, int v2) {
		m_vtx[0] = v0;
		m_vtx[1] = v1;
		m_vtx[2] = v2;
	}
};

class KMultiTexPolygonModel {
public:
	std::vector<KVertex>  m_vertices;
	std::vector<KMultiTexPolygon> m_polygons;
	std::vector<std::string> m_texNames;
public:
	KMultiTexPolygonModel(void) {}
	~KMultiTexPolygonModel(void) {}
	void calcNeighbor();
	void calcSmoothNormals(double threshold = 0.5);
};

class KTetraTexCoord {
public:
	KVector3d m_coord[4];
public:
	KTetraTexCoord(void) {}
	KTetraTexCoord(const KVector3d& w0, const KVector3d& w1, const KVector3d& w2, const KVector3d& w3) {
		m_coord[0] = w0;
		m_coord[1] = w1;
		m_coord[2] = w2;
		m_coord[3] = w3;
	}
};

class KMultiTexTetra {
public:
	int m_vtx[4];
	//KVector3d m_normal[4];
	int m_neighbor[4];
	std::vector<KTetraTexCoord> m_texCoords;
	std::vector<int> m_texIDs;
public:
	KMultiTexTetra(void) {
		init(-1, -1, -1, -1);
	}
	KMultiTexTetra(int v0, int v1, int v2, int v3) {
		init(v0, v1, v2, v3);
	}
protected:
	void init(int v0, int v1, int v2, int v3) {
		m_vtx[0] = v0;
		m_vtx[1] = v1;
		m_vtx[2] = v2;
		m_vtx[3] = v3;
	}
};

class KMultiTexTetraModel {
public:
	std::vector<KVertex>        m_vertices;
	std::vector<KMultiTexTetra> m_tetras;
	std::vector<std::string>    m_texNames;
public:
	KMultiTexTetraModel(void) {}
	~KMultiTexTetraModel(void) {}
	bool loadFile(const std::string& fname);
	//void print();
	//void calcNormal();
	void calcNeighbor();
	KMultiTexPolygonModel toMultiTexPolygonModel();
};
