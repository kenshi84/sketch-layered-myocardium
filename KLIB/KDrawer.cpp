#include "StdAfx.h"
#include "KDrawer.h"

#include <gl/glu.h>

#include <cmath>

void KDrawer::drawPie(const KVector2d& pos, double radius, double angle_begin, double angle_end, int numDivide) {
	glPushMatrix();
	glTranslated(pos.x, pos.y, 0);
	glScaled(radius, radius, 1);
	glBegin(GL_LINES);
	glVertex2d(0, 0);	glVertex2d(cos(angle_begin), sin(angle_begin));
	glVertex2d(0, 0);	glVertex2d(cos(angle_end  ), sin(angle_end  ));
	glEnd();
	glPopMatrix();
	drawArc(pos, radius, angle_begin, angle_end, numDivide);
}

void KDrawer::drawCircle(const KVector2d& pos, double radius, int numDivide) {
	drawArc(pos, radius, 0, 2 * M_PI, numDivide);
}

void KDrawer::drawArc(const KVector2d& pos, double radius, double angle_begin, double angle_end, int numDivide) {
	glPushMatrix();
	glTranslated(pos.x, pos.y, 0);
	glScaled(radius, radius, 1);
	glBegin(GL_LINE_STRIP);
	double delta = (angle_end - angle_begin) / numDivide;
	double theta = angle_begin;
	for (int i = 0; i <= numDivide; ++i, theta += delta) {
		glVertex2d(cos(theta), sin(theta));
	}
	glEnd();
	glPopMatrix();
}

void KDrawer::drawTetraEdge(const KMultiTexTetraModel& tetra, const KMultiTexTetra& tet) {
	const KVector3d& v0 = tetra.m_vertices[tet.m_vtx[0]].m_pos;
	const KVector3d& v1 = tetra.m_vertices[tet.m_vtx[1]].m_pos;
	const KVector3d& v2 = tetra.m_vertices[tet.m_vtx[2]].m_pos;
	const KVector3d& v3 = tetra.m_vertices[tet.m_vtx[3]].m_pos;
	glBegin(GL_LINE_STRIP);
	glVertex3dv(v0.getPtr());
	glVertex3dv(v1.getPtr());
	glVertex3dv(v2.getPtr());
	glVertex3dv(v0.getPtr());
	glVertex3dv(v3.getPtr());
	glVertex3dv(v2.getPtr());
	glEnd();
	glBegin(GL_LINE_STRIP);
	glVertex3dv(v1.getPtr());
	glVertex3dv(v3.getPtr());
	glEnd();
}

void KDrawer::drawTetraEdge(const KTetraModel& tetra, const KTetra& tet) {
	const KVector3d& v0 = tetra.m_vertices[tet.m_vtx[0]].m_pos;
	const KVector3d& v1 = tetra.m_vertices[tet.m_vtx[1]].m_pos;
	const KVector3d& v2 = tetra.m_vertices[tet.m_vtx[2]].m_pos;
	const KVector3d& v3 = tetra.m_vertices[tet.m_vtx[3]].m_pos;
	glBegin(GL_LINE_STRIP);
	glVertex3dv(v0.getPtr());
	glVertex3dv(v1.getPtr());
	glVertex3dv(v2.getPtr());
	glVertex3dv(v0.getPtr());
	glVertex3dv(v3.getPtr());
	glVertex3dv(v2.getPtr());
	glEnd();
	glBegin(GL_LINE_STRIP);
	glVertex3dv(v1.getPtr());
	glVertex3dv(v3.getPtr());
	glEnd();
}

void KDrawer::drawPolygonModel(const KPolygonModel& poly) {
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
		const KPolygon& p = poly.m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			glNormal3dv(p.m_normal[j].getPtr());
			glVertex3dv(poly.m_vertices[p.m_vtx[j]].m_pos.getPtr());
		}
	}
	glEnd();
}

void KDrawer::drawPolygonModel_edge(const KPolygonModel& poly) {
	for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
		const KPolygon& p = poly.m_polygons[i];
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 3; ++j) {
			glVertex3dv(poly.m_vertices[p.m_vtx[j]].m_pos);
		}
		glEnd();
	}
}

void KDrawer::drawPolygonModel2D(const KPolygonModel2D& poly) {
	glBegin(GL_TRIANGLES);
	for (unsigned int i = 0; i < poly.m_polygons.size(); ++i) {
		const KPolygon2D& p = poly.m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			glVertex2dv(poly.m_vertices[p.m_vtx[j]].m_pos);
		}
	}
	glEnd();
}

void KDrawer::drawPolygonModel2D_edge(const KPolygonModel2D& poly) {
	for (unsigned int i = 0; i < poly.m_polygons.size(); ++i) {
		const KPolygon2D& p = poly.m_polygons[i];
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 3; ++j) {
			glVertex2dv(poly.m_vertices[p.m_vtx[j]].m_pos.getPtr());
		}
		glEnd();
	}
}

/* Draw an array whose size is proportional to the input vector length */
void KDrawer::drawArrow(const KVector3d& start, const KVector3d& ori) {
	double len = ori.length();
	
	// transform
	glPushMatrix();
	KVector3d nori(ori);
	nori.normalize();
	KVector3d ey(0, 1, 0);
	KVector3d axis;
	axis.cross(ey, nori);
	double cost = ey.dot(nori);
	double t = acos(cost) * 180 / M_PI;
	glTranslated(start.x, start.y, start.z);
	glRotated(t, axis.x, axis.y, axis.z);
	
	// cusp
	double r = len / 8;
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, 1, 0);
	glVertex3d(0, len, 0);
	for(int i = 0; i <= 6; i++) {
		double a = i * M_PI / 3;
		glNormal3d(sin(a), 0, cos(a));
		glVertex3d(r * sin(a), len * 2 / 3, r * cos(a));
	}
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, -1, 0);
	for(int i = 0; i < 6; i++) {
		double a = - i * M_PI / 3;
		glVertex3d(r * sin(a), len * 2 / 3, r * cos(a));
	}
	glEnd();
	// axis
	double r2 = len / 32;
	glBegin(GL_TRIANGLE_STRIP);
	for(int i = 0; i <= 6; i++) {
		double a = - i * M_PI / 3;
		glNormal3d(sin(a), 0, cos(a));
		glVertex3d(r2 * sin(a), 0,           r2 * cos(a));
		glVertex3d(r2 * sin(a), len * 2 / 3, r2 * cos(a));
	}
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, -1, 0);
	for(int i = 0; i < 6; i++) {
		double a = - i * M_PI / 3;
		glVertex3d(r2 * sin(a), 0, r2 * cos(a));
	}
	glEnd();

	// restore transform
	glPopMatrix();
}

void KDrawer::drawCylinder(const KVector3d& start, const KVector3d& ori, double radius, int numDivide) {
	double len = ori.length();
	
	// transform
	glPushMatrix();
	KVector3d nori(ori);
	nori.normalize();
	KVector3d ey(0, 1, 0);
	KVector3d axis;
	axis.cross(ey, nori);
	double cost = ey.dot(nori);
	double t = acos(cost) * 180 / M_PI;
	glTranslated(start.x, start.y, start.z);
	glRotated(t, axis.x, axis.y, axis.z);
	
	// top
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, 1, 0);
	for(int i = 0; i < 6; i++) {
		double a = i * M_PI / 3;
		glVertex3d(radius * sin(a), len, radius * cos(a));
	}
	glEnd();
	// side
	glBegin(GL_TRIANGLE_STRIP);
	for(int i = 0; i <= 6; i++) {
		double a = - i * M_PI / 3;
		glNormal3d(sin(a), 0, cos(a));
		glVertex3d(radius * sin(a), 0,   radius * cos(a));
		glVertex3d(radius * sin(a), len, radius * cos(a));
	}
	glEnd();
	// bottom
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, -1, 0);
	for(int i = 0; i < 6; i++) {
		double a = - i * M_PI / 3;
		glVertex3d(radius * sin(a), 0, radius * cos(a));
	}
	glEnd();

	// restore transform
	glPopMatrix();
}
void KDrawer::drawCone(const KVector3d& start, const KVector3d& ori, double radius, double height) {
	glPushMatrix();
	KVector3d nori(ori);
	nori.normalize();
	KVector3d ey(0, 1, 0);
	KVector3d axis;
	axis.cross(ey, nori);
	double cost = ey.dot(nori);
	double t = acos(cost) * 180 / M_PI;
	glTranslated(start.x, start.y, start.z);
	glRotated(t, axis.x, axis.y, axis.z);
	
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, 1, 0);
	glVertex3d(0, height, 0);
	for(int i = 0; i <= 6; i++) {
		double a = i * M_PI / 3;
		glNormal3d(sin(a), 0, cos(a));
		glVertex3d(radius * sin(a), 0, radius * cos(a));
	}
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, -1, 0);
	for(int i = 0; i < 6; i++) {
		double a = - i * M_PI / 3;
		glVertex3d(radius * sin(a), 0, radius * cos(a));
	}
	glEnd();
	glPopMatrix();
}

void KDrawer::drawSphere(const KVector3d& pos, double radius, int numDivide) {
	if (numDivide < 1) return;
	glPushMatrix();
	glTranslated(pos.x, pos.y, pos.z);
	GLUquadricObj *sphere = gluNewQuadric();
	gluQuadricDrawStyle(sphere, GLU_FILL);
	gluSphere(sphere, radius, numDivide, numDivide);
	//glScaled(radius, radius, radius);
	//glutSolidSphere(radius, numDivide, numDivide);
	//gluSphere(0, 0, 0, 0);
	//glBegin(GL_TRIANGLES);
	//for (int i = 0; i < 4 * numDivide; ++i) {
	//	double t0 = i       * M_PI / (2 * numDivide);
	//	double t1 = (i + 1) * M_PI / (2 * numDivide);
	//	double cost0 = cos(t0), sint0 = sin(t0);
	//	double cost1 = cos(t1), sint1 = sin(t1);
	//	for (int j = -numDivide; j < numDivide; ++j) {
	//		double p0 = j       * M_PI / (2 * numDivide);
	//		double p1 = (j + 1) * M_PI / (2 * numDivide);
	//		double cosp0 = cos(p0), sinp0 = sin(p0);
	//		double cosp1 = cos(p1), sinp1 = sin(p1);
	//		KVector3d p00(cost0 * cosp0, sinp0, sint0 * cosp0);
	//		KVector3d p10(cost1 * cosp0, sinp0, sint1 * cosp0);
	//		KVector3d p01(cost0 * cosp1, sinp1, sint0 * cosp1);
	//		KVector3d p11(cost1 * cosp1, sinp1, sint1 * cosp1);
	//		if (j != -numDivide) {
	//			glNormal3dv(p00.getPtr());	glVertex3dv(p00.getPtr());
	//			glNormal3dv(p01.getPtr());	glVertex3dv(p01.getPtr());
	//			glNormal3dv(p10.getPtr());	glVertex3dv(p10.getPtr());
	//		}
	//		if (j != numDivide - 1) {
	//			glNormal3dv(p10.getPtr());	glVertex3dv(p10.getPtr());
	//			glNormal3dv(p11.getPtr());	glVertex3dv(p11.getPtr());
	//			glNormal3dv(p01.getPtr());	glVertex3dv(p01.getPtr());
	//		}
	//	}
	//}
	//glEnd();
	glPopMatrix();
}

void KDrawer::drawWireBox(double x0, double y0, double z0, double x1, double y1, double z1) {
	glBegin(GL_LINES);
	glVertex3d(x0, y0, z0);	glVertex3d(x1, y0, z0);
	glVertex3d(x0, y1, z0);	glVertex3d(x1, y1, z0);
	glVertex3d(x0, y1, z1);	glVertex3d(x1, y1, z1);
	glVertex3d(x0, y0, z1);	glVertex3d(x1, y0, z1);
	glVertex3d(x0, y0, z0);	glVertex3d(x0, y1, z0);
	glVertex3d(x0, y0, z1);	glVertex3d(x0, y1, z1);
	glVertex3d(x1, y0, z1);	glVertex3d(x1, y1, z1);
	glVertex3d(x1, y0, z0);	glVertex3d(x1, y1, z0);
	glVertex3d(x0, y0, z0);	glVertex3d(x0, y0, z1);
	glVertex3d(x1, y0, z0);	glVertex3d(x1, y0, z1);
	glVertex3d(x1, y1, z0);	glVertex3d(x1, y1, z1);
	glVertex3d(x0, y1, z0);	glVertex3d(x0, y1, z1);
	glEnd();
}

void KDrawer::drawBoxTex3D(
		double x0, double y0, double z0, double x1, double y1, double z1,
		double s0, double t0, double r0, double s1, double t1, double r1)
{
	glBegin(GL_QUADS);
	// x = x1
	glNormal3d(1, 0, 0);
	glTexCoord3d(s1, t0, r0);		glVertex3d(x1, y0, z0);
	glTexCoord3d(s1, t1, r0);		glVertex3d(x1, y1, z0);
	glTexCoord3d(s1, t1, r1);		glVertex3d(x1, y1, z1);
	glTexCoord3d(s1, t0, r1);		glVertex3d(x1, y0, z1);
	// y = y1
	glNormal3d(0, 1, 0);
	glTexCoord3d(s0, t1, r0);		glVertex3d(x0, y1, z0);
	glTexCoord3d(s0, t1, r1);		glVertex3d(x0, y1, z1);
	glTexCoord3d(s1, t1, r1);		glVertex3d(x1, y1, z1);
	glTexCoord3d(s1, t1, r0);		glVertex3d(x1, y1, z0);
	// z = z1
	glNormal3d(0, 0, 1);
	glTexCoord3d(s0, t0, r1);		glVertex3d(x0, y0, z1);
	glTexCoord3d(s1, t0, r1);		glVertex3d(x1, y0, z1);
	glTexCoord3d(s1, t1, r1);		glVertex3d(x1, y1, z1);
	glTexCoord3d(s0, t1, r1);		glVertex3d(x0, y1, z1);
	// x = x0
	glNormal3d(-1, 0, 0);
	glTexCoord3d(s0, t0, r0);		glVertex3d(x0, y0, z0);
	glTexCoord3d(s0, t0, r1);		glVertex3d(x0, y0, z1);
	glTexCoord3d(s0, t1, r1);		glVertex3d(x0, y1, z1);
	glTexCoord3d(s0, t1, r0);		glVertex3d(x0, y1, z0);
	// y = y0
	glNormal3d(0, -1, 0);
	glTexCoord3d(s0, t0, r0);		glVertex3d(x0, y0, z0);
	glTexCoord3d(s1, t0, r0);		glVertex3d(x1, y0, z0);
	glTexCoord3d(s1, t0, r1);		glVertex3d(x1, y0, z1);
	glTexCoord3d(s0, t0, r1);		glVertex3d(x0, y0, z1);
	// z = z0
	glNormal3d(0, 0, -1);
	glTexCoord3d(s0, t0, r0);		glVertex3d(x0, y0, z0);
	glTexCoord3d(s0, t1, r0);		glVertex3d(x0, y1, z0);
	glTexCoord3d(s1, t1, r0);		glVertex3d(x1, y1, z0);
	glTexCoord3d(s1, t0, r0);		glVertex3d(x1, y0, z0);
	glEnd();
}

void KDrawer::drawBox  (double x0, double y0, double z0, double x1, double y1, double z1) {
	glBegin(GL_QUADS);
	// x = x1
	glNormal3d(1, 0, 0);
	glVertex3d(x1, y0, z0);
	glVertex3d(x1, y1, z0);
	glVertex3d(x1, y1, z1);
	glVertex3d(x1, y0, z1);
	// y = y1
	glNormal3d(0, 1, 0);
	glVertex3d(x0, y1, z0);
	glVertex3d(x0, y1, z1);
	glVertex3d(x1, y1, z1);
	glVertex3d(x1, y1, z0);
	// z = z1
	glNormal3d(0, 0, 1);
	glVertex3d(x0, y0, z1);
	glVertex3d(x1, y0, z1);
	glVertex3d(x1, y1, z1);
	glVertex3d(x0, y1, z1);
	// x = x0
	glNormal3d(-1, 0, 0);
	glVertex3d(x0, y0, z0);
	glVertex3d(x0, y0, z1);
	glVertex3d(x0, y1, z1);
	glVertex3d(x0, y1, z0);
	// y = y0
	glNormal3d(0, -1, 0);
	glVertex3d(x0, y0, z0);
	glVertex3d(x1, y0, z0);
	glVertex3d(x1, y0, z1);
	glVertex3d(x0, y0, z1);
	// z = z0
	glNormal3d(0, 0, -1);
	glVertex3d(x0, y0, z0);
	glVertex3d(x0, y1, z0);
	glVertex3d(x1, y1, z0);
	glVertex3d(x1, y0, z0);
	glEnd();
}

