#include "StdAfx.h"
#include ".\kgeometry.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <list>
//#include <boost/tokenizer.hpp>
//#include <boost/lexical_cast.hpp>

#include "KUtil.h"

using namespace std;

//KTetra::KTetra(KVertex& v0, KVertex& v1, KVertex& v2, KVertex& v3, int id)
//: m_id(id) {
//	m_vtx[0] = v0;
//	m_vtx[1] = v1;
//	m_vtx[2] = v2;
//	m_vtx[3] = v3;
//}

const int KTetraModel::FACE_VTX_ORDER[4][3] = {{1, 2, 3}, {0, 3, 2}, {0, 1, 3}, {0, 2, 1}};

KPolygonModel KTetraModel::toPolygonModel() const {
	vector<int> vtxIDs;
	vector<KVertex> vertices;
	vector<KPolygon> polygons;
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		const KTetra& tet = m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			if (tet.m_neighbor[j] != -1) continue;
			int v[3];
			for (int k = 0; k < 3; ++k) {
				int vtxID = tet.m_vtx[FACE_VTX_ORDER[j][k]];
				vector<int>::iterator findPos = find(vtxIDs.begin(), vtxIDs.end(), vtxID);
				if (findPos == vtxIDs.end()) {
					vtxIDs.push_back(vtxID);
					const KVector3d& pos = m_vertices[vtxID].m_pos;
					vertices.push_back(KVertex(pos));
					v[k] = (int)vtxIDs.size() - 1;
				} else {
					v[k] = (int)distance(vtxIDs.begin(), findPos);
				}
			}
			polygons.push_back(KPolygon(v[0], v[1], v[2]));
		}
	}
	KPolygonModel result;
	result.m_vertices = vertices;
	result.m_polygons = polygons;
	return result;
}

void KTetraModel::calcNeighbor() {
	vector<vector<int> > vertexNeighbor(m_vertices.size());
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		KTetra& tet = m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			vertexNeighbor[tet.m_vtx[j]].push_back(i);
		}
	}
	static int order[][3] = {{3, 2, 1}, {0, 2, 3}, {0, 3, 1}, {0, 1, 2}};
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		KTetra& tet = m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			vector<int>& n0 = vertexNeighbor[tet.m_vtx[order[j][0]]];
			vector<int>& n1 = vertexNeighbor[tet.m_vtx[order[j][1]]];
			vector<int>& n2 = vertexNeighbor[tet.m_vtx[order[j][2]]];
			vector<int> n01;
			set_intersection(n0 .begin(), n0 .end(), n1.begin(), n1.end(), back_inserter(n01));
			vector<int> n012;
			set_intersection(n01.begin(), n01.end(), n2.begin(), n2.end(), back_inserter(n012));
			if ((int)n012.size() == 2) {
				int k = n012.front() == i ?
						n012.back()
					:
						n012.front();
				tet.m_neighbor[j] = k;
			} else if ((int)n012.size() == 1) {
				tet.m_neighbor[j] = -1;
			} else {
				cout << "error in calcNeighbor()" << endl;
				return;
			}
		}
	}
}

void KTetraModel::calcNormal() {
	static int order[][3] = {{3, 2, 1}, {0, 2, 3}, {0, 3, 1}, {0, 1, 2}};
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		KTetra& tet = m_tetras[i];
		bool inverse = false;
		for (int j = 0; j < 4; ++j) {
			KVector3d& v0 = m_vertices[tet.m_vtx[order[j][0]]].m_pos;
			KVector3d v01(m_vertices[tet.m_vtx[order[j][1]]].m_pos);
			KVector3d v02(m_vertices[tet.m_vtx[order[j][2]]].m_pos);
			v01.sub(v0);
			v02.sub(v0);
			KVector3d n;
			n.cross(v01, v02);
			if (j == 0) {
				KVector3d v03(m_vertices[tet.m_vtx[0]].m_pos);
				v03.sub(v0);
				inverse = (n.dot(v03) > 0);
			}
			n.normalize();
			if (inverse) n.scale(-1);
			tet.m_normal[j] = n;
		}
	}
}

void KTetraModel::saveEleNodeFile(const string& fname) const {
	string& fname_ele  = fname + ".ele";
	string& fname_node = fname + ".node";
	ofstream ofs_ele (fname_ele .c_str());
	ofstream ofs_node(fname_node.c_str());
	
	ofs_node << m_vertices.size() << " 3 0 0" << endl;
	for (int i = 0; i < m_vertices.size(); ++i) {
		const KVector3d& pos = m_vertices[i].m_pos;
		ofs_node << i << " " << pos.x << " " << pos.y << " " << pos.z << endl; 
	}
	ofs_ele << m_tetras.size() << " 4 0" << endl;
	for (int i = 0; i < m_tetras.size(); ++i) {
		KTetra t = m_tetras[i];
		ofs_ele << i << " " << t.m_vtx[0] << " " << t.m_vtx[1] << " " << t.m_vtx[2] << " " << t.m_vtx[3] << endl;
	}
	ofs_ele .close();
	ofs_node.close();
}


bool KTetraModel::loadFile(const std::string& fname)
{
	//typedef boost::tokenizer<boost::char_delimiters_separator<char> > tokenizer;
	//boost::char_delimiters_separator<char> sep(false, "", " ");
	
	vector<KVertex> verticesTmp;
	vector<KTetra>  tetrasTmp;
	
	string line;
	ifstream ifs;
	
	const string& fnameNode = fname + ".node";
	ifs.open(fnameNode.c_str());
	if (!ifs.is_open()) {
		printf("Could not open file: %s\n", fnameNode.c_str());
		return false;
	}
	//getline(ifs, line);
	//tokenizer tok(line, sep);
	//int numVtx = boost::lexical_cast<int>(*tok.begin());
	int numVtx;
	ifs >> numVtx;
	int tmp;
	ifs >> tmp >> tmp >> tmp;
	verticesTmp.reserve(numVtx);
	for (int i = 0; i < numVtx; ++i) {
		//getline(ifs, line);
		//tok.assign(line, sep);
		//tokenizer::iterator iter = tok.begin();
		ifs >> tmp;
		double x, y, z;
		ifs >> x >> y >> z;
		//double x = boost::lexical_cast<double>(*(++iter));
		//double y = boost::lexical_cast<double>(*(++iter));
		//double z = boost::lexical_cast<double>(*(++iter));
		KVertex v(KVector3d(x, y, z));
		verticesTmp.push_back(v);
	}
	ifs.close();
	
	const string& fnameEle = fname + ".ele";
	ifs.open(fnameEle.c_str());
	if (!ifs.is_open()) return false;
	//getline(ifs, line);
	//tok.assign(line, sep);
	//int numTet = boost::lexical_cast<int>(*tok.begin());
	int numTet;
	ifs >> numTet;
	ifs >> tmp >> tmp;
	tetrasTmp.reserve(numTet);
	for (int i = 0; i < numTet; ++i) {
		//getline(ifs, line);
		//tok.assign(line, sep);
		//tokenizer::iterator iter = tok.begin();
		//int i0 = boost::lexical_cast<int>(*(++iter));
		//int i1 = boost::lexical_cast<int>(*(++iter));
		//int i2 = boost::lexical_cast<int>(*(++iter));
		//int i3 = boost::lexical_cast<int>(*(++iter));
		ifs >> tmp;
		int i0, i1, i2, i3;
		ifs >> i0 >> i1 >> i2 >> i3;
		KTetra tetra(i0, i1, i2, i3);
		tetrasTmp.push_back(tetra);
	}
	ifs.close();
	
	m_vertices = verticesTmp;
	m_tetras   = tetrasTmp;
	return true;
}

void KTetraModel::print() {
	using namespace std;
	cout << "vertices:" << endl;
	for (vector<KVertex>::iterator iter = m_vertices.begin(); iter != m_vertices.end(); ++iter)
		cout << iter->m_pos.x << ", " << iter->m_pos.y << ", " << iter->m_pos.z << endl;
	for (vector<KTetra>::iterator iter = m_tetras.begin(); iter != m_tetras.end(); ++iter)
		cout << iter->m_vtx[0] << ", " << iter->m_vtx[1] << ", " << iter->m_vtx[2] << ", " << iter->m_vtx[3] << endl;
}

bool KPolygonModel::loadFile(const string& fname) {
	size_t extPos = fname.rfind('.');
	string ext = fname.substr(extPos, fname.length() - extPos);
	transform(ext.begin(), ext.end(), ext.begin(), tolower);
	if (ext.compare(".obj") == 0) {
		return loadObjFile(fname);
	} else if (ext.compare(".mqo") == 0) {
		return loadMqoFile(fname);
	}
	return false;
}

bool KPolygonModel::saveFile(const std::string& fname) const {
	size_t extPos = fname.rfind('.');
	string ext = fname.substr(extPos, fname.length() - extPos);
	transform(ext.begin(), ext.end(), ext.begin(), tolower);
	if (ext.compare(".obj") == 0) {
		return saveObjFile(fname);
	}
	return false;
}

bool KPolygonModel::loadMqoFile(const std::string& fname) {
	ifstream ifs(fname.c_str());
	if (!ifs.is_open()) {
		return false;
	}
	string line;
	string header;
	istringstream iss;
	m_vertices.clear();
	m_polygons.clear();
	double vx, vy, vz;
	int i0, i1, i2;
	while (!ifs.eof()) {
		getline(ifs, line);
		if (line.empty()) continue;
		iss.clear();
		iss.str(line);
		cout << iss.str() << endl;
		iss >> header;
		if (header.compare("vertex") == 0) {
			int n;
			iss >> n;
			m_vertices.resize(n);
			for (int i = 0; i < n; ++i) {
				KVector3d& pos = m_vertices[i].m_pos;
				getline(ifs, line);
				iss.clear();
				iss.str(line);
				iss >> pos.x >> pos.y >> pos.z;
			}
		} else if (header.compare("face") == 0) {
			int n;
			iss >> n;
			m_polygons.resize(n);
			for (int i = 0; i < n; ++i) {
				KPolygon& p = m_polygons[i];
				getline(ifs, line);
				iss.str(line);
				int num1;
				char c1, c2, c3;
				iss >> num1 >> c1 >> c2 >> p.m_vtx[2] >> p.m_vtx[1] >> p.m_vtx[0] >> c3;
			}
		}
	}
	return true;
}

bool KPolygonModel::loadObjFile(const std::string& fname) {
	ifstream ifs(fname.c_str());
	if (!ifs.is_open()) {
		return false;
	}
	string line;
	
	m_vertices.clear();
	m_polygons.clear();

	double vx, vy, vz;
	int i0, i1, i2;
	while (!ifs.eof()) {
		getline(ifs, line);
		char c = line[0];
		char tmp;
		if (c == 'v' || c == 'V') {
			sscanf(line.c_str(), "%c %lf %lf %lf", &tmp, &vx, &vy, &vz);
			//ifs >> vx >> vy >> vz;
			m_vertices.push_back(KVertex(KVector3d(vx, vy, vz)));
		} else if (c == 'f' || c == 'F') {
			sscanf(line.c_str(), "%c %d %d %d", &tmp, &i0, &i1, &i2);
			//ifs >> i0 >> i1 >> i2;
			m_polygons.push_back(KPolygon(i0 - 1, i1 - 1, i2 - 1));
		}
	}
	return true;
}

bool KPolygonModel::saveObjFile(const std::string& fname) const {
	ofstream ofs(fname.c_str());
	string line;

	for (int i = 0; i < m_vertices.size(); ++i) {
		const KVector3d& pos = m_vertices[i].m_pos;
		ofs << "v " << pos.x << " " << pos.y << " " << pos.z << endl; 
	}
	for (int i = 0; i < m_polygons.size(); ++i) {
		KPolygon p = m_polygons[i];
		ofs << "f " << (p.m_vtx[0] + 1) << " " << (p.m_vtx[1] + 1) << " " << (p.m_vtx[2] + 1) << endl;
	}
	ofs.close();
	return true;
}

void KPolygonModel::calcEdges() {
	m_edges.clear();
	for each (const KPolygon& p in m_polygons) {
		for (int i = 0; i < 3; ++i) {
			int vtx0 = p.m_vtx[i];
			int vtx1 = p.m_vtx[(i + 1) % 3];
			if (vtx1 < vtx0) continue;
			m_edges.push_back(KEdge(vtx0, vtx1));
		}
	}
}
void KPolygonModel::calcNeighbors() {
	if (m_edges.empty()) {
		calcEdges();
	}
	for (int i = 0; i < m_vertices.size(); ++i) m_vertices[i].m_neighbor.clear();
	//m_neighbors.clear();
	//m_neighbors.resize(m_vertices.size());
	for each (const KEdge& e in m_edges) {
		int vtx0 = e.m_vtx[0];
		int vtx1 = e.m_vtx[1];
		m_vertices[vtx0].m_neighbor.push_back(vtx1);
		m_vertices[vtx1].m_neighbor.push_back(vtx0);
		//m_neighbors[vtx0].push_back(vtx1);
		//m_neighbors[vtx1].push_back(vtx0);
	}
}

void KPolygonModel::calcSmoothNormals() {
	vector<KVector3d> normals(m_vertices.size());
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KPolygon& p = m_polygons[i];
		KVector3d& v0 = m_vertices[p.m_vtx[0]].m_pos;
		KVector3d& v1 = m_vertices[p.m_vtx[1]].m_pos;
		KVector3d& v2 = m_vertices[p.m_vtx[2]].m_pos;
		KVector3d& n = KUtil::calcNormal(v0, v1, v2);
		for (int j = 0; j < 3; ++j) {
			normals[p.m_vtx[j]].add(n);
		}
	}
	for (int i = 0; i < (int)m_vertices.size(); ++i)
		normals[i].normalize();
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KPolygon& p = m_polygons[i];
		for (int j = 0; j < 3; ++j)
			p.m_normal[j] = normals[p.m_vtx[j]];
	}
}

void KPolygonModel::calcSmoothNormalsWithSharpEdges(double threshold) {
	vector<map<int, KVector3d> > normals(m_vertices.size());
	vector<list<int> > plists(m_vertices.size());
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KPolygon& p = m_polygons[i];
		KVector3d& v0 = m_vertices[p.m_vtx[0]].m_pos;
		KVector3d& v1 = m_vertices[p.m_vtx[1]].m_pos;
		KVector3d& v2 = m_vertices[p.m_vtx[2]].m_pos;
		KVector3d& n = KUtil::calcNormal(v0, v1, v2);
		for (int j = 0; j < 3; ++j) {
			normals[p.m_vtx[j]].insert(pair<int, KVector3d>(i, n));
			plists[p.m_vtx[j]].push_back(i);
		}
	}
	for (int i = 0; i < (int)m_vertices.size(); ++i) {
		list<int>& plist = plists[i];
		int pID = plist.front();
		vector<int> pOrdered;
		pOrdered.reserve(plist.size());
		pOrdered.push_back(pID);
		plist.pop_front();
		bool isLooped = true;
		while (!plist.empty()) {
			KPolygon& p = m_polygons[pID];
			int vID;
			for (int j = 0; j < 3; ++j) {
				if (p.m_vtx[j] == i) {
					vID = p.m_vtx[(j + 1) % 3];
					break;
				}
			}
			list<int>::iterator findPos = plist.begin();
			for (; findPos != plist.end(); ++findPos) {
				KPolygon& q = m_polygons[*findPos];
				if (q.m_vtx[0] == vID || q.m_vtx[1] == vID || q.m_vtx[2] == vID) break;
			}
			if (findPos == plist.end()) {
				isLooped = false;
				break;
			}
			pID = *findPos;
			pOrdered.push_back(pID);
			plist.erase(findPos);
		}
		if (!isLooped) {
			KVector3d avg;
			for (map<int, KVector3d>::iterator j = normals[i].begin(); j != normals[i].end(); ++j)
				avg.add(j->second);
			avg.normalize();
			for (map<int, KVector3d>::iterator j = normals[i].begin(); j != normals[i].end(); ++j) {
				KPolygon& q = m_polygons[j->first];
				for (int k = 0; k < 3; ++k)
					if (q.m_vtx[k] == i) {
						q.m_normal[k] = avg;
						break;
					}
			}
			continue;
		}
		int N = (int)pOrdered.size();
		vector<KVector3d> nOrdered(N);
		for (int j = 0; j < N; ++j)
			nOrdered[j] = normals[i].find(pOrdered[j])->second;
		vector<bool> isSeparated(N);
		int cntSeparated = 0;
		int i0;
		for (int j = 0; j < N; ++j) {
			KVector3d& p0 = nOrdered[j];
			KVector3d& p1 = nOrdered[(j + 1) % N];
			bool b = p0.dot(p1) < threshold;
			isSeparated[j] = b;
			if (b) {
				++cntSeparated;
				i0 = j;
			}
		}
		if (cntSeparated <= 1) {
			KVector3d avg;
			for (map<int, KVector3d>::iterator j = normals[i].begin(); j != normals[i].end(); ++j)
				avg.add(j->second);
			avg.normalize();
			for (int j = 0; j < N; ++j) {
				KPolygon& q = m_polygons[pOrdered[j]];
				for (int k = 0; k < 3; ++k)
					if (q.m_vtx[k] == i) {
						q.m_normal[k] = avg;
						break;
					}
			}
			continue;
		}
		int cnt = 0;
		while (cnt < N) {
			int i1 = (i0 + 1) % N;
			while (!isSeparated[i1]) i1 = (i1 + 1) % N;
			i0 = (i0 + 1) % N;
			i1 = (i1 + 1) % N;
			KVector3d avg;
			for (int j = i0; j != i1; j = (j + 1) % N) {
				avg.add(nOrdered[j]);
			}
			avg.normalize();
			for (int j = i0; j != i1; j = (j + 1) % N) {
				KPolygon& q = m_polygons[pOrdered[j]];
				for (int k = 0; k < 3; ++k)
					if (q.m_vtx[k] == i) {
						q.m_normal[k] = avg;
						break;
					}
				++cnt;
			}
			i0 = (i1 + N - 1) % N;
		}
	}
}

bool KPolygonModel2D::loadFile(const char* fname) {
	ifstream ifs(fname);
	if (!ifs.is_open()) return false;
	string line;
	
	m_vertices.clear();
	m_polygons.clear();
	
	double vx, vy;
	int i0, i1, i2;
	while (!ifs.eof()) {
		getline(ifs, line);
		char c = line[0];
		char tmp;
		if (c == 'v' || c == 'V') {
			sscanf(line.c_str(), "%c %lf %lf", &tmp, &vx, &vy);
			//ifs >> vx >> vy;
			m_vertices.push_back(KVertex2D(KVector2d(vx, vy)));
		} else if (c == 'f' || c == 'F') {
			sscanf(line.c_str(), "%c %d %d %d", &tmp, &i0, &i1, &i2);
			//ifs >> i0 >> i1 >> i2;
			m_polygons.push_back(KPolygon2D(i0 - 1, i1 - 1, i2 - 1));
		}
	}
	return true;
}

bool KPolygonModel2D::saveFile(const char* fname) const {
	ofstream ofs(fname);
	if (!ofs.is_open()) return false;
	string line;
	
	ofs << m_vertices.size() << endl;
	for (int i = 0; i < (int)m_vertices.size(); ++i) {
		ofs << m_vertices[i].m_pos.x << ' ' << m_vertices[i].m_pos.y << endl;
	}
	ofs << m_polygons.size() << endl;
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		const KPolygon2D& p = m_polygons[i];
		ofs << (p.m_vtx[0] - 1) << ' ' << (p.m_vtx[1] - 1) << ' ' << (p.m_vtx[2] - 1) << endl;
	}
	ofs.close();
	return true;
}

void KPolygonModel2D::calcFaceNeighbor() {
	vector<vector<int> > vertexNeighbor(m_vertices.size());
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KPolygon2D& p = m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			vertexNeighbor[p.m_vtx[j]].push_back(i);
		}
	}
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KPolygon2D& p = m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			vector<int>& n0 = vertexNeighbor[p.m_vtx[(j + 1) % 3]];
			vector<int>& n1 = vertexNeighbor[p.m_vtx[(j + 2) % 3]];
			vector<int> n01;
			set_intersection(n0 .begin(), n0 .end(), n1.begin(), n1.end(), back_inserter(n01));
			if ((int)n01.size() == 2) {
				int k = n01.front() == i ?
						n01.back()
					:
						n01.front();
				p.m_neighbor[j] = k;
			} else if ((int)n01.size() == 1) {
				p.m_neighbor[j] = -1;
				m_vertices[p.m_vtx[(j + 1) % 3]].m_isBoundary = true;
				m_vertices[p.m_vtx[(j + 2) % 3]].m_isBoundary = true;
			} else {
				cout << "error in calcNeighbor()" << endl;
				return;
			}
		}
	}
}

double KPolygonModel2D::getArea() const {
	double area = 0;
	for (unsigned int i = 0; i < m_polygons.size(); ++i) {
		const KPolygon2D& p = m_polygons[i];
		const KVector2d& pos0 = m_vertices[p.m_vtx[0]].m_pos;
		const KVector2d& pos1 = m_vertices[p.m_vtx[1]].m_pos;
		const KVector2d& pos2 = m_vertices[p.m_vtx[2]].m_pos;
		area += KUtil::calcArea(pos0, pos1, pos2);
	}
	return area;
}

bool KPolygonModel2D::isInside(
   const KVector2d& pos, unsigned int& polygon_id,
   double& barycoord_p0, double& barycoord_p1, double& barycoord_p2) const
{
	for (polygon_id = 0; polygon_id < m_polygons.size(); ++polygon_id) {
		const KPolygon2D& p = m_polygons[polygon_id];
		const KVector2d& pos0 = m_vertices[p.m_vtx[0]].m_pos;
		const KVector2d& pos1 = m_vertices[p.m_vtx[1]].m_pos;
		const KVector2d& pos2 = m_vertices[p.m_vtx[2]].m_pos;
		KUtil::getBarycentricCoord(barycoord_p0, barycoord_p1, barycoord_p2, pos0, pos1, pos2, pos);
		if (barycoord_p0 >= 0 && barycoord_p1 >= 0 && barycoord_p2 >= 0) return true;
	}
	polygon_id = -1;
	barycoord_p0 = barycoord_p1 = barycoord_p2 = 0;
	return false;
}

void KPolygonModel2D::calcHalfEdge() {
	// register all half-edge data
	m_halfedges.clear();
	m_halfedges.resize(3 * m_polygons.size());
	for (int i = 0; i < m_polygons.size(); ++i) {
		KHalfEdge2D* halfedges[3];
		for (int j = 0; j < 3; ++j) {
			//m_halfedges.push_back(KHalfEdge2D());
			halfedges[j] = &m_halfedges[3 * i + j];
		}
		KPolygon2D& p = m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			int j_next = (j + 1) % 3;
			int j_prev = (j + 2) % 3;
			KHalfEdge2D& halfedge = *halfedges[j];
			halfedge.mp_vtx = &m_vertices[p.m_vtx[j]];
			halfedge.mp_face = &p;
			halfedge.mp_next = halfedges[j_next];
			halfedge.mp_prev = halfedges[j_prev];
			halfedge.mp_vtx ->mp_halfedge = &halfedge;
			halfedge.mp_face->mp_halfedge = &halfedge;
		}
	}
	// calc pair of edges
	calcEdge();
	for (int i = 0; i < m_edges.size(); ++i) {
		KEdge2D& edge = m_edges[i];
		bool found_right = false;
		bool found_left  = false;
		for (int k = 0; k < m_halfedges.size(); ++k) {
			KHalfEdge2D& halfedge = m_halfedges[k];
			if (!found_right && halfedge.mp_vtx == edge.mp_vtx[0] && halfedge.mp_prev->mp_vtx == edge.mp_vtx[1]) {
				edge.mp_halfedge[0] = &halfedge;
				found_right = true;
			}
			if (!found_left  && halfedge.mp_vtx == edge.mp_vtx[1] && halfedge.mp_prev->mp_vtx == edge.mp_vtx[0]) {
				edge.mp_halfedge[1] = &halfedge;
				found_left  = true;
			}
			if (found_right && found_left) break;
		}
		if (found_right) edge.mp_halfedge[0]->mp_edge = &edge;
		if (found_left ) edge.mp_halfedge[1]->mp_edge = &edge;
		if (found_right && found_left) {
			edge.mp_halfedge[0]->mp_pair = edge.mp_halfedge[1];
			edge.mp_halfedge[1]->mp_pair = edge.mp_halfedge[0];
		}
	}
}

void KPolygonModel2D::calcEdge() {
	m_edges.clear();
	for (int i = 0; i < m_polygons.size(); ++i) {
		KPolygon2D& p = m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			int vtx0 = p.m_vtx[j];
			int vtx1 = p.m_vtx[(j + 1) % 3];
			if (vtx0 > vtx1) continue;
			m_edges.push_back(KEdge2D());
			KEdge2D& e = m_edges.back();
			e.mp_vtx[0] = &m_vertices[vtx0];
			e.mp_vtx[1] = &m_vertices[vtx1];
		}
	}
}
