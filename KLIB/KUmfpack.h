#pragma once

#include <vector>
#include "umfpack/umfpack.h"

class KUmfpack {
	int m_size;
	std::vector<int>    m_Ap;
	std::vector<int>    m_Ai;
	std::vector<double> m_Ax;
	//const int* m_Ap;
	//const int   * m_Ai;
	//const double* m_Ax;
	void* mp_symbolic;
	void* mp_numeric;
public:
	KUmfpack() : m_size(0), mp_symbolic(NULL), mp_numeric(NULL) {};
	KUmfpack(const KUmfpack& src) : m_size(src.m_size), m_Ap(src.m_Ap), m_Ai(src.m_Ai), m_Ax(src.m_Ax) {
		if (!m_Ap.empty()) {
			umfpack_di_symbolic(m_size, m_size, &m_Ap[0], &m_Ai[0], &m_Ax[0], &mp_symbolic,              (double*)0, (double*)0);
			umfpack_di_numeric(                 &m_Ap[0], &m_Ai[0], &m_Ax[0],  mp_symbolic, &mp_numeric, (double*)0, (double*)0);
		} else {
			mp_symbolic = NULL;
			mp_numeric  = NULL;
		}
	}
	KUmfpack& operator=(const KUmfpack& src) {
		m_size = src.m_size;
		m_Ap = src.m_Ap;
		m_Ai = src.m_Ai;
		m_Ax = src.m_Ax;
		if (!m_Ap.empty()) {
			umfpack_di_symbolic(m_size, m_size, &m_Ap[0], &m_Ai[0], &m_Ax[0], &mp_symbolic,              (double*)0, (double*)0);
			umfpack_di_numeric(                 &m_Ap[0], &m_Ai[0], &m_Ax[0],  mp_symbolic, &mp_numeric, (double*)0, (double*)0);
		} else {
			mp_symbolic = NULL;
			mp_numeric  = NULL;
		}
		return *this;
	}
	KUmfpack(int size, const std::vector<int>& Ap, const std::vector<int>& Ai, const std::vector<double>& Ax) :
	mp_symbolic(NULL), mp_numeric(NULL) {
		init(size, Ap, Ai, Ax);
		//: m_size(size), m_Ap(Ap), m_Ai(Ai), m_Ax(Ax) {
	//KUmfpack(int size, const int* Ap, const int* Ai, const double* Ax)
	//	: m_size(size), m_Ap(Ap), m_Ai(Ai), m_Ax(Ax) {
		//umfpack_di_symbolic(m_size, m_size, &m_Ap[0], &m_Ai[0], &m_Ax[0], &mp_symbolic,             (double*)0, (double*)0);
		//umfpack_di_numeric(                 &m_Ap[0], &m_Ai[0], &m_Ax[0],  mp_symbolic, &mp_numeric, (double*)0, (double*)0);
		//umfpack_di_symbolic(m_size, m_size, m_Ap, m_Ai, m_Ax, &mp_symbolic,             (double*)0, (double*)0);
		//umfpack_di_numeric(                 m_Ap, m_Ai, m_Ax,  mp_symbolic, &mp_numeric, (double*)0, (double*)0);
	}
	void init(int size, const std::vector<int>& Ap, const std::vector<int>& Ai, const std::vector<double>& Ax) {
		m_size = size;
		m_Ap = Ap;
		m_Ai = Ai;
		m_Ax = Ax;
		if (mp_symbolic != 0) umfpack_di_free_symbolic(&mp_symbolic);
		if (mp_numeric  != 0) umfpack_di_free_numeric (&mp_numeric );
		umfpack_di_symbolic(m_size, m_size, &m_Ap[0], &m_Ai[0], &m_Ax[0], &mp_symbolic,              (double*)0, (double*)0);
		umfpack_di_numeric(                 &m_Ap[0], &m_Ai[0], &m_Ax[0],  mp_symbolic, &mp_numeric, (double*)0, (double*)0);
	}
	~KUmfpack() {
		if (mp_symbolic != NULL) umfpack_di_free_symbolic(&mp_symbolic);
		if (mp_numeric  != NULL) umfpack_di_free_numeric (&mp_numeric );
	}
	//void solve(double* x, const double* b) {
	void solve(std::vector<double>& x, const std::vector<double>& b) {
		x.resize(b.size(), 0);
		umfpack_di_solve(UMFPACK_A, &m_Ap[0], &m_Ai[0], &m_Ax[0], &x[0], &b[0], mp_numeric, (double*)0, (double*)0);
		//umfpack_di_solve(UMFPACK_A, m_Ap, m_Ai, m_Ax, x, b, mp_numeric, (double*)0, (double*)0);
	}
};
