#pragma once

#include <vector>
#include "KMath.h"

//#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/numeric/ublas/vector.hpp>

class KThinPlate3DConstant
{
public:
	//KThinPlate3DConstant(const std::vector<KVector3d>& points);
	KThinPlate3DConstant(void) {}
	~KThinPlate3DConstant(void) {}
	void setPoints(const std::vector<KVector3d>& points);
	void setValues(const std::vector<double>& values);
	double getValue(const KVector3d& pos) const {
		return getValue(pos.x, pos.y, pos.z);
	}
	double getValue(const double x, const double y, const double z) const;
protected:
	//KThinPlate3DConstant(const KThinPlate3DConstant&);
	std::vector<KVector3d> m_points;
	std::vector<double> m_coefficients;
	std::vector<double> m_A;
	
	double phi(const KVector3d& v0, const KVector3d& v1) const {
		return phi(v0.x, v0.y, v0.z, v1.x, v1.y, v1.z);
	}
	double phi(const double x0, const double y0, const double z0, const double x1, const double y1, const double z1) const {
		double dx = x1 - x0;
		double dy = y1 - y0;
		double dz = z1 - z0;
		return sqrt(dx * dx + dy * dy + dz * dz);
	}
};

class KThinPlate3D
{
public:
	KThinPlate3D(void) : m_isReady(false) {}
	~KThinPlate3D(void) {}
	void clear() {
		m_points.clear();
		m_coefficients.clear();
		m_A.clear();
		m_isReady = false;
	}
	bool isReady() { return m_isReady; }
	void setPoints(const std::vector<KVector3d>& points);
	void setValues(const std::vector<double>& values);
	double getValue(const KVector3d& pos) const {
		return getValue(pos.x, pos.y, pos.z);
	}
	double getValue(const double x, const double y, const double z) const;
	KVector3d calcGradient(const KVector3d& pos, double delta = 0.0001) {
		double f = getValue(pos);
		double fx = getValue(pos.x + delta, pos.y, pos.z) - f;
		double fy = getValue(pos.x, pos.y + delta, pos.z) - f;
		double fz = getValue(pos.x, pos.y, pos.z + delta) - f;
		KVector3d g(fx, fy, fz);
		g.scale(1 / delta);
		return g;
	}
protected:
	std::vector<KVector3d> m_points;
	std::vector<double> m_coefficients;
	std::vector<double> m_A;
	bool m_isReady;
	//typedef boost::numeric::ublas::matrix<double> dmatrix;
	//typedef boost::numeric::ublas::vector<double> dvector;
	//dvector m_coefficients;
	//dmatrix m_A;
	
	double phi(const KVector3d& v0, const KVector3d& v1) const {
		return phi(v0.x, v0.y, v0.z, v1.x, v1.y, v1.z);
	}
	double phi(const double x0, const double y0, const double z0, const double x1, const double y1, const double z1) const {
		double dx = x1 - x0;
		double dy = y1 - y0;
		double dz = z1 - z0;
		return sqrt(dx * dx + dy * dy + dz * dz);
	}
};

class KThinPlate2D
{
public:
	KThinPlate2D(void) {}
	~KThinPlate2D(void) {}
	void setPoints(const std::vector<KVector2d>& points);
	void setValues(const std::vector<double>& values);
	double getValue(const KVector2d& pos) const {
		return getValue(pos.x, pos.y);
	}
	double getValue(const double x, const double y) const;
protected:
	std::vector<KVector2d> m_points;
	std::vector<double> m_coefficients;
	std::vector<double> m_A;
	
	double phi(const KVector2d& v0, const KVector2d& v1) const {
		return phi(v0.x, v0.y, v1.x, v1.y);
	}
	double phi(const double x0, const double y0, const double x1, const double y1) const {
		double dx = x1 - x0;
		double dy = y1 - y0;
		double r = dx * dx + dy * dy;
		if (r == 0) return 0;
		return r * log(sqrt(r));
	}
};

class KThinPlate2DConstant
{
public:
	KThinPlate2DConstant(void) {}
	~KThinPlate2DConstant(void) {}
	void setPoints(const std::vector<KVector2d>& points);
	void setValues(const std::vector<double>& values);
	double getValue(const KVector2d& pos) const {
		return getValue(pos.x, pos.y);
	}
	double getValue(const double x, const double y) const;
protected:
	std::vector<KVector2d> m_points;
	std::vector<double> m_coefficients;
	std::vector<double> m_A;
	
	double phi(const KVector2d& v0, const KVector2d& v1) const {
		return phi(v0.x, v0.y, v1.x, v1.y);
	}
	double phi(const double x0, const double y0, const double x1, const double y1) const {
		double dx = x1 - x0;
		double dy = y1 - y0;
		double r = dx * dx + dy * dy;
		if (r == 0) return 0;
		return r * log(sqrt(r));
	}
};
