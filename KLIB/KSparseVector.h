#pragma once

#include <vector>

class KSparseVector
{
	int m_size;
	std::vector<int>    m_Ai;
	std::vector<double> m_Ax;
public:
	KSparseVector(void) { init(0); }
	KSparseVector(int size, int initDataSize = 16) { init(size, initDataSize); }
	~KSparseVector(void) {}
	void init(int size, int initDataSize = 16);
	int getSize() const { return m_size; }
	std::vector<int>    getAi() const { return m_Ai; }
	std::vector<double> getAx() const { return m_Ax; }
	double getValue(int index) const;
	void   setValue(int index, double val);
	void   addValue(int index, double val);
	double innerProduct(const KSparseVector& vec)  const;
};
