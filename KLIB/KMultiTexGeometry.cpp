#include "StdAfx.h"
#include ".\kmultitexgeometry.h"

#include <iostream>
#include <fstream>
#include <map>
#include <list>

#include "KUtil.h"

using namespace std;

void KMultiTexPolygonModel::calcNeighbor() {
	vector<vector<int> > vertexNeighbor(m_vertices.size());
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KMultiTexPolygon& p = m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			vertexNeighbor[p.m_vtx[j]].push_back(i);
		}
	}
	static int order[][2] = {{1, 2}, {2, 0}, {0, 1}};
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KMultiTexPolygon& p = m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			vector<int>& n0 = vertexNeighbor[p.m_vtx[order[j][0]]];
			vector<int>& n1 = vertexNeighbor[p.m_vtx[order[j][1]]];
			vector<int> n01;
			set_intersection(n0 .begin(), n0 .end(), n1.begin(), n1.end(), back_inserter(n01));
			if ((int)n01.size() == 2) {
				int k = n01.front() == i ? n01.back() : n01.front();
				p.m_neighbor[j] = k;
			} else if ((int)n01.size() == 1) {
				p.m_neighbor[j] = -1;
			} else {
				throw "error in KMultiTexPolygonModel::calcNeighbor()";
			}
		}
	}
}
void KMultiTexPolygonModel::calcSmoothNormals(double threshold) {
	vector<map<int, KVector3d> > normals(m_vertices.size());
	vector<list<int> > plists(m_vertices.size());
	for (int i = 0; i < (int)m_polygons.size(); ++i) {
		KMultiTexPolygon& p = m_polygons[i];
		KVector3d& v0 = m_vertices[p.m_vtx[0]].m_pos;
		KVector3d& v1 = m_vertices[p.m_vtx[1]].m_pos;
		KVector3d& v2 = m_vertices[p.m_vtx[2]].m_pos;
		KVector3d& n = KUtil::calcNormal(v0, v1, v2);
		for (int j = 0; j < 3; ++j) {
			normals[p.m_vtx[j]].insert(pair<int, KVector3d>(i, n));
			plists[p.m_vtx[j]].push_back(i);
		}
	}
	bool isLooped = true;
	for (int i = 0; i < (int)m_vertices.size(); ++i) {
		list<int>& plist = plists[i];
		int pID = plist.front();
		vector<int> pOrdered;
		pOrdered.reserve(plist.size());
		pOrdered.push_back(pID);
		plist.pop_front();
		while (!plist.empty()) {
			KMultiTexPolygon& p = m_polygons[pID];
			int vID;
			for (int j = 0; j < 3; ++j) {
				if (p.m_vtx[j] == i) {
					vID = p.m_vtx[(j + 1) % 3];
					break;
				}
			}
			list<int>::iterator findPos = plist.begin();
			for (; findPos != plist.end(); ++findPos) {
				KMultiTexPolygon& q = m_polygons[*findPos];
				if (q.m_vtx[0] == vID || q.m_vtx[1] == vID || q.m_vtx[2] == vID) break;
			}
			if (findPos == plist.end()) {
				isLooped = false;
				break;
			}
			pID = *findPos;
			pOrdered.push_back(pID);
			plist.erase(findPos);
		}
		if (!isLooped) {
			KVector3d avg;
			for (map<int, KVector3d>::iterator j = normals[i].begin(); j != normals[i].end(); ++j)
				avg.add(j->second);
			avg.normalize();
			for (map<int, KVector3d>::iterator j = normals[i].begin(); j != normals[i].end(); ++j) {
				KMultiTexPolygon& q = m_polygons[j->first];
				for (int k = 0; k < 3; ++k)
					if (q.m_vtx[k] == i) {
						q.m_normal[k] = avg;
						break;
					}
			}
			continue;
		}
		int N = (int)pOrdered.size();
		vector<KVector3d> nOrdered(N);
		for (int j = 0; j < N; ++j)
			nOrdered[j] = normals[i].find(pOrdered[j])->second;
		vector<bool> isSeparated(N);
		int cntSeparated = 0;
		int i0;
		for (int j = 0; j < N; ++j) {
			KVector3d& p0 = nOrdered[j];
			KVector3d& p1 = nOrdered[(j + 1) % N];
			bool b = p0.dot(p1) < threshold;
			isSeparated[j] = b;
			if (b) {
				++cntSeparated;
				i0 = j;
			}
		}
		if (cntSeparated <= 1) {
			KVector3d avg;
			for (map<int, KVector3d>::iterator j = normals[i].begin(); j != normals[i].end(); ++j)
				avg.add(j->second);
			avg.normalize();
			for (int j = 0; j < N; ++j) {
				KMultiTexPolygon& q = m_polygons[pOrdered[j]];
				for (int k = 0; k < 3; ++k)
					if (q.m_vtx[k] == i) {
						q.m_normal[k] = avg;
						break;
					}
			}
			continue;
		}
		int cnt = 0;
		while (cnt < N) {
			int i1 = (i0 + 1) % N;
			while (!isSeparated[i1]) i1 = (i1 + 1) % N;
			i0 = (i0 + 1) % N;
			i1 = (i1 + 1) % N;
			KVector3d avg;
			for (int j = i0; j != i1; j = (j + 1) % N) {
				avg.add(nOrdered[j]);
			}
			avg.normalize();
			for (int j = i0; j != i1; j = (j + 1) % N) {
				KMultiTexPolygon& q = m_polygons[pOrdered[j]];
				for (int k = 0; k < 3; ++k)
					if (q.m_vtx[k] == i) {
						q.m_normal[k] = avg;
						break;
					}
				++cnt;
			}
			i0 = (i1 + N - 1) % N;
		}
	}
}

bool KMultiTexTetraModel::loadFile(const string& fname) {
	vector<KVertex> verticesTmp;
	vector<KMultiTexTetra>  tetrasTmp;
	
	string line;
	ifstream ifs;
	
	const string& fnameNode = fname + ".node";
	ifs.open(fnameNode.c_str());
	if (!ifs.is_open()) {
		printf("Could not open file: %s\n", fnameNode.c_str());
		return false;
	}
	int numVtx;
	ifs >> numVtx;
	int tmp;
	ifs >> tmp >> tmp >> tmp;
	verticesTmp.reserve(numVtx);
	for (int i = 0; i < numVtx; ++i) {
		ifs >> tmp;
		double x, y, z;
		ifs >> x >> y >> z;
		KVertex v(KVector3d(x, y, z));
		verticesTmp.push_back(v);
	}
	ifs.close();
	
	const string& fnameEle = fname + ".ele";
	ifs.open(fnameEle.c_str());
	if (!ifs.is_open()) return false;
	int numTet;
	ifs >> numTet;
	ifs >> tmp >> tmp;
	tetrasTmp.reserve(numTet);
	for (int i = 0; i < numTet; ++i) {
		ifs >> tmp;
		int i0, i1, i2, i3;
		ifs >> i0 >> i1 >> i2 >> i3;
		KMultiTexTetra tetra(i0, i1, i2, i3);
		tetrasTmp.push_back(tetra);
	}
	ifs.close();
	
	m_vertices = verticesTmp;
	m_tetras   = tetrasTmp;
	return true;
}
void KMultiTexTetraModel::calcNeighbor() {
	vector<vector<int> > vertexNeighbor(m_vertices.size());
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		KMultiTexTetra& tet = m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			vertexNeighbor[tet.m_vtx[j]].push_back(i);
		}
	}
	static int order[][3] = {{3, 2, 1}, {0, 2, 3}, {0, 3, 1}, {0, 1, 2}};
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		KMultiTexTetra& tet = m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			vector<int>& n0 = vertexNeighbor[tet.m_vtx[order[j][0]]];
			vector<int>& n1 = vertexNeighbor[tet.m_vtx[order[j][1]]];
			vector<int>& n2 = vertexNeighbor[tet.m_vtx[order[j][2]]];
			vector<int> n01;
			set_intersection(n0 .begin(), n0 .end(), n1.begin(), n1.end(), back_inserter(n01));
			vector<int> n012;
			set_intersection(n01.begin(), n01.end(), n2.begin(), n2.end(), back_inserter(n012));
			if ((int)n012.size() == 2) {
				int k = n012.front() == i ?
						n012.back()
					:
						n012.front();
				tet.m_neighbor[j] = k;
			} else if ((int)n012.size() == 1) {
				tet.m_neighbor[j] = -1;
			} else {
				for (int k = 0; k < (int)m_tetras.size(); ++k) {
					KMultiTexTetra& tetk = m_tetras[k];
					cout << tetk.m_vtx[0] << ", " << tetk.m_vtx[1] << ", " << tetk.m_vtx[2] << ", " << tetk.m_vtx[3] << endl;
				}
				cout << "error in calcNeighbor()" << endl;
				return;
			}
		}
	}
}

KMultiTexPolygonModel KMultiTexTetraModel::toMultiTexPolygonModel() {
	vector<KVertex> vertices;
	vector<KMultiTexPolygon> polygons;
	map<int, int> mapV2W;
	for (int i = 0; i < (int)m_tetras.size(); ++i) {
		KMultiTexTetra& tet = m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			if (tet.m_neighbor[j] != -1) continue;
			int w[3];
			for (int k = 0; k < 3; ++k) {
				int v = tet.m_vtx[KTetraModel::FACE_VTX_ORDER[j][k]];
				map<int, int>::iterator findPos = mapV2W.find(v);
				if (findPos != mapV2W.end()) {
					w[k] = findPos->second;
				} else {
					w[k] = (int)vertices.size();
					mapV2W.insert(pair<int, int>(v, w[k]));
					vertices.push_back(m_vertices[v]);
				}
			}
			KMultiTexPolygon p(w[0], w[1], w[2]);
			vector<KPolygonTexCoord> texCoords;
			for (int k = 0; k < (int)tet.m_texCoords.size(); ++k) {
				KTetraTexCoord& texTetra = tet.m_texCoords[k];
				KVector3d& t0 = texTetra.m_coord[KTetraModel::FACE_VTX_ORDER[j][0]];
				KVector3d& t1 = texTetra.m_coord[KTetraModel::FACE_VTX_ORDER[j][1]];
				KVector3d& t2 = texTetra.m_coord[KTetraModel::FACE_VTX_ORDER[j][2]];
				KPolygonTexCoord texPoly(t0, t1, t2);
				texCoords.push_back(texPoly);
			}
			p.m_texCoords = texCoords;
			p.m_texIDs = tet.m_texIDs;
			polygons.push_back(p);
		}
	}
	KMultiTexPolygonModel result;
	result.m_vertices = vertices;
	result.m_polygons = polygons;
	return result;
}

