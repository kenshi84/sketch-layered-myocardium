#pragma once

#include "State.h"
#include "Core.h"

class StateDeformTetra : public State
{
	StateDeformTetra(void) {}
	~StateDeformTetra(void) {}
public:
	static StateDeformTetra* getInstance() {
		static StateDeformTetra p;
		return &p;
	}
	
	// interface of [State] begin
	State* next() {
		return NULL;
	}
	bool isReady() {
		return false;
	}
	void init();
	void draw();
	void postDraw(CWnd* hWnd, CDC* pDC);
	void OnLButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnMouseMove  (CView* view, UINT nFlags, CPoint& point);
	void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags);
	bool load(const std::string& fname);
	void undo() {}
	bool isUndoable() { return false; }
	void save();
	bool isSavable() { return true; }
	// end
	
	void updateFiber();

	bool m_isSettingRadius;
	bool m_isSettingOffset;
protected:
	bool m_isCutting;
	CPoint m_pointOld;
	bool m_isDrawFace;
	bool m_isDrawFrame;
	KPolygonModel m_polyContour;
	void calcPolyContour();
};
