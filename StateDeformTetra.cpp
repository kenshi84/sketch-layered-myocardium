#include "StdAfx.h"
#include "StateDeformTetra.h"
#include "Drawer.h"
#include "MainFrm.h"

#include <KLIB/KUtil.h>
#include <KLIB/KDrawer.h>

#include <fstream>

using namespace std;

void StateDeformTetra::init() {
	m_isDrawFace = true;
	m_isDrawFrame = false;
	calcPolyContour();
}

void StateDeformTetra::draw() {
	Core& core = *Core::getInstance();
	if (m_isDrawFace) {
		if (core.m_showStreamLines) {
			glDisable(GL_LIGHTING);
			vector<vector<pair<KVector3d, double> > >& streamLines =
				core.m_viewMode == Core::VIEWMODE_CUT ? core.m_streamLinesPoly : core.m_streamLinesPolyLayers[core.m_currentLayer];
			glColor3dv(Drawer::COLOR_FACE);
			glEnable(GL_TEXTURE_1D);
			glBindTexture(GL_TEXTURE_1D, core.m_drawer.m_texNameDepth);
			glEnable(GL_BLEND);
			glLineWidth(2);
			for (int i = 0; i < (int)streamLines.size(); ++i) {
				vector<pair<KVector3d, double> >& streamLine = streamLines[i];
				glBegin(GL_LINE_STRIP);
				for (int j = 0; j < (int)streamLine.size(); ++j) {
					glTexCoord1d(-0.5 * streamLine[j].second);
					glVertex3dv(streamLine[j].first.getPtr());
				}
				glEnd();
			}
			glDisable(GL_BLEND);
			glDisable(GL_TEXTURE_1D);
			glEnable(GL_LIGHTING);
		} else {
			KPolygonModel& poly =
				core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
			glColor3dv(Drawer::COLOR_FACE);
			glBegin(GL_TRIANGLES);
			for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
				KPolygon& p = poly.m_polygons[i];
				for (int j = 0; j < 3; ++j) {
					KVector3d& n = p.m_normal[j];
					KVector3d& v = poly.m_vertices[p.m_vtx[j]].m_pos;
					glNormal3dv(n.getPtr());
					glVertex3dv(v.getPtr());
				}
			}
			glEnd();
			glColor3dv(Drawer::COLOR_STROKE);
			for (int i = 0; i < (int)core.m_strokes.size(); ++i) {
				vector<KVector3d>& stroke = core.m_strokes[i];
				for (int j = 1; j < (int)stroke.size(); ++j) {
					KVector3d& start = stroke[j - 1];
					KVector3d  ori   = stroke[j];
					ori.sub(start);
					double radius = 0.01;
					KDrawer::drawCylinder(start, ori, radius);
					if (j == (int)stroke.size() - 1) KDrawer::drawCone(stroke.back(), ori, 2 * radius, 0.05);
				}
			}
		}
	}
	if (m_isSettingRadius || m_isSettingOffset) {
		glColor3d(1, 0, 0);
		double r = 0.01;
		KDrawer::drawSphere(core.m_deformOrigin, r);
		glColor3d(0, 0, 1);
		for (int i = 0; i < (int)core.m_deformVtxList.size(); ++i)
			KDrawer::drawSphere(core.m_tetra.m_vertices[core.m_deformVtxList[i]].m_pos, r);
	}
	if (m_isSettingOffset) {
		glColor3d(1, 0, 0);
		KDrawer::drawArrow(core.m_deformOrigin, core.m_deformOffset);
		glColor3d(0, 0, 1);
		for (int i = 0; i < (int)core.m_deformVtxList.size(); ++i)
			KDrawer::drawArrow(core.m_tetra.m_vertices[core.m_deformVtxList[i]].m_pos, core.m_deformVtxOffset[i]);
	}
	if (m_isSettingRadius) {
		glEnable(GL_BLEND);
		glColor4d(1, 1, 0.5, 0.5);
		KDrawer::drawSphere(core.m_deformOrigin, core.m_deformRadius);
		glDisable(GL_BLEND);
	}
	if (m_isDrawFrame) {
		glLineWidth(1);
		glColor3d(0.5, 0.5, 0.5);
		KDrawer::drawWireBox(0, 0, 0, 1, 1, 1);
		glLineWidth(5);
		KDrawer::drawAxis();
	}
	if (!m_isDrawFace) {
		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glDisable(GL_LIGHTING);
		glColor4dv(Drawer::COLOR_CONTOUR);
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < (int)m_polyContour.m_polygons.size(); ++i) {
			KPolygon& p = m_polyContour.m_polygons[i];
			for (int j = 0; j < 3; ++j)
				glVertex3dv(m_polyContour.m_vertices[p.m_vtx[j]].m_pos.getPtr());
		}
		glEnd();
		glDisable(GL_BLEND);
	}
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	glColor3dv(Drawer::COLOR_CUTSTROKE);
	glLineWidth(5);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < (int)core.m_cutStroke.size(); ++i)
		glVertex3dv(core.m_cutStroke[i].getPtr());
	glEnd();
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}

void StateDeformTetra::postDraw(CWnd* hWnd, CDC* pDC) {
	Core& core = *Core::getInstance();
	CFont font;
	CFont *pOldFont;
	font.CreatePointFont ( 300, "Comic Sans MS", pDC );
	pOldFont = pDC->SelectObject ( &font );
	pDC->SetBkMode ( TRANSPARENT );
	pDC->SetTextColor ( RGB ( 255, 0, 0 ) );
	CRect rc;
	hWnd->GetClientRect ( &rc );
	pDC->DrawText ( "Deform the tetra model!", -1, &rc, DT_LEFT | DT_TOP | DT_SINGLELINE);
	pDC->SelectObject ( pOldFont );
}

void StateDeformTetra::OnLButtonDown(CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	KVector3d start, ori;
	core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
	KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
	KVector3d pos;
	int polyID;
	m_pointOld = point;
	if (m_isSettingOffset) {
		m_isSettingOffset = false;
		vector<KVector3d> vtxPosNew;
		vtxPosNew.reserve(core.m_tetra.m_vertices.size());
		for (int i = 0; i < (int)core.m_tetra.m_vertices.size(); ++i)
			vtxPosNew.push_back(core.m_tetra.m_vertices[i].m_pos);
		for (int i = 0; i < (int)core.m_deformVtxOffset.size(); ++i)
			vtxPosNew[core.m_deformVtxList[i]].add(core.m_deformVtxOffset[i]);
		core.deformTetra(vtxPosNew);
		calcPolyContour();
	} else if (!core.m_showStreamLines && KUtil::getIntersection(pos, polyID, poly, start, ori)) {
		core.m_deformOrigin = pos;
		m_isSettingRadius = true;
		core.m_deformRadius = 0;
		core.m_deformVtxList.clear();
	} else if (core.m_viewMode == Core::VIEWMODE_CUT) {
		start.addWeighted(ori, 0.5);
		core.m_cutStroke.push_back(start);
		m_isCutting = true;
		core.initPoly();
		core.initStreamLinesPoly();
	}
	core.m_ogl.RedrawWindow();
}

void StateDeformTetra::OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isSettingRadius) {
		m_isSettingRadius = false;
		m_isSettingOffset = true;
		return;
	}
	if (!m_isCutting) return;
	m_isCutting = false;
	if (core.m_cutStroke.size() == 1) {
		core.m_cutStroke.clear();
		return;
	} else {
		core.calcPoly(core.m_cutStroke);
		core.calcStreamLinesPoly(core.m_cutStroke);
	}
	core.m_cutStroke.clear();
	core.m_ogl.RedrawWindow();
}

void StateDeformTetra::OnMouseMove  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isCutting || m_isSettingRadius || m_isSettingOffset) {
		KVector3d start, ori;
		core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
		if (m_isCutting) {
			double dx = point.x - m_pointOld.x;
			double dy = point.y - m_pointOld.y;
			double dist = sqrt(dx * dx + dy * dy);
			if (dist < 20) return;
			m_pointOld = point;
			KVector3d pos(start);
			pos.addWeighted(ori, 0.5);
			core.m_cutStroke.push_back(pos);
		} else {
			KVector3d e(core.m_ogl.m_eyePoint);
			KVector3d ep(core.m_deformOrigin);
			ep.sub(e);
			KVector3d x(e);
			x.addWeighted(ori, ep.lengthSquared() / ori.dot(ep));
			x.sub(core.m_deformOrigin);
			if (m_isSettingRadius) {
				core.m_deformRadius = x.length();
				core.calcDeformVtxList(core.m_deformOrigin, core.m_deformRadius);
			} else {
				core.m_deformOffset = x;
				core.calcDeformVtxOffset(core.m_deformOffset);
			}
		}
		core.m_ogl.RedrawWindow();
	}
}

void StateDeformTetra::OnKeyDown(CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) {
	Core& core = *Core::getInstance();
	switch (nChar) {
		case VK_SPACE:
			m_isDrawFace = !m_isDrawFace;
			core.m_ogl.RedrawWindow();
			break;
		case VK_RETURN:
			m_isDrawFrame = !m_isDrawFrame;
			core.m_ogl.RedrawWindow();
			break;
	}
}

bool StateDeformTetra::load(const string& fname){
	Core& core = *Core::getInstance();
	string ext = fname.substr(fname.size() - 5, 5);
	if (ext.compare(".node")) {
		CString str;
		str.Format("Please drop *.node file");
		AfxMessageBox(str);
		return false;
	}
	FILE * fin = fopen(fname.c_str(), "r");
	if (!fin) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return false;
	}
	int numVtx, tmp;
	ifstream ifs(fname.c_str());
	ifs >> numVtx >> tmp >> tmp >> tmp;
	if (numVtx != (int)core.m_tetra.m_vertices.size()) {
		AfxMessageBox("Incompatible number of vertices");
		return false;
	}
	vector<KVector3d> vtxPosNew;
	vtxPosNew.reserve(numVtx);
	for (int i = 0; i < numVtx; ++i) {
		double x, y, z;
		ifs >> tmp >> x >> y >> z;
		vtxPosNew.push_back(KVector3d(x, y, z));
	}
	core.deformTetra(vtxPosNew);
	core.initPoly();
	core.calcPolyLayers();
	calcPolyContour();
	return true;
}

void StateDeformTetra::save() {
	Core& core = *Core::getInstance();
	CFileDialog dialog(FALSE, ".node", NULL, OFN_OVERWRITEPROMPT, "Node files(*.node)|*.node||");
	if (dialog.DoModal() != IDOK) return;
	string fname = dialog.GetPathName();
	FILE * fout = fopen(fname.c_str(), "w");
	if (!fout) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return;
	}
	fprintf(fout, "%d 3 0 0\n", core.m_tetra.m_vertices.size());
	for (int i = 0; i < (int)core.m_tetra.m_vertices.size(); ++i) {
		KVector3d& pos = core.m_tetra.m_vertices[i].m_pos;
		fprintf(fout, "%d %f %f %f\n", i, pos.x, pos.y, pos.z);
	}
	fclose(fout);
}

void StateDeformTetra::updateFiber() {
	Core& core = *Core::getInstance();
	core.calcLaplacian();
	core.calcVtxFiber();
	core.calcVolTetID();
	core.calcVolDepth();
	core.calcVolFiber();
	core.calcStreamLines();
	core.initStreamLinesPoly();
	core.calcStreamLinesPolyLayers();
}

void StateDeformTetra::calcPolyContour() {
	Core& core = *Core::getInstance();
	m_polyContour = core.m_tetra.toPolygonModel();
}
