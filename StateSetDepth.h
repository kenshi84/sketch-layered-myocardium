#pragma once

#include "State.h"
#include "Core.h"

class StateSetDepth : public State
{
	static const double RBFVALUE_EPI;
	static const double RBFVALUE_SEPT;
	static const double RBFVALUE_RV;
	static const double RBFVALUE_LV;

	static const double COLOR_EPI [3];
	static const double COLOR_SEPT[3];
	static const double COLOR_RV  [3];
	static const double COLOR_LV  [3];
	
	StateSetDepth(void) {}
	~StateSetDepth(void) {}
public:
	static StateSetDepth* getInstance() {
		static StateSetDepth p;
		return &p;
	}

	// interface of [State] begin
	State* next();
	bool isReady() {
		return Core::getInstance()->m_rbfDepth.isReady();
	}
	void init();
	void draw();
	void postDraw(CWnd* hWnd, CDC* pDC);
	void OnLButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnMouseMove  (CView* view, UINT nFlags, CPoint& point);
	void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags);
	bool load(const std::string& fname);
	void undo();
	bool isUndoable();
	void save();
	bool isSavable();
	// end

	void updateDepth();

	enum DrawMode {
		DRAWMODE_EPI,
		DRAWMODE_SEPT,
		DRAWMODE_RV,
		DRAWMODE_LV
	} m_drawMode;
protected:
	bool m_isCutting;
	bool m_isPutting;
	CPoint m_pointOld;
	std::vector<KVector3d> m_rbfPointsEpi;
	std::vector<KVector3d> m_rbfPointsSept;
	std::vector<KVector3d> m_rbfPointsRv;
	std::vector<KVector3d> m_rbfPointsLv;
	std::vector<bool> m_isVisibleEpi;
	std::vector<bool> m_isVisibleSept;
	std::vector<bool> m_isVisibleRv;
	std::vector<bool> m_isVisibleLv;
	void calcVisible(const std::vector<KVector3d>& cutStroke);
	void initVisible();
	
	bool m_isDrawFace;
	bool m_isDrawPoint;
};
