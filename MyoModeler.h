// MyoModeler.h : main header file for the MyoModeler application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMyoModelerApp:
// See MyoModeler.cpp for the implementation of this class
//

class CMyoModelerApp : public CWinApp
{
public:
	CMyoModelerApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMyoModelerApp theApp;