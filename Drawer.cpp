#include "StdAfx.h"
#include "Drawer.h"

#include "Core.h"
#include <KLIB/KDrawer.h>
#include <KLIB/KUtil.h>

#include <gl/glext.h>
//extern PFNGLTEXIMAGE3DPROC glTexImage3D;

#include <vector>

using namespace std;

const double Drawer::COLOR_EDGE      [3] = {0.5, 0.5, 0.5};
const double Drawer::COLOR_FACE      [3] = {1, 1, 1};
const double Drawer::COLOR_CUTSTROKE [3] = {1, 0, 0};
const double Drawer::COLOR_STROKE    [3] = {1, 0.5, 0};
const double Drawer::COLOR_CONTOUR   [4] = {0, 0, 1, 0.125};

Drawer::Drawer(void) {
}

void Drawer::init() {
	Core& core = *Core::getInstance();
	core.m_ogl.m_clearColor[0] = core.m_ogl.m_clearColor[1] = core.m_ogl.m_clearColor[2] = 1.0f;
	core.m_ogl.m_clearColor[3] = 0.0f;
	core.m_ogl.makeOpenGLCurrent();
	
	glEnable(GL_LIGHTING);
	glDisable(GL_LIGHT1);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	genTexDepth();
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void Drawer::draw() {
	Core::getInstance()->m_state->draw();
}

void Drawer::postDraw(CWnd* hWnd, CDC* pDC) {
	Core::getInstance()->m_state->postDraw(hWnd, pDC);
}

void Drawer::genTexDepth() {
	int texSize = 128;
	int texSize_2 = texSize / 2;
	vector<GLubyte> data(texSize * 3);
	for (int i = 0; i < texSize; ++i) {
		int r = 512 - 1024 * i / texSize;
		int g = 512 - 512 * abs(i - texSize_2) / texSize_2;
		int b = -512 + 1024 * i / texSize;
		GLubyte r_ = (GLubyte)max(min(r, 255), 0);
		GLubyte g_ = (GLubyte)max(min(g, 255), 0);
		GLubyte b_ = (GLubyte)max(min(b, 255), 0);
		data[3 * i]     = r_;
		data[3 * i + 1] = g_;
		data[3 * i + 2] = b_;
	}
	Core& core = *Core::getInstance();
	core.m_ogl.makeOpenGLCurrent();
	glGenTextures(1, &m_texNameDepth);
	glBindTexture(GL_TEXTURE_1D, m_texNameDepth);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, texSize, 0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);
}


//void Drawer::genTexLIC(int volSize, const vector<KVector3d>& volVector, int lenLIC) {
//	int& N   = volSize;
//	int NN  = N  * N;
//	int NNN = NN * N;
//	vector<double> bufNoise(NNN);
//	for (int i = 0; i < NNN; ++i)
//		bufNoise[i] = rand() / (double)RAND_MAX;
//	vector<GLubyte> data(NNN * 3);
//	for (int iz = 0; iz < N; ++iz)
//	for (int iy = 0; iy < N; ++iy)
//	for (int ix = 0; ix < N; ++ix) {
//		int index = ix + N * iy + NN * iz;
//		const KVector3d& vec = volVector[index];
//		double lumi = 0;
//		for (int k = -lenLIC; k <= lenLIC; ++k) {
//			int px = ix + (int)(k * vec.x);
//			int py = iy + (int)(k * vec.y);
//			int pz = iz + (int)(k * vec.z);
//			while (px < 0) px += N;
//			while (py < 0) py += N;
//			while (pz < 0) pz += N;
//			while (N <= px) px -= N;
//			while (N <= py) py -= N;
//			while (N <= pz) pz -= N;
//			double noise = bufNoise[px + N * py + NN * pz];
//			lumi += noise;
//		}
//		lumi /= (2 * lenLIC + 1);
//		GLubyte rgb = (GLubyte)(0xFF * lumi);
//		data[3 * index] = data[3 * index + 1] = data[3 * index + 2] = rgb;
//	}
//	
//	Core& core = *Core::getInstance();
//	core.m_ogl.MakeOpenGLCurrent();
//	if (glTexImage3D == 0) glTexImage3D = (PFNGLTEXIMAGE3DPROC)wglGetProcAddress("glTexImage3D");
//	glGenTextures(1, &m_texNameLIC);
//	glBindTexture(GL_TEXTURE_3D, m_texNameLIC);
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
//	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, N, N, N, 0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);
//}
