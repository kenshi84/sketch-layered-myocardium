#include "StdAfx.h"
#include "Drawer.h"
#include "MainFrm.h"
#include <KLIB/KUtil.h>
#include <KLIB/KDrawer.h>
#include "StateDrawStroke.h"
#include "StateSetS1.h"
#include "MinSelector.h"

#include <iomanip>

#include <gl/glext.h>

using namespace std;

State* StateDrawStroke::next() {
	return 0;//StateSetS1::getInstance();
}

namespace {
	double bar_radius = 0.005;
	double bar_length = 0.02;
	bool showInterpolatedBars = false;
	bool isDrawingConstraintStroke = false;
}

void StateDrawStroke::init() {
	Core& core = *Core::getInstance();
	CMainFrame& f = *core.p_mainFrm;
	f.ShowControlBar(&f.m_wndToolBar_setdepth, false, true);
	f.ShowControlBar(&f.m_wndToolBar_drawstroke, true, true);
	core.m_strokes.clear();
	core.m_tetVtxConstraintDirection.clear();
	core.m_tetVtxConstraintDirection.resize(core.m_tetra.m_vertices.size(), {false, Vector3d::Zero()});
	m_selectedVtx = -1;
}

void StateDrawStroke::draw() {
	Core& core = *Core::getInstance();
	if (core.m_showStreamLines) {
		glDisable(GL_LIGHTING);
		vector<vector<pair<KVector3d, double> > >& streamLines =
			core.m_viewMode == Core::VIEWMODE_CUT ? core.m_streamLinesPoly : core.m_streamLinesPolyLayers[core.m_currentLayer];
		// glColor3dv(Drawer::COLOR_FACE);
		// glEnable(GL_TEXTURE_1D);
		// glBindTexture(GL_TEXTURE_1D, core.m_drawer.m_texNameDepth);
		// glLineWidth(2);
		srand(0);
		glEnable(GL_BLEND);
		for (int i = 0; i < (int)streamLines.size(); ++i) {
			vector<pair<KVector3d, double> >& streamLine = streamLines[i];
			glBegin(GL_LINE_STRIP);
			Vector3d color = Vector3d::Constant(0.6) + 0.25 * Vector3d::Random();
			glColor3dv(color.data());
			for (int j = 0; j < (int)streamLine.size(); ++j) {
				//if (j < (int)streamLine.size() - 1) {
				//	KVector3d d = streamLine[j + 1].first - streamLine[j].first;
				//	if (d.sum() > 0)
				//		d.scale(-1);
				//	d.normalize();
				//	d.add(1,1,1);
				//	d.scale(0.5);
				//	glColor3dv(d.getPtr());
				//}
				// glTexCoord1d(-0.5 * streamLine[j].second);
				glVertex3dv(streamLine[j].first.getPtr());
			}
			glEnd();
		}
		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_1D);
		glEnable(GL_LIGHTING);
	} else {
		KPolygonModel& poly =
			core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
		glColor3dv(Drawer::COLOR_FACE);
		//if (m_useTexture) {
		//	glEnable(GL_TEXTURE_3D);
		//	glBindTexture(GL_TEXTURE_3D, core.m_drawer.m_texNameLIC);
		//}
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
			KPolygon& p = poly.m_polygons[i];
			for (int j = 0; j < 3; ++j) {
				KVector3d& n = p.m_normal[j];
				KVector3d& v = poly.m_vertices[p.m_vtx[j]].m_pos;
				glNormal3dv(n.getPtr());
				//if (m_useTexture) glTexCoord3dv(v.getPtr());
				glVertex3dv(v.getPtr());
			}
		}
		glEnd();
		glColor3d(0, 0, 0);
		glPointSize(2);
		glBegin(GL_POINTS);
		for (int i = 0; i < (int)poly.m_vertices.size(); ++i) {
			glVertex3dv(poly.m_vertices[i].m_pos.getPtr());
		}
		glEnd();
		for (size_t i = 0; i < core.m_tetra.m_vertices.size(); ++i) {
			if (!core.m_isTetVtxBoundary[i]) continue;
			if (!core.m_tetVtxConstraintDirection[i].first) {
				if (!showInterpolatedBars)
					continue;
				KVector3d pos = core.m_tetra.m_vertices[i].m_pos;
				KVector3d dir = vector3_cast<KVector3d>(get_minor_eigenvector(vector_to_tensor(core.m_vtxFiber.row(i))));
				glColor3d(0.5, 0.25, 0);
				KDrawer::drawCylinder(pos - 0.5 * bar_length * dir, bar_length * dir, 0.5 * bar_radius);
				continue;
			}
			KVector3d pos = core.m_tetra.m_vertices[i].m_pos;
			KVector3d dir = vector3_cast<KVector3d>(core.m_tetVtxConstraintDirection[i].second);
			if (i == m_selectedVtx)
				glColor3d(1, 0, 0);
			else
				glColor3dv(Drawer::COLOR_STROKE);
			KDrawer::drawCylinder(pos - bar_length * dir, 2 * bar_length * dir, bar_radius);
		}

/*
		for (int i = 0; i < (int)core.m_strokes.size(); ++i) {
			vector<KVector3d>& stroke = core.m_strokes[i];
			for (int j = 1; j < (int)stroke.size(); ++j) {
				KVector3d& start = stroke[j - 1];
				KVector3d  ori   = stroke[j];
				ori.sub(start);
				double radius = 0.01;
				KDrawer::drawCylinder(start, ori, radius);
				if (j == (int)stroke.size() - 1) KDrawer::drawCone(stroke.back(), ori, 2 * radius, 0.05);
			}
		}
*/
	}
	//if (m_useTexture) glDisable(GL_TEXTURE_3D);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	if (isDrawingConstraintStroke)
		glColor3d(0.2, 0.3, 1);
	else
		glColor3dv(Drawer::COLOR_CUTSTROKE);
	glLineWidth(5);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < (int)core.m_cutStroke.size(); ++i)
		glVertex3dv(core.m_cutStroke[i].getPtr());
	glEnd();
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}

void StateDrawStroke::postDraw(CWnd* hWnd, CDC* pDC) {
	Core& core = *Core::getInstance();
	CFont font;
	CFont *pOldFont;
	font.CreatePointFont ( 300, "Comic Sans MS", pDC );
	pOldFont = pDC->SelectObject ( &font );
	pDC->SetBkMode ( TRANSPARENT );
	pDC->SetTextColor ( RGB ( 255, 0, 0 ) );
	CRect rc;
	hWnd->GetClientRect ( &rc );
	pDC->DrawText ( "Draw strokes!", -1, &rc, DT_LEFT | DT_TOP | DT_SINGLELINE);
	pDC->SelectObject ( pOldFont );
}

void StateDrawStroke::OnLButtonDown(CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	KVector3d start, ori;
	core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
	KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
	KVector3d pos;
	int polyID;
	m_pointOld = point;

	m_selectedVtx = -1;

	if (!core.m_showStreamLines && KUtil::getIntersection(pos, polyID, poly, start, ori) && (nFlags & MK_SHIFT) == 0) {
/*
		core.m_strokes.push_back(vector<KVector3d>());
		core.m_strokes.back().push_back(pos);
*/
		// Find closest boundary vertex
		MinSelector<int> closestVtx;
		for (size_t i = 0; i < core.m_tetra.m_vertices.size(); ++i) {
			if (!core.m_isTetVtxBoundary[i]) continue;
			closestVtx.update((pos - core.m_tetra.m_vertices[i].m_pos).length(), i);
		}

		m_selectedVtx = closestVtx.value;
		core.m_tetVtxConstraintDirection[m_selectedVtx].first = true;

		m_isDrawing = true;
	} else if (core.m_viewMode == Core::VIEWMODE_CUT) {
		start.addWeighted(ori, 0.5);
		core.m_cutStroke.push_back(start);
		m_isCutting = true;
		if (nFlags & MK_SHIFT) {
			isDrawingConstraintStroke = true;
		} else {
			core.initPoly();
			core.initStreamLinesPoly();
		}
	}
	core.m_ogl.RedrawWindow();
}

void StateDrawStroke::OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	m_isDrawing = false;
	if (!m_isCutting) return;
	m_isCutting = false;
	if (core.m_cutStroke.size() == 1) {
		core.m_cutStroke.clear();
	} else if (isDrawingConstraintStroke) {
		// determine closest vertex for each sample point in m_cutStroke
		bool isValid = true;
		std::vector<int> closestVtx(core.m_cutStroke.size());
		std::vector<KVector3d> projectedPos(core.m_cutStroke.size());
		for (size_t i = 0; i < core.m_cutStroke.size(); ++i) {
			int polyID;
			if (KUtil::getIntersection(projectedPos[i], polyID, core.m_poly, core.m_ogl.m_eyePoint, core.m_cutStroke[i] - core.m_ogl.m_eyePoint)) {
				MinSelector<int> closestVtxSelector;
				for (size_t j = 0; j < core.m_tetra.m_vertices.size(); ++j) {
					if (!core.m_isTetVtxBoundary[j]) continue;
					closestVtxSelector.update((projectedPos[i] - core.m_tetra.m_vertices[j].m_pos).length(), j);
				}
				closestVtx[i] = closestVtxSelector.value;
			} else {
				isValid = false;
				break;
			}
		}
		if (isValid) {
			for (size_t i = 0; i < core.m_cutStroke.size() - 1; ++i) {
				Vector3d normal = vector3_cast<Vector3d>(core.m_tetVtxBoundaryNormal[closestVtx[i]]);
				Vector3d v0 = vector3_cast<Vector3d>(projectedPos[i] - core.m_ogl.m_eyePoint);
				Vector3d v1 = vector3_cast<Vector3d>(projectedPos[i + 1] - core.m_ogl.m_eyePoint);
				Vector3d dir_fiber = v0.cross(v1).cross(normal).normalized();
				core.m_tetVtxConstraintDirection[closestVtx[i]] = { true, dir_fiber };
			}
		}
	} else {
		core.calcPoly(core.m_cutStroke);
		core.calcStreamLinesPoly(core.m_cutStroke);
	}
	core.m_cutStroke.clear();
	core.m_ogl.RedrawWindow();
	isDrawingConstraintStroke = false;
}

void StateDrawStroke::OnMouseMove  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isCutting || m_isDrawing) {
		double dx = point.x - m_pointOld.x;
		double dy = point.y - m_pointOld.y;
		double dist = sqrt(dx * dx + dy * dy);
		KVector3d start, ori;
		core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
		if (m_isCutting) {
			if (dist < 20) return;
			m_pointOld = point;
			KVector3d pos(start);
			pos.addWeighted(ori, 0.5);
			core.m_cutStroke.push_back(pos);
			core.m_ogl.RedrawWindow();
		} else {
			// Get intersection between view ray and tangent plane
			// (start + t * ori - pos).dot(normal) = 0
			// -->
			// t = (pos - start).dot(normal) / ori.dot(normal)
			KVector3d pos = core.m_tetra.m_vertices[m_selectedVtx].m_pos;
			KVector3d normal = core.m_tetVtxBoundaryNormal[m_selectedVtx];
			double t = (pos - start).dot(normal) / ori.dot(normal);
			KVector3d dir_fiber = start + t * ori - pos;
			dir_fiber.normalize();
			core.m_tetVtxConstraintDirection[m_selectedVtx].second = vector3_cast<Vector3d>(dir_fiber);
/*
			KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
			KVector3d pos;
			int polyID;
			if (KUtil::getIntersection(pos, polyID, poly, start, ori)) {
				core.m_strokes.back().push_back(pos);
			}
*/
			core.m_ogl.RedrawWindow();
		}
	}
}

void StateDrawStroke::OnKeyDown(CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) {
	Core& core = *Core::getInstance();
	if (nChar == VK_UP || nChar == VK_DOWN) {
		bar_radius *= nChar == VK_UP ? 2.0 : 0.5;
		bar_length *= nChar == VK_UP ? 2.0 : 0.5;
		core.m_ogl.RedrawWindow();
	}
	if (nChar == 'B') {
		showInterpolatedBars = !showInterpolatedBars;
		core.m_ogl.RedrawWindow();
	}
	if (nChar == 'N' || nChar == 'M') {
		if (m_selectedVtx >= 0) {
			Vector3d& fiber_dir = core.m_tetVtxConstraintDirection[m_selectedVtx].second;
			const Vector3d normal = vector3_cast<Vector3d>(core.m_tetVtxBoundaryNormal[m_selectedVtx]);
			double theta = std::acos(std::max<double>(std::min<double>(fiber_dir.dot(normal), 1), -1));
			theta *= 180 / M_PI;
			if (theta <= 10.001 && nChar == 'N') return;
			if (theta >= 169.999 && nChar == 'M') return;
			theta += nChar == 'N' ? -10 : 10;
			const Vector3d fiber_tangent = (fiber_dir - fiber_dir.dot(normal) * normal).normalized();
			theta *= M_PI / 180;
			fiber_dir = std::cos(theta) * normal + std::sin(theta) * fiber_tangent;
			core.m_ogl.RedrawWindow();
		}
	}
	if (nChar == 'P') {
		core.lineType = (core.lineType + 1) % 3;
		cout << "Line Type: " << (
			core.lineType == 0 ? "Normal" :
			core.lineType == 1 ? "Binormal" : "Fiber"
			) << endl;
		core.calcStreamLines();
		core.initStreamLinesPoly();
		core.m_ogl.RedrawWindow();
	}
}

bool StateDrawStroke::load(const string& fname) {
	Core& core = *Core::getInstance();
	string ext = fname.substr(fname.size() - 7, 7);
	if (ext.compare(".stroke")) {
		CString str;
		str.Format("Please drop *.stroke file");
		AfxMessageBox(str);
		return false;
	}
	std::ifstream fin(fname.c_str());
	if (!fin.is_open()) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return false;
	}
	
	size_t nV;
	fin >> nV;
	if (nV != core.m_tetra.m_vertices.size()) {
		AfxMessageBox("Wrong number of vertices");
		return false;
	}
	core.m_tetVtxConstraintDirection.clear();
	core.m_tetVtxConstraintDirection.resize(nV, {false, Vector3d::Zero()});
	while (true) {
		size_t i;
		double x, y, z;
		fin >> i >> x >> y >> z;
		if (fin) {
			core.m_tetVtxConstraintDirection[i] = {true, {x, y, z}};
		} else {
			break;
		}
	}
	return true;
}

void StateDrawStroke::undo() {
	// Delete selected constraint
	Core& core = *Core::getInstance();
	if (m_selectedVtx >= 0)
		core.m_tetVtxConstraintDirection[m_selectedVtx].first = false;
}

bool StateDrawStroke::isUndoable() {
	return true;
}

void StateDrawStroke::save() {
	Core& core = *Core::getInstance();
	CFileDialog dialog(FALSE, ".stroke", NULL, OFN_OVERWRITEPROMPT, "Stroke files(*.stroke)|*.stroke||");
	if (dialog.DoModal() != IDOK) return;
	string fname = dialog.GetPathName();
	std::ofstream fout(fname.c_str());
	if (!fout.is_open()) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return;
	}
	
	const size_t nV = core.m_tetra.m_vertices.size();
	fout << std::setprecision(static_cast<std::streamsize>(std::numeric_limits<double>::digits10) + 1);
	fout << nV << std::endl;
	for (size_t i = 0; i < nV; ++i) {
		if (core.m_tetVtxConstraintDirection[i].first) {
			fout << i << " " << core.m_tetVtxConstraintDirection[i].second.transpose() << std::endl;
		}
	}
}

bool StateDrawStroke::isSavable() {
	Core& core = *Core::getInstance();
	for (const auto& p : core.m_tetVtxConstraintDirection)
		if (p.first)
			return true;
	return false;
}

void StateDrawStroke::update() {
	Core& core = *Core::getInstance();
	core.calcVtxFiber();
	core.calcVolFiber();
	core.calcStreamLines();
	core.initStreamLinesPoly();
	core.calcStreamLinesPolyLayers();
	//core.m_drawer.genTexLIC(Core::VOL_SIZE, core.m_volFiber, 5);
}
