// MyoModelerDoc.h : interface of the CMyoModelerDoc class
//


#pragma once


class CMyoModelerDoc : public CDocument
{
protected: // create from serialization only
	CMyoModelerDoc();
	DECLARE_DYNCREATE(CMyoModelerDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CMyoModelerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileSave();
public:
	afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
};


