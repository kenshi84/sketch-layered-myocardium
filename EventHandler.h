#pragma once

class EventHandler
{
public:
	EventHandler(void);
	~EventHandler(void) {}
public:
	void OnLButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnRButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnRButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnMouseMove  (CView* view, UINT nFlags, CPoint& point);
	void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags);
	void OnDropFiles  (CView* view, HDROP hDropInfo);
protected:
	bool m_isRButtonDown;
};
