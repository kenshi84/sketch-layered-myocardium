#pragma once

#include <string>

class State
{
protected:
	State(void) {}
	~State(void) {}
public:
	virtual State* next() = 0;
	virtual bool isReady() = 0;
	virtual void init() = 0;
	virtual void draw() = 0;
	virtual void postDraw(CWnd* hWnd, CDC* pDC) = 0;
	virtual void OnLButtonDown(CView* view, UINT nFlags, CPoint& point) = 0;
	virtual void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) = 0;
	virtual void OnMouseMove  (CView* view, UINT nFlags, CPoint& point) = 0;
	virtual void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) = 0;
	virtual bool load(const std::string& fname) = 0;
	virtual void undo() = 0;
	virtual bool isUndoable() = 0;
	virtual void save() = 0;
	virtual bool isSavable() = 0;
};
