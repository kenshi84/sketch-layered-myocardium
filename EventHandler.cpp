#include "StdAfx.h"
#include "EventHandler.h"
#include "StateLoadTetra.h"
#include "StateSetDepth.h"

#include "Core.h"
#include <KLIB/KUtil.h>

#include <iostream>
#include <string>
using namespace std;

EventHandler::EventHandler(void) {
}

void EventHandler::OnDropFiles(CView* view, HDROP hDropInfo) {
	Core& core = *Core::getInstance();
	char fnameChar[256];
	if (DragQueryFile(hDropInfo, -1, fnameChar, sizeof(fnameChar)) != 1) return;
	DragQueryFile(hDropInfo, 0, fnameChar, sizeof(fnameChar));
	
	string fname(fnameChar);
	if (!core.m_state->load(fname)) return;
	core.m_ogl.RedrawWindow();
}

void EventHandler::OnLButtonDown(CView* view, UINT nFlags, CPoint& point) {
	Core::getInstance()->m_state->OnLButtonDown(view, nFlags, point);
}

void EventHandler::OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core::getInstance()->m_state->OnLButtonUp(view, nFlags, point);
}

void EventHandler::OnRButtonDown(CView* view, UINT nFlags, CPoint& point) {
	CRect rect;
	view->GetWindowRect(&rect);
	int width = rect.right - rect.left;
	if (point.x < width * 0.9) {
		if (nFlags & MK_SHIFT) {
			Core::getInstance()->m_ogl.ButtonDownForTranslate(point);
		} else {
			Core::getInstance()->m_ogl.ButtonDownForRotate(point);
		}
	}else {
		Core::getInstance()->m_ogl.ButtonDownForZoom(point);
	}
	m_isRButtonDown = true;
}

void EventHandler::OnRButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core::getInstance()->m_ogl.ButtonUp();
	m_isRButtonDown = false;
}

void EventHandler::OnMouseMove  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isRButtonDown) {
		core.m_ogl.MouseMove(point);
		return;
	} else {
		core.m_state->OnMouseMove(view, nFlags, point);
	}
}

void EventHandler::OnKeyDown   (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) {
	Core::getInstance()->m_state->OnKeyDown(view, nChar, nRepCnt, nFlags);
}
