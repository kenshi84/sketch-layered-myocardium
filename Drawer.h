#pragma once

#include <vector>
#include <gl/gl.h>

#include <KLIB/KMath.h>

class Drawer {
public:
	static const double COLOR_EDGE      [3];
	static const double COLOR_FACE      [3];
	static const double COLOR_CUTSTROKE [3];
	static const double COLOR_STROKE    [3];
	static const double COLOR_CONTOUR   [4];
	
	Drawer(void);
	~Drawer(void) {}
	void draw();
	void init();
	void postDraw(CWnd* hWnd, CDC* pDC);
	
	GLuint m_texNameDepth;
	//GLuint m_texNameLIC;
	//void genTexLIC(int volSize, const std::vector<KVector3d>& volVector, int lenLIC = 10);
protected:
	void genTexDepth();
};
