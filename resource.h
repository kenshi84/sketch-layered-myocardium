//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// MyoModeler.rc で使用
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TOOLBAR_COMMON              128
#define IDR_MyoModelerTYPE              129
#define IDR_TOOLBAR_SETDEPTH            132
#define IDR_TOOLBAR_VIEWMODE            134
#define IDR_TOOLBAR_DRAWSTROKE          136
#define ID_FILE_NEXT                    32772
#define ID_DEPTH_EPI                    32775
#define ID_DEPTH_LV                     32776
#define ID_DEPTH_RV                     32777
#define ID_DEPTH_SEPT                   32778
#define ID_VIEW_DEPTH                   32784
#define ID_VIEW_STROKE                  32785
#define ID_VIEWMODE_CUT                 32786
#define ID_VIEWMODE_LAYER               32787
#define ID_TOOLBAR_COMMON               32788
#define ID_TOOLBAR_DEPTH                32789
#define ID_TOOLBAR_STROKE               32790
#define ID_TOOLBAR_VIEWMODE             32791
#define ID_LAYER_IN                     32800
#define ID_LAYER_OUT                    32801
#define ID_TOOLBAR_SETDEPTH             32802
#define ID_LAYER_UPDATE                 32803
#define ID_DEPTH_UPDATE                 32804
#define ID_TEXTURE_UPDATE               32806
#define ID_TEXTURE_UPDATE32807          32807
#define ID_TEXTURE_ON                   32808
#define ID_VIEWMODE_FIBER               32810
#define ID_FIBER_UPDATE                 32811
#define ID_FILE_HOME                    32813
#define ID_VIEWMODE_STREAMLINE          32816
#define ID_FILE_EXPORT                  32817

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32823
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
