#include "StdAfx.h"
#include "Core.h"

#include <KLIB/KMarchingTetra.h>
#include <KLIB/KUtil.h>
#include <KLIB/KUmfpack.h>
#include <KLIB/KUmfpackSparseMatrix.h>

#include "StateLoadTetra.h"

#include <gl/glext.h>
PFNGLTEXIMAGE3DPROC glTexImage3D;

#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>

#include <ctime>
#include <cstdlib>

#include <igl/cotmatrix.h>
#include <igl/massmatrix.h>
#include <igl/invert_diag.h>

using namespace std;

const double Core::GRADIENT_DELTA = 0.005;
const int Core::LAYER_MAX = 9;
int Core::VOL_SIZE = 300;
const double Core::LAMBDA_EUCLID = 1;
const double Core::LAMBDA_DEPTH  = 5;

Core::Core(void) {
	if (__argc >= 2) {
		try {
			int i = std::stoi(__argv[1]);
			if (i > 0)
				VOL_SIZE = i;
		} catch (const std::exception& e) {
			std::cout << "Error while parsing command line argument: " << __argv[1] << ", e.what() = " << e.what() << std::endl;
		}
	}
	std::cout << "VOL_SIZE: " << VOL_SIZE << std::endl;

	m_state = StateLoadTetra::getInstance();
	//m_state->init();
	m_viewMode = VIEWMODE_CUT;
	m_currentLayer = 0;
	srand( static_cast<unsigned int>( time(NULL) ) );
}

void Core::initPoly() {
	m_poly = m_tetra.toPolygonModel();
	m_poly.calcSmoothNormals();
}

void Core::initPolyContour() {
	m_polyContour = m_tetra.toPolygonModel();
	m_polyContour.calcSmoothNormals();
}

void Core::calcPoly(const vector<KVector3d>& cutStroke) {
	KThinPlate2D rbfCut;
	{
		// construct 2D-RBF height field by projecting cutStroke onto the screen
		vector<KVector2d> cutStroke2D;
		for (int i = 0; i < (int)cutStroke.size(); ++i) {
			const KVector3d& pos = cutStroke[i];
			double winx, winy, winz;
			m_ogl.project(pos.x, pos.y, pos.z, winx, winy, winz);
			cutStroke2D.push_back(KVector2d(winx, winy));
		}
		vector<KVector2d> points;
		vector<double   > values;
		for (int i = 1; i < (int)cutStroke2D.size(); ++i) {
			KVector2d& p0 = cutStroke2D[i - 1];
			KVector2d& p1 = cutStroke2D[i];
			KVector2d d(p1);
			d.sub(p0);
			d.normalize();
			d.scale(10);
			KVector2d cp(p0), cm(p0);
			cp.add(-d.y, d.x);
			cm.add(d.y, -d.x);
			points.push_back(cp);
			points.push_back(cm);
			values.push_back( 1);
			values.push_back(-1);
		}
		rbfCut.setPoints(points);
		rbfCut.setValues(values);
	}
	vector<double> vtxValue(m_tetra.m_vertices.size());
	{
		// calc vtxValue of the height field by projecting each tetra vtx
		for (int i = 0; i < (int)m_tetra.m_vertices.size(); ++i) {
			KVector3d& pos = m_tetra.m_vertices[i].m_pos;
			double winx, winy, winz;
			m_ogl.project(pos.x, pos.y, pos.z, winx, winy, winz);
			double val = rbfCut.getValue(winx, winy);
			vtxValue[i] = val;
		}
	}
	m_poly = KMarchingTetra::calcMarchingTetra(m_tetra, vtxValue, 0);
	m_poly.calcSmoothNormals();
}

void Core::calcVolTetID() {
	const int& N = VOL_SIZE;
	m_volTetID.clear();
	m_volTetID.resize(N * N * N, -1);
	int count = 0;
	for (int i = 0; i < (int)m_tetra.m_tetras.size(); ++i) {
		KTetra& tet = m_tetra.m_tetras[i];
		double zmin =  DBL_MAX;
		double zmax = -DBL_MAX;
		for (int j = 0; j < 4; ++j) {
			KVector3d& pos = m_tetra.m_vertices[tet.m_vtx[j]].m_pos;
			zmin = min(pos.z, zmin);
			zmax = max(pos.z, zmax);
		}
		int izmin = d2i_ceil (zmin);
		int izmax = d2i_floor(zmax);
		for (int iz = izmin; iz <= izmax; ++iz) {
			VERIFY(iz >= 0 && iz < N);
			double dz = i2d(iz);
			vector<KVector3d> intersectZ;
			intersectZ.reserve(4);
			for (int j = 0; j < 4; ++j) {
				KVector3d& pos0 = m_tetra.m_vertices[tet.m_vtx[j]].m_pos;
				double uz0 = pos0.z - dz;
				for (int k = j + 1; k < 4; ++k) {
					KVector3d& pos1 = m_tetra.m_vertices[tet.m_vtx[k]].m_pos;
					double uz1 = pos1.z - dz;
					if (uz0 * uz1 <= 0) {
						KVector3d pos;
						pos.addWeighted(pos0, abs(uz1 / (uz1 - uz0)));
						pos.addWeighted(pos1, abs(uz0 / (uz1 - uz0)));
						intersectZ.push_back(pos);
					}
				}
			}
			VERIFY(intersectZ.empty() || intersectZ.size() == 3 || intersectZ.size() == 4);
			double ymin =  DBL_MAX;
			double ymax = -DBL_MAX;
			for (int j = 0; j < (int)intersectZ.size(); ++j) {
				KVector3d& pos = intersectZ[j];
				ymin = min(pos.y, ymin);
				ymax = max(pos.y, ymax);
			}
			int iymin = d2i_ceil (ymin);
			int iymax = d2i_floor(ymax);
			for (int iy = iymin; iy <= iymax; ++iy) {
				VERIFY(iy >= 0 && iy < N);
				double dy = i2d(iy);
				vector<KVector3d> intersectY;
				intersectY.reserve(4);
				for (int j = 0; j < (int)intersectZ.size(); ++j) {
					KVector3d& pos0 = intersectZ[j];
					double uy0 = pos0.y - dy;
					for (int k = j + 1; k < (int)intersectZ.size(); ++k) {
						KVector3d& pos1 = intersectZ[k];
						double uy1 = pos1.y - dy;
						if (uy0 * uy1 <= 0) {
							KVector3d pos;
							pos.addWeighted(pos0, abs(uy1 / (uy1 - uy0)));
							pos.addWeighted(pos1, abs(uy0 / (uy1 - uy0)));
							intersectY.push_back(pos);
						}
					}
				}
				double xmin =  DBL_MAX;
				double xmax = -DBL_MAX;
				for (int j = 0; j < (int)intersectY.size(); ++j) {
					KVector3d& pos = intersectY[j];
					xmin = min(pos.x, xmin);
					xmax = max(pos.x, xmax);
				}
				if (xmin > xmax) continue;
				int ixmin = d2i_ceil (xmin);
				int ixmax = d2i_floor(xmax);
				for (int ix = ixmin; ix <= ixmax; ++ix) {
					VERIFY(ix >= 0 && ix < N);
					if (m_volTetID[getIndex(ix, iy, iz)] == -1)
						++count;
					m_volTetID[getIndex(ix, iy, iz)] = i;
				}
			}
		}
	}
	std::cout << "Number of filled voxels: " << count << std::endl;

	// Export to Qubicle format
	std::ofstream fout("voxels.qb", std::ios::binary);

	uint8_t version[4] = { 1, 1, 0, 0 };
	fout.write((char*)version, 4);

	uint32_t colorFormat = 0;
	fout.write((char*)&colorFormat, 4);

	uint32_t zAxisOrientation = 0;
	fout.write((char*)&zAxisOrientation, 4);

	uint32_t compressed = 0;
	fout.write((char*)&compressed, 4);

	uint32_t visibilityMaskEncoded = 0;
	fout.write((char*)&visibilityMaskEncoded, 4);

	uint32_t numMatrices = 1;
	fout.write((char*)&numMatrices, 4);

	uint8_t nameLength = 6;
	fout.write((char*)&nameLength, 1);

	string name = "Matrix";
	fout.write(name.data(), 6);

	uint32_t s = VOL_SIZE;
	fout.write((char*)&s, 4);
	fout.write((char*)&s, 4);
	fout.write((char*)&s, 4);

	int32_t offset = 0;
	fout.write((char*)&offset, 4);
	fout.write((char*)&offset, 4);
	fout.write((char*)&offset, 4);

	for (int iz = 0; iz < s; ++iz)
	for (int iy = 0; iy < s; ++iy)
	for (int ix = 0; ix < s; ++ix)
	{
		uint32_t value = m_volTetID[getIndex(ix, iy, iz)] != -1 ? 0xFFBFBFBF : 0;
		fout.write((char*)&value, 4);
	}
}

void Core::calcTetVtxBoundary() {
	int numVtx = (int)m_tetra.m_vertices.size();
	m_isTetVtxBoundary    .clear();
	m_tetVtxBoundaryNormal.clear();
	m_isTetVtxBoundary    .resize(numVtx, false);
	m_tetVtxBoundaryNormal.resize(numVtx, KVector3d());
	for (int i = 0; i < (int)m_tetra.m_tetras.size(); ++i) {
		KTetra& tet = m_tetra.m_tetras[i];
		for (int j = 0; j < 4; ++j) {
			if (tet.m_neighbor[j] != -1) continue;
			KVector3d& n = KUtil::calcNormal(
				m_tetra.m_vertices[tet.m_vtx[KTetraModel::FACE_VTX_ORDER[j][0]]].m_pos,
				m_tetra.m_vertices[tet.m_vtx[KTetraModel::FACE_VTX_ORDER[j][1]]].m_pos,
				m_tetra.m_vertices[tet.m_vtx[KTetraModel::FACE_VTX_ORDER[j][2]]].m_pos);
			for (int k = 0; k < 3; ++k) {
				int vtxID = tet.m_vtx[KTetraModel::FACE_VTX_ORDER[j][k]];
				m_isTetVtxBoundary[vtxID] = true;
				m_tetVtxBoundaryNormal[vtxID].add(n);
			}
		}
	}
	for (int i = 0; i < numVtx; ++i) {
		if (!m_isTetVtxBoundary[i]) continue;
		m_tetVtxBoundaryNormal[i].normalize();
	}
}

void Core::calcRbfDepth() {
	m_rbfDepth.setPoints(m_rbfPoints);
	m_rbfDepth.setValues(m_rbfValues);
}

void Core::calcVtxDepth() {
	int vtxSize = (int)m_tetra.m_vertices.size();
	m_vtxDepth.resize(vtxSize);
	for (int i = 0; i < vtxSize; ++i) {
		KVector3d& pos = m_tetra.m_vertices[i].m_pos;
		m_vtxDepth[i] = m_rbfDepth.getValue(pos);
	}
}

void Core::calcPolyLayers() {
	int margin0 = 0;
	int margin1 = 1;
	m_polyLayers.clear();
	m_polyLayers.reserve(LAYER_MAX + 1);
	for (int i = 0; i <= LAYER_MAX; ++i) {
		double depth = -2 * (i + margin0) / (double)(LAYER_MAX + margin0 + margin1);
		KPolygonModel& poly = KMarchingTetra::calcMarchingTetra(m_tetra, m_vtxDepth, depth);
		poly.calcSmoothNormals();
		m_polyLayers.push_back(poly);
	}
}

void Core::calcPolyVtxDepth() {
	m_polyVtxDepth.resize(m_poly.m_vertices.size());
	for (int i = 0; i < (int)m_poly.m_vertices.size(); ++i) {
		m_polyVtxDepth[i] = m_rbfDepth.getValue(m_poly.m_vertices[i].m_pos);
	}
}

void Core::calcPolyLayersVtxDepth() {
	m_polyLayersVtxDepth.resize(LAYER_MAX + 1, vector<double>());
	for (int i = 0; i <= LAYER_MAX; ++i) {
		vector<double>& vtxDepth = m_polyLayersVtxDepth[i];
		KPolygonModel& poly = m_polyLayers[i];
		vtxDepth.resize(poly.m_vertices.size());
		for (int j = 0; j < (int)poly.m_vertices.size(); ++j)
			vtxDepth[j] = m_rbfDepth.getValue(poly.m_vertices[j].m_pos);
	}
}

void Core::calcVolDepth() {
	const int& N = VOL_SIZE;
	m_volDepth.clear();
	m_volDepth.resize(N * N * N, -DBL_MAX);
	for (int iz = 0; iz < N; ++iz) {
		for (int iy = 0; iy < N; ++iy) {
			for (int ix = 0; ix < N; ++ix) {
				int index = getIndex(ix, iy, iz);
				int tetID = m_volTetID[index];
				if (tetID == -1) continue;
				KVector3d pos(i2d(ix), i2d(iy), i2d(iz));
				KTetra& tet = m_tetra.m_tetras[tetID];
				double w[4];
				KVector3d vtx[4];
				for (int i = 0; i < 4; ++i)
					vtx[i] = m_tetra.m_vertices[tet.m_vtx[i]].m_pos;
				KUtil::getBarycentricCoord(w[0], w[1], w[2], w[3], vtx[0], vtx[1], vtx[2], vtx[3], pos, 1);
				double depth = 0;
				for (int i = 0; i < 4; ++i)
					depth += w[i] * m_vtxDepth[tet.m_vtx[i]];
				//double depth = m_rbfDepth.getValue(dx, dy, dz);
				m_volDepth[index] = depth;
			}
		}
	}
}

void Core::calcLaplacian() {
	MatrixXd V(m_tetra.m_vertices.size(), 3);
	MatrixXi F(m_tetra.m_tetras.size(), 4);
	for (int i = 0; i < V.rows(); ++i) {
		V.row(i) << vector3_cast<Vector3d>(m_tetra.m_vertices[i].m_pos).transpose();
	}
	for (int i = 0; i < F.rows(); ++i) {
		for (int j = 0; j < 4; ++j)
			F(i, j) = m_tetra.m_tetras[i].m_vtx[j];
	}
	igl::cotmatrix(V, F, m_cotmatrix);

	// Bi-Laplacian
	SparseMatrixd M;
	igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_DEFAULT, M);
	SparseMatrixd Mi;
  igl::invert_diag(M, Mi);
	m_bilaplacian = m_cotmatrix * Mi * m_cotmatrix;
}

void Core::calcVtxFiber() {
	const int nV = m_cotmatrix.rows();

	// Step 1: smoothing without considering surface normal constraints

	VectorXi known(nV);
	MatrixXd known_values(nV, 5);
	int count = 0;
	for (int i = 0; i < nV; ++i) {
		if (!m_isTetVtxBoundary[i]) continue;
		if (!m_tetVtxConstraintDirection[i].first) continue;

		Vector3d dir_fiber = m_tetVtxConstraintDirection[i].second;

		// Ensure orientation constraint is properly set, otherwise mark it as unconstrained
		if (dir_fiber == Vector3d::Zero()) {
			m_tetVtxConstraintDirection[i].first = false;
			continue;
		}

		VERIFY(std::abs(dir_fiber.norm() - 1) < 1.e-8);

		Vector3d dir_normal = vector3_cast<Vector3d>(m_tetVtxBoundaryNormal[i]);
		dir_normal = (dir_normal - dir_fiber * dir_fiber.dot(dir_normal)).normalized();

		Vector3d dir_binormal = dir_fiber.cross(dir_normal);

		const double lambda0 = 1.0;
		const double lambda1 = 1.0 / 3.0;		// Should be in range [0, 0.5)
		const double lambda2 = 1.0 - lambda1;

		Matrix3d tensor_mat = lambda0 * dir_normal * dir_normal.transpose() + // major corresponds to normal
			-1.0 * lambda1 * dir_binormal * dir_binormal.transpose() +          // medium corresponds to binormal
			-1.0 * lambda2 * dir_fiber * dir_fiber.transpose();                 // minor corresponds to fiber

		Vector5d tensor_vec = tensor_to_vector(tensor_mat);

		known[count] = i;
		known_values.row(count) = tensor_vec.transpose();
		++count;
	}
	known.conservativeResize(count);
	known_values.conservativeResize(count, 5);

	igl::min_quad_with_fixed_precompute(SparseMatrixd{m_bilaplacian}, known, SparseMatrixd{}, true, m_solver_data);
	igl::min_quad_with_fixed_solve(m_solver_data, MatrixXd::Zero(nV, 5), known_values, MatrixXd{}, m_vtxFiber);

#if 0
	// Step 2: fix boundary vertices after removing normal component

	known = VectorXi(nV);
	known_values = MatrixXd(nV, 5);
	count = 0;
	for (int i = 0; i < nV; ++i) {
		if (!m_isTetVtxBoundary[i]) continue;

		Matrix3d tensor_mat = vector_to_tensor(m_vtxFiber.row(i).transpose());

		Vector3d dir_fiber = get_largest_eigenvector(tensor_mat);

		// Remove normal component
		Vector3d normal = vector3_cast<Vector3d>(m_tetVtxBoundaryNormal[i]);
		dir_fiber = (dir_fiber - (dir_fiber.dot(normal)) * normal).normalized();

		tensor_mat = dir_fiber * dir_fiber.transpose();
		tensor_mat -= (tensor_mat.trace() / 3.0) * Matrix3d::Identity();
		Vector5d tensor_vec = tensor_to_vector(tensor_mat);

		known[count] = i;
		known_values.row(count) = tensor_vec.transpose();
		++count;
	}
	known.conservativeResize(count);
	known_values.conservativeResize(count, 5);

	igl::min_quad_with_fixed_precompute(SparseMatrixd{ m_bilaplacian }, known, SparseMatrixd{}, true, m_solver_data);
	igl::min_quad_with_fixed_solve(m_solver_data, MatrixXd::Zero(nV, 5), known_values, MatrixXd{}, m_vtxFiber);
#endif
}

void Core::calcVolFiber() {
	VERIFY(m_vtxFiber.rows() == m_tetra.m_vertices.size());
	VERIFY(m_vtxFiber.cols() == 5);

	const int& N = VOL_SIZE;
	m_volFiber.clear();
	m_volFiber.resize(N * N * N);
#pragma omp parallel for
	for (int iz = 0; iz < N; ++iz) {
		for (int iy = 0; iy < N; ++iy) {
			for (int ix = 0; ix < N; ++ix) {
				int index = getIndex(ix, iy, iz);
				int tetID = m_volTetID[index];
				if (tetID == -1) {
					continue;
				}
				KVector3d pos(i2d(ix), i2d(iy), i2d(iz));
				KTetra& tet = m_tetra.m_tetras[tetID];
				double weights[4];
				// simple linear interpolation
				KVector3d vtx[4];
				for (int i = 0; i < 4; ++i)
					vtx[i] = m_tetra.m_vertices[tet.m_vtx[i]].m_pos;
				KUtil::getBarycentricCoord(weights[0], weights[1], weights[2], weights[3], vtx[0], vtx[1], vtx[2], vtx[3], pos);
				
				Vector5d fiber = Vector5d::Zero();
				for (int i = 0; i < 4; ++i) {
					fiber += weights[i] * m_vtxFiber.row(tet.m_vtx[i]);
				}

				Matrix3d t = vector_to_tensor(fiber);

				m_volFiber[index] = get_eigenvectors(t);
			}
		}
	}
}

void Core::calcStreamLines() {
	const int& N = VOL_SIZE;
	m_streamLines.clear();
	const double delta = 0.0025;
	const size_t numStreamLines = 20000;
	m_streamLines.reserve(numStreamLines);
	while (m_streamLines.size() < numStreamLines) {
		KVector3d pos0(rand() / (double)RAND_MAX, rand() / (double)RAND_MAX, rand() / (double)RAND_MAX);
		int index0 = getIndex(d2i(pos0.x), d2i(pos0.y), d2i(pos0.z));
		VERIFY(index0 >= 0);
		if (m_volTetID[index0] == -1) continue;
		KVector3d pos, vec;
		// go forward
		vector<pair<KVector3d, double> > forward;
		pos.set(pos0);
		vec.set(vector3_cast<KVector3d>(m_volFiber[index0].col(lineType)));
		while (true) {
			pos.addWeighted(vec, delta);
			int index = getIndex(d2i(pos.x), d2i(pos.y), d2i(pos.z));
			if (index < 0) break;
			if (m_volTetID[index] < 0) break;
			forward.push_back(pair<KVector3d, double>(pos, 0));
			KVector3d vecNew = vector3_cast<KVector3d>(m_volFiber[index].col(lineType));
			if (vec.dot(vecNew) < 0)
				vecNew.scale(-1);
			vec.set(vecNew);
		}
		// go backward
		vector<pair<KVector3d, double> > backward;
		pos.set(pos0);
		vec.set(vector3_cast<KVector3d>(m_volFiber[index0].col(lineType)));
		while (true) {
			pos.addWeighted(vec, -delta);
			int index = getIndex(d2i(pos.x), d2i(pos.y), d2i(pos.z));
			if (index < 0) break;
			if (m_volTetID[index] < 0) break;
			backward.push_back(pair<KVector3d, double>(pos, 0));
			KVector3d vecNew = vector3_cast<KVector3d>(m_volFiber[index].col(lineType));
			if (vec.dot(vecNew) < 0)
				vecNew.scale(-1);
			vec.set(vecNew);
		}
		int size = (int)(forward.size() + backward.size() + 1);
		//if (size < sizeMin) continue;
		vector<pair<KVector3d, double> > streamLine;
		streamLine.reserve(size);
		for (int i = (int)backward.size() - 1; i >= 0; --i)
			streamLine.push_back(backward[i]);
			//streamLine.push_back(pair<KVector3d, double>(backward[i], m_rbfDepth.getValue(backward[i])));
		//streamLine.push_back(pair<KVector3d, double>(pos0, m_rbfDepth.getValue(pos0)));
		streamLine.push_back(pair<KVector3d, double>(pos0, 0));
		for (int i = 0; i < (int)forward.size(); ++i)
			streamLine.push_back(forward[i]);
		m_streamLines.push_back(streamLine);
	}
}

void Core::initStreamLinesPoly() {
	m_streamLinesPoly = m_streamLines;
}

void Core::calcStreamLinesPoly(const vector<KVector3d>& cutStroke) {
	KThinPlate2D rbfCut;
	{
		// construct 2D-RBF height field by projecting cutStroke onto the screen
		vector<KVector2d> cutStroke2D;
		for (int i = 0; i < (int)cutStroke.size(); ++i) {
			const KVector3d& pos = cutStroke[i];
			double winx, winy, winz;
			m_ogl.project(pos.x, pos.y, pos.z, winx, winy, winz);
			cutStroke2D.push_back(KVector2d(winx, winy));
		}
		vector<KVector2d> points;
		vector<double   > values;
		for (int i = 1; i < (int)cutStroke2D.size(); ++i) {
			KVector2d& p0 = cutStroke2D[i - 1];
			KVector2d& p1 = cutStroke2D[i];
			KVector2d d(p1);
			d.sub(p0);
			d.normalize();
			d.scale(10);
			KVector2d cp(p0), cm(p0);
			cp.add(-d.y, d.x);
			cm.add(d.y, -d.x);
			points.push_back(cp);
			points.push_back(cm);
			values.push_back( 1);
			values.push_back(-1);
		}
		rbfCut.setPoints(points);
		rbfCut.setValues(values);
	}
	m_streamLinesPoly.clear();
	for (int i = 0; i < (int)m_streamLines.size(); ++i) {
		m_streamLinesPoly.push_back(vector<pair<KVector3d, double> >());
		vector<pair<KVector3d, double> >& streamLine = m_streamLines[i];
		for (int j = 0; j < (int)streamLine.size(); ++j) {
			KVector3d& pos = streamLine[j].first;
			double winx, winy, winz;
			m_ogl.project(pos.x, pos.y, pos.z, winx, winy, winz);
			double val = rbfCut.getValue(winx, winy);
			if (val > 0) {
				if (!m_streamLinesPoly.back().empty()) m_streamLinesPoly.push_back(vector<pair<KVector3d, double> >());
				continue;
			}
			m_streamLinesPoly.back().push_back(streamLine[j]);
		}
	}
	if (!m_streamLinesPoly.empty() && m_streamLinesPoly.back().empty()) m_streamLinesPoly.pop_back();
}

void Core::calcStreamLinesPolyLayers() {
	int margin0 = 0;
	int margin1 = 1;
	m_streamLinesPolyLayers.clear();
	m_streamLinesPolyLayers.reserve(LAYER_MAX + 1);
	for (int i = 0; i <= LAYER_MAX; ++i) {
		double layerDepth = -2 * (i + margin0) / (double)(LAYER_MAX + margin0 + margin1);
		vector<vector<pair<KVector3d, double> > > streamLines;
		for (int j = 0; j < (int)m_streamLines.size(); ++j) {
			streamLines.push_back(vector<pair<KVector3d, double> >());
			vector<pair<KVector3d, double> >& streamLine = m_streamLines[j];
			for (int k = 0; k < (int)streamLine.size(); ++k) {
				double depth = streamLine[k].second;
				if (depth > layerDepth) {
					if (!streamLines.back().empty()) streamLines.push_back(vector<pair<KVector3d, double> >());
					continue;
				}
				streamLines.back().push_back(streamLine[k]);
			}
		}
		if (streamLines.back().empty()) streamLines.pop_back();
		m_streamLinesPolyLayers.push_back(streamLines);
	}
}
void Core::calcDeformVtxList(const KVector3d& origin, double radius) {
	m_deformVtxList.clear();
	m_deformVtxWeight.clear();
	KVector3d eye(m_ogl.m_focusPoint);
	eye.sub(m_ogl.m_eyePoint);
	eye.normalize();
	for (int i = 0; i < (int)m_tetra.m_vertices.size(); ++i) {
		if (!m_isTetVtxBoundary[i]) continue;
		KVector3d& pos = m_tetra.m_vertices[i].m_pos;
		double dist = KUtil::getDistance(origin, pos);
		if (dist > radius) continue;
		KVector3d& n = m_tetVtxBoundaryNormal[i];
		if (eye.dot(n) > 0) continue;
		m_deformVtxList.push_back(i);
		m_deformVtxWeight.push_back(0.5 + 0.5 * cos(M_PI * dist / radius));
	}
}

void Core::calcDeformVtxOffset(const KVector3d& offset) {
	m_deformVtxOffset.clear();
	for (int i = 0; i < (int)m_deformVtxList.size(); ++i) {
		double weight = m_deformVtxWeight[i];
		KVector3d v(offset);
		v.scale(weight);
		m_deformVtxOffset.push_back(v);
	}
}

void Core::deformTetra(const vector<KVector3d>& vtxPosNew) {
	KTetraModel tetraOld = m_tetra;
	//vector<KVector3d> vtxPosOld;
	//vtxPosOld.reserve(m_tetra.m_vertices.size());
	//for (int i = 0; i < (int)m_tetra.m_vertices.size(); ++i)
	//	vtxPosOld.push_back(m_tetra.m_vertices[i].m_pos);
	//vector<KVector3d> vtxPosNew(vtxPosOld);
	//for (int i = 0; i < (int)m_deformVtxList.size(); ++i)
	//	vtxPosNew[m_deformVtxList[i]].add(m_deformVtxOffset[i]);
	
	// transform tetra vertices
	for (int i = 0; i < (int)vtxPosNew.size(); ++i)
		m_tetra.m_vertices[i].m_pos.set(vtxPosNew[i]);
	// update geometry
	initPoly();
	
	//// transform depth-constraint points
	//vector<KVector3d> rbfPointsNew;
	//rbfPointsNew.reserve(m_rbfPoints.size());
	//for (int i = 0; i < (int)m_rbfPoints.size(); ++i)
	//	rbfPointsNew.push_back(transform(vtxPosOld, vtxPosNew, m_rbfPoints[i]));
	//
	//m_rbfPoints = rbfPointsNew;
	// update depth
	//calcRbfDepth();
	//calcVtxDepth();
	calcPolyLayers();
	
	// transform strokes
	vector<vector<KVector3d> > strokesNew;
	strokesNew.reserve(m_strokes.size());
	for (int i = 0; i < (int)m_strokes.size(); ++i) {
		vector<KVector3d> strokeNew;
		strokeNew.reserve(m_strokes[i].size());
		for (int j = 0; j < (int)m_strokes[i].size(); ++j) {
			strokeNew.push_back(transform(tetraOld, m_tetra, m_strokes[i][j]));
		}
		strokesNew.push_back(strokeNew);
	}
	m_strokes = strokesNew;
	vector<KVector3d> s1PointsNew;
	s1PointsNew.reserve(m_s1Points.size());
	for (int i = 0; i < (int)m_s1Points.size(); ++i)
		s1PointsNew.push_back(transform(tetraOld, m_tetra, m_s1Points[i]));
	m_s1Points = s1PointsNew;
}

KVector3d Core::transform(const KTetraModel& tetraOld, const KTetraModel& tetraNew, const KVector3d& pos) {
	//int ix = d2i(pos.x);
	//int iy = d2i(pos.y);
	//int iz = d2i(pos.z);
	//int tetID = m_volTetID[getIndex(ix, iy, iz)];
	//if (tetID < 0) return pos;
	int tetID;
	double dist;
	KUtil::getNearestTetraID(tetID, dist, tetraOld, pos);
	double w[4];
	KVector3d v[4];
	for (int i = 0; i < 4; ++i)
		//v[i] = vtxPosOld[m_tetra.m_tetras[tetID].m_vtx[i]];
		v[i] = tetraOld.m_vertices[tetraOld.m_tetras[tetID].m_vtx[i]].m_pos;
	KUtil::getBarycentricCoord(w[0], w[1], w[2], w[3], v[0], v[1], v[2], v[3], pos, 1);
	KVector3d posNew;
	for (int i = 0; i < 4; ++i)
		posNew.addWeighted(tetraNew.m_vertices[tetraNew.m_tetras[tetID].m_vtx[i]].m_pos, w[i]);
	return posNew;
}

void Core::exportVolData() {
	CFileDialog dialog(FALSE, ".txt", NULL, OFN_OVERWRITEPROMPT, "Text files(*.txt)|*.txt||");
	if (dialog.DoModal() != IDOK) return;
	string fname = dialog.GetPathName();
  std::ofstream fout(fname.c_str());
	if (!fout) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return;
	}

  const int& N = VOL_SIZE;

  for (int iz = 0; iz < N; ++iz)
  for (int iy = 0; iy < N; ++iy)
  for (int ix = 0; ix < N; ++ix)
  {
  	int index = getIndex(ix, iy, iz);

    if (m_volTetID[index] == -1)
    	continue;

    fout << ix << " " << iy << " " << iz << " ";

    fout << m_volFiber[index].col(2).transpose() << " ";
    fout << m_volFiber[index].col(1).transpose() << " ";
    fout << m_volFiber[index].col(0).transpose() << std::endl;
  }

}


void Core::initEyePosition() {
	m_ogl.m_eyePoint.set(0.5, 0.5, -2);
	m_ogl.m_focusPoint.set(0.5, 0.5, 0.5);
	m_ogl.m_upDirection.set(0, 1, 0);
	m_ogl.updateEyePosition();
}

