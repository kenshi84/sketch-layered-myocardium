// MyoModelerDoc.cpp : implementation of the CMyoModelerDoc class
//

#include "stdafx.h"
#include "MyoModeler.h"

#include "MyoModelerDoc.h"

#include "Core.h"
#include "StateSetDepth.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMyoModelerDoc

IMPLEMENT_DYNCREATE(CMyoModelerDoc, CDocument)

BEGIN_MESSAGE_MAP(CMyoModelerDoc, CDocument)
	ON_COMMAND(ID_FILE_SAVE, &CMyoModelerDoc::OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, &CMyoModelerDoc::OnUpdateFileSave)
END_MESSAGE_MAP()


// CMyoModelerDoc construction/destruction

CMyoModelerDoc::CMyoModelerDoc()
{
	// TODO: add one-time construction code here

}

CMyoModelerDoc::~CMyoModelerDoc()
{
}

BOOL CMyoModelerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMyoModelerDoc serialization

void CMyoModelerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CMyoModelerDoc diagnostics

#ifdef _DEBUG
void CMyoModelerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMyoModelerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMyoModelerDoc commands

void CMyoModelerDoc::OnFileSave() {
	Core::getInstance()->m_state->save();
}

void CMyoModelerDoc::OnUpdateFileSave(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state->isSavable());
}
