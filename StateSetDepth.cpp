#include "StdAfx.h"
#include "StateSetDepth.h"
#include "Drawer.h"
#include "MainFrm.h"
#include <KLIB/KUtil.h>
#include <KLIB/KDrawer.h>
#include "StateDrawStroke.h"

using namespace std;

const double StateSetDepth::RBFVALUE_EPI  = 0;
const double StateSetDepth::RBFVALUE_SEPT = -0.5;
const double StateSetDepth::RBFVALUE_RV   = -1;
const double StateSetDepth::RBFVALUE_LV   = -2;

const double StateSetDepth::COLOR_EPI [3] = {1, 0, 0};
const double StateSetDepth::COLOR_SEPT[3] = {1, 1, 0};
const double StateSetDepth::COLOR_RV  [3] = {0, 1, 0};
const double StateSetDepth::COLOR_LV  [3] = {0, 0, 1};

State* StateSetDepth::next() {
	Core& core = *Core::getInstance();
	core.calcLaplacian();
	core.calcVolDepth();
	return StateDrawStroke::getInstance();
}

void StateSetDepth::init() {
	Core& core = *Core::getInstance();
	CMainFrame& f = *core.p_mainFrm;
	f.ShowControlBar(&f.m_wndToolBar_setdepth, true, true);
	f.ShowControlBar(&f.m_wndToolBar_viewmode, true, true);
	m_rbfPointsEpi .clear();
	m_rbfPointsSept.clear();
	m_rbfPointsRv  .clear();
	m_rbfPointsLv  .clear();
	core.m_vtxDepth.clear();
	core.m_polyVtxDepth.clear();
	core.m_polyLayersVtxDepth.clear();
	m_isDrawFace = true;
	m_isDrawPoint = true;
}

void StateSetDepth::draw() {
	Core& core = *Core::getInstance();
	KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
	vector<double>& vtxDepth = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_polyVtxDepth : core.m_polyLayersVtxDepth[core.m_currentLayer];
	if (!vtxDepth.empty()) {
		glEnable(GL_TEXTURE_1D);
		glBindTexture(GL_TEXTURE_1D, core.m_drawer.m_texNameDepth);
	}
	if (m_isDrawFace) {
		glColor3dv(Drawer::COLOR_FACE);
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
			KPolygon& p = poly.m_polygons[i];
			for (int j = 0; j < 3; ++j) {
				KVector3d& n = p.m_normal[j];
				KVector3d& v = poly.m_vertices[p.m_vtx[j]].m_pos;
				glNormal3dv(n.getPtr());
				if (!vtxDepth.empty()) glTexCoord1d(-0.5 * vtxDepth[p.m_vtx[j]]);
				glVertex3dv(v.getPtr());
			}
		}
		glEnd();
	}
	if (!vtxDepth.empty())
		glDisable(GL_TEXTURE_1D);
	if (m_isDrawPoint) {
		const double* color =
			m_drawMode == DRAWMODE_EPI  ? COLOR_EPI  :
			m_drawMode == DRAWMODE_SEPT ? COLOR_SEPT :
			m_drawMode == DRAWMODE_RV   ? COLOR_RV   : COLOR_LV;
		vector<KVector3d>& points =
			m_drawMode == DRAWMODE_EPI  ? m_rbfPointsEpi  :
			m_drawMode == DRAWMODE_SEPT ? m_rbfPointsSept :
			m_drawMode == DRAWMODE_RV   ? m_rbfPointsRv   : m_rbfPointsLv;
		vector<bool>& visible =
			m_drawMode == DRAWMODE_EPI  ? m_isVisibleEpi  :
			m_drawMode == DRAWMODE_SEPT ? m_isVisibleSept :
			m_drawMode == DRAWMODE_RV   ? m_isVisibleRv   : m_isVisibleLv;
		glColor3dv(color);
		for (int j = 0; j < (int)points.size(); ++j) {
			if (core.m_viewMode == Core::VIEWMODE_CUT && !visible[j]) continue;
			KDrawer::drawSphere(points[j], 0.02);
		}
	}
	if (!m_isDrawFace) {
		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glDisable(GL_LIGHTING);
		glColor4dv(Drawer::COLOR_CONTOUR);
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < (int)core.m_polyContour.m_polygons.size(); ++i) {
			KPolygon& p = core.m_polyContour.m_polygons[i];
			for (int j = 0; j < 3; ++j)
				glVertex3dv(core.m_polyContour.m_vertices[p.m_vtx[j]].m_pos.getPtr());
		}
		glEnd();
		glDisable(GL_BLEND);
	}
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	glColor3dv(Drawer::COLOR_CUTSTROKE);
	glLineWidth(5);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < (int)core.m_cutStroke.size(); ++i)
		glVertex3dv(core.m_cutStroke[i].getPtr());
	glEnd();
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}

void StateSetDepth::postDraw(CWnd* hWnd, CDC* pDC) {
	Core& core = *Core::getInstance();
	CFont font;
	CFont *pOldFont;
	font.CreatePointFont ( 300, "Comic Sans MS", pDC );
	pOldFont = pDC->SelectObject ( &font );
	pDC->SetBkMode ( TRANSPARENT );
	pDC->SetTextColor ( RGB ( 255, 0, 0 ) );
	CRect rc;
	hWnd->GetClientRect ( &rc );
	pDC->DrawText ( "Set depth field!", -1, &rc, DT_LEFT | DT_TOP | DT_SINGLELINE);
	pDC->SelectObject ( pOldFont );
}

void StateSetDepth::OnLButtonDown(CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	KVector3d start, ori;
	core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
	KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
	KVector3d pos;
	int polyID;
	m_pointOld = point;
	if (KUtil::getIntersection(pos, polyID, poly, start, ori)) {
		vector<KVector3d>& points =
			m_drawMode == DRAWMODE_EPI  ? m_rbfPointsEpi  :
			m_drawMode == DRAWMODE_SEPT ? m_rbfPointsSept :
			m_drawMode == DRAWMODE_RV   ? m_rbfPointsRv   : m_rbfPointsLv;
		vector<bool>& visible =
			m_drawMode == DRAWMODE_EPI  ? m_isVisibleEpi  :
			m_drawMode == DRAWMODE_SEPT ? m_isVisibleSept :
			m_drawMode == DRAWMODE_RV   ? m_isVisibleRv   : m_isVisibleLv;
		points.push_back(pos);
		visible.push_back(true);
		m_isPutting = true;
	} else if (core.m_viewMode == Core::VIEWMODE_CUT) {
		start.addWeighted(ori, 0.5);
		core.m_cutStroke.push_back(start);
		m_isCutting = true;
		core.initPoly();
		if (core.m_rbfDepth.isReady()) core.calcPolyVtxDepth();
		initVisible();
	}
	core.m_ogl.RedrawWindow();
}

void StateSetDepth::OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	m_isPutting = false;
	if (!m_isCutting) return;
	m_isCutting = false;
	if (core.m_cutStroke.size() == 1) {
		core.m_cutStroke.clear();
		return;
	} else {
		core.calcPoly(core.m_cutStroke);
		if (core.m_rbfDepth.isReady()) core.calcPolyVtxDepth();
		calcVisible(core.m_cutStroke);
	}
	core.m_cutStroke.clear();
	core.m_ogl.RedrawWindow();
}

void StateSetDepth::OnMouseMove  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isCutting || m_isPutting) {
		double dx = point.x - m_pointOld.x;
		double dy = point.y - m_pointOld.y;
		double dist = sqrt(dx * dx + dy * dy);
		KVector3d start, ori;
		core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
		if (m_isCutting) {
			if (dist < 20) return;
			m_pointOld = point;
			KVector3d pos(start);
			pos.addWeighted(ori, 0.5);
			core.m_cutStroke.push_back(pos);
			core.m_ogl.RedrawWindow();
		} else {
			if (dist < 50) return;
			m_pointOld = point;
			KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
			KVector3d pos;
			int polyID;
			if (KUtil::getIntersection(pos, polyID, poly, start, ori)) {
				vector<KVector3d>& points =
					m_drawMode == DRAWMODE_EPI  ? m_rbfPointsEpi  :
					m_drawMode == DRAWMODE_SEPT ? m_rbfPointsSept :
					m_drawMode == DRAWMODE_RV   ? m_rbfPointsRv   : m_rbfPointsLv;
				vector<bool>& visible =
					m_drawMode == DRAWMODE_EPI  ? m_isVisibleEpi  :
					m_drawMode == DRAWMODE_SEPT ? m_isVisibleSept :
					m_drawMode == DRAWMODE_RV   ? m_isVisibleRv   : m_isVisibleLv;
				points.push_back(pos);
				visible.push_back(true);
			}
			core.m_ogl.RedrawWindow();
		}
	}
}

void StateSetDepth::updateDepth() {
	Core& core = *Core::getInstance();
	core.m_rbfPoints.clear();
	core.m_rbfValues.clear();
	double value[4] = {RBFVALUE_EPI, RBFVALUE_SEPT, RBFVALUE_RV, RBFVALUE_LV};
	vector<KVector3d>* pPoints[4] = {&m_rbfPointsEpi, &m_rbfPointsSept, &m_rbfPointsRv, &m_rbfPointsLv};
	for (int i = 0; i < 4; ++i) {
		vector<KVector3d>& points = *pPoints[i];
		for (int j = 0; j < (int)points.size(); ++j) {
			core.m_rbfPoints.push_back(points[j]);
			core.m_rbfValues.push_back(value[i]);
		}
	}
	core.calcRbfDepth();
	core.calcVtxDepth();
	core.calcPolyLayers();
	core.calcPolyVtxDepth();
	core.calcPolyLayersVtxDepth();
}

void StateSetDepth::calcVisible(const std::vector<KVector3d>& cutStroke) {
	Core& core = *Core::getInstance();
	KThinPlate2D rbfCut;
	{
		// construct 2D-RBF height field by projecting cutStroke onto the screen
		vector<KVector2d> cutStroke2D;
		for (int i = 0; i < (int)cutStroke.size(); ++i) {
			const KVector3d& pos = cutStroke[i];
			double winx, winy, winz;
			core.m_ogl.project(pos.x, pos.y, pos.z, winx, winy, winz);
			cutStroke2D.push_back(KVector2d(winx, winy));
		}
		vector<KVector2d> points;
		vector<double   > values;
		for (int i = 1; i < (int)cutStroke2D.size(); ++i) {
			KVector2d& p0 = cutStroke2D[i - 1];
			KVector2d& p1 = cutStroke2D[i];
			KVector2d d(p1);
			d.sub(p0);
			d.normalize();
			d.scale(10);
			KVector2d cp(p0), cm(p0);
			cp.add(-d.y, d.x);
			cm.add(d.y, -d.x);
			points.push_back(cp);
			points.push_back(cm);
			values.push_back( 1);
			values.push_back(-1);
		}
		rbfCut.setPoints(points);
		rbfCut.setValues(values);
	}
	vector<KVector3d>* pPoints[4] = {&m_rbfPointsEpi, &m_rbfPointsSept, &m_rbfPointsRv, &m_rbfPointsLv};
	vector<bool>* pVisible[4] = {&m_isVisibleEpi, &m_isVisibleSept, &m_isVisibleRv, &m_isVisibleLv};
	for (int i = 0; i < 4; ++i) {
		vector<KVector3d>& points = *pPoints[i];
		vector<bool>& visible = *pVisible[i];
		visible.resize(points.size());
		for (int j = 0; j < (int)points.size(); ++j) {
			KVector3d& pos = points[j];
			double winx, winy, winz;
			core.m_ogl.project(pos.x, pos.y, pos.z, winx, winy, winz);
			double val = rbfCut.getValue(winx, winy);
			visible[j] = val < 0;
		}
	}
}

void StateSetDepth::initVisible() {
	vector<KVector3d>* pPoints[4] = {&m_rbfPointsEpi, &m_rbfPointsSept, &m_rbfPointsRv, &m_rbfPointsLv};
	vector<bool>* pVisible[4] = {&m_isVisibleEpi, &m_isVisibleSept, &m_isVisibleRv, &m_isVisibleLv};
	for (int i = 0; i < 4; ++i) {
		vector<KVector3d>& points = *pPoints[i];
		vector<bool>& visible = *pVisible[i];
		visible.clear();
		visible.resize(points.size(), true);
	}
}

bool StateSetDepth::load(const string& fname) {
	Core& core = *Core::getInstance();
	string ext = fname.substr(fname.size() - 6, 6);
	if (ext.compare(".depth")) {
		CString str;
		str.Format("Please drop *.depth file");
		AfxMessageBox(str);
		return false;
	}
	FILE * fin = fopen(fname.c_str(), "rb");
	if (!fin) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return false;
	}
	
	int numPointsEpi;
	fread(&numPointsEpi, sizeof(int), 1, fin);
	vector<KVector3d> pointsEpi(numPointsEpi);
	if (numPointsEpi > 0) fread(&pointsEpi[0], sizeof(KVector3d), numPointsEpi, fin);
	
	int numPointsSept;
	fread(&numPointsSept, sizeof(int), 1, fin);
	vector<KVector3d> pointsSept(numPointsSept);
	if (numPointsSept > 0) fread(&pointsSept[0], sizeof(KVector3d), numPointsSept, fin);
	
	int numPointsRv;
	fread(&numPointsRv, sizeof(int), 1, fin);
	vector<KVector3d> pointsRv(numPointsRv);
	if (numPointsRv > 0) fread(&pointsRv[0], sizeof(KVector3d), numPointsRv, fin);
	
	int numPointsLv;
	fread(&numPointsLv, sizeof(int), 1, fin);
	vector<KVector3d> pointsLv(numPointsLv);
	if (numPointsLv > 0) fread(&pointsLv[0], sizeof(KVector3d), numPointsLv, fin);
	
	fclose(fin);
	
	m_rbfPointsEpi  = pointsEpi;
	m_rbfPointsSept = pointsSept;
	m_rbfPointsRv   = pointsRv;
	m_rbfPointsLv   = pointsLv;
	updateDepth();
	initVisible();
	return true;
}

void StateSetDepth::undo() {
	vector<KVector3d>& points =
		m_drawMode == DRAWMODE_EPI  ? m_rbfPointsEpi  :
		m_drawMode == DRAWMODE_SEPT ? m_rbfPointsSept :
		m_drawMode == DRAWMODE_RV   ? m_rbfPointsRv   : m_rbfPointsLv;
	points.pop_back();
}

bool StateSetDepth::isUndoable() {
	vector<KVector3d>& points =
		m_drawMode == DRAWMODE_EPI  ? m_rbfPointsEpi  :
		m_drawMode == DRAWMODE_SEPT ? m_rbfPointsSept :
		m_drawMode == DRAWMODE_RV   ? m_rbfPointsRv   : m_rbfPointsLv;
	return !points.empty();
}

void StateSetDepth::save() {
	Core& core = *Core::getInstance();
	CFileDialog dialog(FALSE, ".depth", NULL, OFN_OVERWRITEPROMPT, "Depth files(*.depth)|*.depth||");
	if (dialog.DoModal() != IDOK) return;
	string fname = dialog.GetPathName();
	FILE * fout = fopen(fname.c_str(), "wb");
	if (!fout) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return;
	}
	int numPointsEpi = (int)m_rbfPointsEpi.size();
	fwrite(&numPointsEpi, sizeof(int), 1, fout);
	if (numPointsEpi > 0) fwrite(&m_rbfPointsEpi[0], sizeof(KVector3d), numPointsEpi, fout);
	
	int numPointsSept = (int)m_rbfPointsSept.size();
	fwrite(&numPointsSept, sizeof(int), 1, fout);
	if (numPointsSept > 0) fwrite(&m_rbfPointsSept[0], sizeof(KVector3d), numPointsSept, fout);
	
	int numPointsRv = (int)m_rbfPointsRv.size();
	fwrite(&numPointsRv, sizeof(int), 1, fout);
	if (numPointsRv > 0) fwrite(&m_rbfPointsRv[0], sizeof(KVector3d), numPointsRv, fout);
	
	int numPointsLv = (int)m_rbfPointsLv.size();
	fwrite(&numPointsLv, sizeof(int), 1, fout);
	if (numPointsLv > 0) fwrite(&m_rbfPointsLv[0], sizeof(KVector3d), numPointsLv, fout);
	fclose(fout);
}

bool StateSetDepth::isSavable() {
	return
		!m_rbfPointsEpi .empty() ||
		!m_rbfPointsSept.empty() ||
		!m_rbfPointsRv  .empty() ||
		!m_rbfPointsLv  .empty();
}

void StateSetDepth::OnKeyDown(CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) {
	Core& core = *Core::getInstance();
	switch (nChar) {
		case VK_SPACE:
			m_isDrawFace = !m_isDrawFace;
			Core::getInstance()->m_ogl.RedrawWindow();
			break;
		case VK_RETURN:
			m_isDrawPoint = !m_isDrawPoint;
			Core::getInstance()->m_ogl.RedrawWindow();
			break;
		case 'S':
		{
			FILE* fout = fopen(core.m_fname + ".depth", "wb");
			int numData = (int)core.m_rbfPoints.size();
			fwrite(&numData, sizeof(int), 1, fout);
			fwrite(&core.m_rbfPoints[0], sizeof(KVector3d), numData, fout);
			vector<double> values2(numData);
			for (int i = 0; i < numData; ++i) values2[i] = -core.m_rbfValues[i] / 2;
			fwrite(&values2[0], sizeof(double   ), numData, fout);
			fclose(fout);
		}
		break;
	}
}
