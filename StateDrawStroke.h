#pragma once

#include "State.h"
#include "Core.h"

class StateDrawStroke : public State
{	
	StateDrawStroke(void) {}
	~StateDrawStroke(void) {}
public:
	static StateDrawStroke* getInstance() {
		static StateDrawStroke p;
		return &p;
	}

	// interface of [State] begin
	State* next();
	bool isReady() {
		return !Core::getInstance()->m_volFiber.empty();
	}
	void init();
	void draw();
	void postDraw(CWnd* hWnd, CDC* pDC);
	void OnLButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnMouseMove  (CView* view, UINT nFlags, CPoint& point);
	void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags);
	bool load(const std::string& fname);
	void undo();
	bool isUndoable();
	void save();
	bool isSavable();
	// end
	
	void update();
	//bool m_useTexture;
	
protected:
	bool m_isCutting;
	bool m_isDrawing;
	int m_selectedVtx;
	CPoint m_pointOld;
};
