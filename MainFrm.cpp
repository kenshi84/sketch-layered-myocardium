// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MyoModeler.h"

#include "MainFrm.h"

#include "Core.h"
#include "StateLoadTetra.h"
#include "StateSetDepth.h"
#include "StateDrawStroke.h"
#include "StateDeformTetra.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_NEXT, &CMainFrame::OnFileNext)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEXT, &CMainFrame::OnUpdateFileNext)
	ON_COMMAND(ID_DEPTH_EPI, &CMainFrame::OnDepthEpi)
	ON_UPDATE_COMMAND_UI(ID_DEPTH_EPI, &CMainFrame::OnUpdateDepthEpi)
	ON_COMMAND(ID_DEPTH_LV, &CMainFrame::OnDepthLv)
	ON_UPDATE_COMMAND_UI(ID_DEPTH_LV, &CMainFrame::OnUpdateDepthLv)
	ON_COMMAND(ID_DEPTH_RV, &CMainFrame::OnDepthRv)
	ON_UPDATE_COMMAND_UI(ID_DEPTH_RV, &CMainFrame::OnUpdateDepthRv)
	ON_COMMAND(ID_DEPTH_SEPT, &CMainFrame::OnDepthSept)
	ON_UPDATE_COMMAND_UI(ID_DEPTH_SEPT, &CMainFrame::OnUpdateDepthSept)
	ON_COMMAND(ID_LAYER_IN, &CMainFrame::OnLayerIn)
	ON_UPDATE_COMMAND_UI(ID_LAYER_IN, &CMainFrame::OnUpdateLayerIn)
	ON_COMMAND(ID_LAYER_OUT, &CMainFrame::OnLayerOut)
	ON_UPDATE_COMMAND_UI(ID_LAYER_OUT, &CMainFrame::OnUpdateLayerOut)
	ON_COMMAND(ID_TOOLBAR_COMMON, &CMainFrame::OnToolbarCommon)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_COMMON, &CMainFrame::OnUpdateToolbarCommon)
	ON_COMMAND(ID_TOOLBAR_SETDEPTH, &CMainFrame::OnToolbarSetdepth)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_SETDEPTH, &CMainFrame::OnUpdateToolbarSetdepth)
	ON_COMMAND(ID_TOOLBAR_VIEWMODE, &CMainFrame::OnToolbarViewmode)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_VIEWMODE, &CMainFrame::OnUpdateToolbarViewmode)
	ON_COMMAND(ID_VIEWMODE_CUT, &CMainFrame::OnViewmodeCut)
	ON_UPDATE_COMMAND_UI(ID_VIEWMODE_CUT, &CMainFrame::OnUpdateViewmodeCut)
	ON_COMMAND(ID_VIEWMODE_LAYER, &CMainFrame::OnViewmodeLayer)
	ON_UPDATE_COMMAND_UI(ID_VIEWMODE_LAYER, &CMainFrame::OnUpdateViewmodeLayer)
	ON_COMMAND(ID_DEPTH_UPDATE, &CMainFrame::OnDepthUpdate)
	ON_UPDATE_COMMAND_UI(ID_DEPTH_UPDATE, &CMainFrame::OnUpdateDepthUpdate)
	ON_COMMAND(ID_EDIT_UNDO, &CMainFrame::OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, &CMainFrame::OnUpdateEditUndo)
	ON_COMMAND(ID_FIBER_UPDATE, &CMainFrame::OnFiberUpdate)
	ON_UPDATE_COMMAND_UI(ID_FIBER_UPDATE, &CMainFrame::OnUpdateFiberUpdate)
	ON_COMMAND(ID_FILE_HOME, &CMainFrame::OnFileHome)
	ON_COMMAND(ID_VIEWMODE_STREAMLINE, &CMainFrame::OnViewmodeStreamline)
	ON_UPDATE_COMMAND_UI(ID_VIEWMODE_STREAMLINE, &CMainFrame::OnUpdateViewmodeStreamline)
	ON_COMMAND(ID_FILE_EXPORT, &CMainFrame::OnFileExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_EXPORT, &CMainFrame::OnUpdateFileExport)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar_common.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_BOTTOM
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar_common.LoadToolBar(IDR_TOOLBAR_COMMON))
	{
		TRACE0("Failed to create toolbar_common\n");
		return -1;      // fail to create
	}
	if (!m_wndToolBar_setdepth.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar_setdepth.LoadToolBar(IDR_TOOLBAR_SETDEPTH))
	{
		TRACE0("Failed to create toolbar_setdepth\n");
		return -1;      // fail to create
	}
	if (!m_wndToolBar_drawstroke.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar_drawstroke.LoadToolBar(IDR_TOOLBAR_DRAWSTROKE))
	{
		TRACE0("Failed to create toolbar_drawstroke\n");
		return -1;      // fail to create
	}
	if (!m_wndToolBar_viewmode.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar_viewmode.LoadToolBar(IDR_TOOLBAR_VIEWMODE))
	{
		TRACE0("Failed to create toolbar_viewmode\n");
		return -1;      // fail to create
	}
	
	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar_common  .EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar_setdepth.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar_drawstroke.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar_viewmode.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar_common);
	DockControlBar(&m_wndToolBar_setdepth);
	DockControlBar(&m_wndToolBar_drawstroke);
	DockControlBar(&m_wndToolBar_viewmode);
	ShowControlBar(&m_wndToolBar_setdepth, false, true);
	ShowControlBar(&m_wndToolBar_drawstroke, false, true);
	ShowControlBar(&m_wndToolBar_setdepth, false, true);

  Core& core = *Core::getInstance();
  core.p_mainFrm = this;
  core.m_state->init();

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers




void CMainFrame::OnFileNext() {
	Core& core = *Core::getInstance();
	State* next = core.m_state->next();
	if (next) {
		core.m_state = next;
		core.m_state->init();
		core.m_ogl.RedrawWindow();
	}
}

void CMainFrame::OnUpdateFileNext(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state->isReady());
}


void CMainFrame::OnDepthEpi() {
	StateSetDepth::getInstance()->m_drawMode = StateSetDepth::DRAWMODE_EPI;
	Core::getInstance()->m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateDepthEpi(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state == StateSetDepth::getInstance());
	pCmdUI->SetCheck(StateSetDepth::getInstance()->m_drawMode == StateSetDepth::DRAWMODE_EPI);
}

void CMainFrame::OnDepthSept() {
	StateSetDepth::getInstance()->m_drawMode = StateSetDepth::DRAWMODE_SEPT;
	Core::getInstance()->m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateDepthSept(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state == StateSetDepth::getInstance());
	pCmdUI->SetCheck(StateSetDepth::getInstance()->m_drawMode == StateSetDepth::DRAWMODE_SEPT);
}

void CMainFrame::OnDepthRv() {
	StateSetDepth::getInstance()->m_drawMode = StateSetDepth::DRAWMODE_RV;
	Core::getInstance()->m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateDepthRv(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state == StateSetDepth::getInstance());
	pCmdUI->SetCheck(StateSetDepth::getInstance()->m_drawMode == StateSetDepth::DRAWMODE_RV);
}

void CMainFrame::OnDepthLv() {
	StateSetDepth::getInstance()->m_drawMode = StateSetDepth::DRAWMODE_LV;
	Core::getInstance()->m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateDepthLv(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state == StateSetDepth::getInstance());
	pCmdUI->SetCheck(StateSetDepth::getInstance()->m_drawMode == StateSetDepth::DRAWMODE_LV);
}

void CMainFrame::OnLayerIn() {
	Core& core = *Core::getInstance();
	++core.m_currentLayer;
	core.m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateLayerIn(CCmdUI *pCmdUI) {
	Core& core = *Core::getInstance();
	pCmdUI->Enable(
		core.m_viewMode == Core::VIEWMODE_LAYER &&
		core.m_currentLayer < Core::LAYER_MAX);
}

void CMainFrame::OnLayerOut() {
	Core& core = *Core::getInstance();
	--core.m_currentLayer;
	core.m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateLayerOut(CCmdUI *pCmdUI) {
	Core& core = *Core::getInstance();
	pCmdUI->Enable(
		core.m_viewMode == Core::VIEWMODE_LAYER &&
		core.m_currentLayer > 0);
}

void CMainFrame::OnToolbarCommon() {
	bool b = m_wndToolBar_common.IsWindowVisible() == TRUE;
	ShowControlBar(&m_wndToolBar_common, !b, true);
}

void CMainFrame::OnUpdateToolbarCommon(CCmdUI *pCmdUI) {
	pCmdUI->SetCheck(m_wndToolBar_common.IsWindowVisible());
}

void CMainFrame::OnToolbarSetdepth() {
	bool b = m_wndToolBar_setdepth.IsWindowVisible() == TRUE;
	ShowControlBar(&m_wndToolBar_setdepth, !b, true);
}

void CMainFrame::OnUpdateToolbarSetdepth(CCmdUI *pCmdUI) {
	pCmdUI->SetCheck(m_wndToolBar_setdepth.IsWindowVisible());
}

void CMainFrame::OnToolbarViewmode() {
	bool b = m_wndToolBar_viewmode.IsWindowVisible() == TRUE;
	ShowControlBar(&m_wndToolBar_viewmode, !b, true);
}

void CMainFrame::OnUpdateToolbarViewmode(CCmdUI *pCmdUI) {
	pCmdUI->SetCheck(m_wndToolBar_viewmode.IsWindowVisible());
}

void CMainFrame::OnViewmodeCut() {
	Core& core = *Core::getInstance();
	if (core.m_viewMode == Core::VIEWMODE_CUT) return;
	core.m_viewMode = Core::VIEWMODE_CUT;
	core.m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateViewmodeCut(CCmdUI *pCmdUI) {
	pCmdUI->SetCheck(Core::getInstance()->m_viewMode == Core::VIEWMODE_CUT);
}

void CMainFrame::OnViewmodeLayer() {
	Core& core = *Core::getInstance();
	if (core.m_viewMode == Core::VIEWMODE_LAYER) return;
	core.m_viewMode = Core::VIEWMODE_LAYER;
	core.m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateViewmodeLayer(CCmdUI *pCmdUI) {
	Core& core = *Core::getInstance();
	bool b = core.m_polyLayers.empty();
	pCmdUI->Enable(!b);
	if (b) return;
	pCmdUI->SetCheck(core.m_viewMode == Core::VIEWMODE_LAYER);
}


void CMainFrame::OnDepthUpdate() {
	StateSetDepth::getInstance()->updateDepth();
	Core::getInstance()->m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateDepthUpdate(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state == StateSetDepth::getInstance());
}

void CMainFrame::OnEditUndo() {
	Core& core = *Core::getInstance();
	core.m_state->undo();
	core.m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateEditUndo(CCmdUI *pCmdUI) {
	pCmdUI->Enable(Core::getInstance()->m_state->isUndoable());
}

void CMainFrame::OnFiberUpdate() {
	Core& core = *Core::getInstance();
	if (core.m_state == StateDrawStroke::getInstance())
		StateDrawStroke::getInstance()->update();
	else
		StateDeformTetra::getInstance()->updateFiber();
	Core::getInstance()->m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateFiberUpdate(CCmdUI *pCmdUI) {
	Core& core = *Core::getInstance();
	bool isAnyConstrained = false;
	for (auto& p : core.m_tetVtxConstraintDirection) {
		if (p.first) {
			isAnyConstrained = true;
			break;
		}
	}
	pCmdUI->Enable(
		core.m_state == StateDrawStroke::getInstance() && isAnyConstrained ||
		core.m_state == StateDeformTetra::getInstance());
}

void CMainFrame::OnViewmodeStreamline() {
	Core& core = *Core::getInstance();
	bool& b = core.m_showStreamLines;
	b = !b;
	core.m_ogl.RedrawWindow();
}

void CMainFrame::OnUpdateViewmodeStreamline(CCmdUI *pCmdUI) {
	Core& core = *Core::getInstance();
	pCmdUI->Enable(!core.m_streamLines.empty());
	pCmdUI->SetCheck(core.m_showStreamLines);
}

void CMainFrame::OnFileHome() {
	Core& core = *Core::getInstance();
	core.m_state = StateLoadTetra::getInstance();
	core.m_state->init();
	core.m_ogl.RedrawWindow();
}


void CMainFrame::OnFileExport() {
	Core::getInstance()->exportVolData();
}

void CMainFrame::OnUpdateFileExport(CCmdUI *pCmdUI) {
	//pCmdUI->Enable(Core::getInstance()->m_state == StateDeformTetra::getInstance());
	pCmdUI->Enable(Core::getInstance()->m_state == StateDrawStroke::getInstance()
		&& StateDrawStroke::getInstance()->isReady());
}
