// MainFrm.h : interface of the CMainFrame class
//


#pragma once

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:  // control bar embedded members
	CToolBar    m_wndToolBar_common;
	CToolBar    m_wndToolBar_viewmode;
	CToolBar    m_wndToolBar_setdepth;
	CToolBar    m_wndToolBar_drawstroke;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileNext();
public:
	afx_msg void OnUpdateFileNext(CCmdUI *pCmdUI);
public:
	afx_msg void OnDepthEpi();
public:
	afx_msg void OnUpdateDepthEpi(CCmdUI *pCmdUI);
public:
	afx_msg void OnDepthLv();
public:
	afx_msg void OnUpdateDepthLv(CCmdUI *pCmdUI);
public:
	afx_msg void OnDepthRv();
public:
	afx_msg void OnUpdateDepthRv(CCmdUI *pCmdUI);
public:
	afx_msg void OnDepthSept();
public:
	afx_msg void OnUpdateDepthSept(CCmdUI *pCmdUI);
public:
	afx_msg void OnLayerIn();
public:
	afx_msg void OnUpdateLayerIn(CCmdUI *pCmdUI);
public:
	afx_msg void OnLayerOut();
public:
	afx_msg void OnUpdateLayerOut(CCmdUI *pCmdUI);
public:
	afx_msg void OnToolbarCommon();
public:
	afx_msg void OnUpdateToolbarCommon(CCmdUI *pCmdUI);
public:
//	afx_msg void OnToolbarDepth();
public:
//	afx_msg void OnUpdateToolbarDepth(CCmdUI *pCmdUI);
public:
	afx_msg void OnToolbarViewmode();
public:
	afx_msg void OnUpdateToolbarViewmode(CCmdUI *pCmdUI);
public:
	afx_msg void OnViewmodeCut();
public:
	afx_msg void OnUpdateViewmodeCut(CCmdUI *pCmdUI);
public:
	afx_msg void OnViewmodeLayer();
public:
	afx_msg void OnUpdateViewmodeLayer(CCmdUI *pCmdUI);
public:
	afx_msg void OnToolbarSetdepth();
public:
	afx_msg void OnUpdateToolbarSetdepth(CCmdUI *pCmdUI);
public:
	afx_msg void OnDepthUpdate();
public:
	afx_msg void OnUpdateDepthUpdate(CCmdUI *pCmdUI);
public:
	afx_msg void OnEditUndo();
public:
	afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
public:
//	afx_msg void OnTextureOn();
public:
//	afx_msg void OnUpdateTextureOn(CCmdUI *pCmdUI);
public:
//	afx_msg void OnTextureUpdate();
public:
//	afx_msg void OnUpdateTextureUpdate(CCmdUI *pCmdUI);
public:
	afx_msg void OnFiberUpdate();
public:
	afx_msg void OnUpdateFiberUpdate(CCmdUI *pCmdUI);
public:
//	afx_msg void OnViewmodeFiber();
public:
//	afx_msg void OnUpdateViewmodeFiber(CCmdUI *pCmdUI);
public:
	afx_msg void OnFileHome();
public:
	afx_msg void OnViewmodeStreamline();
public:
	afx_msg void OnUpdateViewmodeStreamline(CCmdUI *pCmdUI);
public:
	afx_msg void OnFileExport();
public:
	afx_msg void OnUpdateFileExport(CCmdUI *pCmdUI);
};


