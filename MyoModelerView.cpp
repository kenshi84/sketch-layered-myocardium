// MyoModelerView.cpp : implementation of the CMyoModelerView class
//

#include "stdafx.h"
#include "MyoModeler.h"

#include "MyoModelerDoc.h"
#include "MyoModelerView.h"

#include "Core.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMyoModelerView

IMPLEMENT_DYNCREATE(CMyoModelerView, CView)

BEGIN_MESSAGE_MAP(CMyoModelerView, CView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_DROPFILES()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// CMyoModelerView construction/destruction

CMyoModelerView::CMyoModelerView()
{
	// TODO: add construction code here

}

CMyoModelerView::~CMyoModelerView()
{
}

BOOL CMyoModelerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMyoModelerView drawing

void CMyoModelerView::OnDraw(CDC* pDC)
{
	CMyoModelerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	Core::getInstance()->m_ogl.OnDraw_Begin();
	Core::getInstance()->m_drawer.draw();
	Core::getInstance()->m_ogl.OnDraw_End();
	Core::getInstance()->m_drawer.postDraw(this, pDC);
}


// CMyoModelerView diagnostics

#ifdef _DEBUG
void CMyoModelerView::AssertValid() const
{
	CView::AssertValid();
}

void CMyoModelerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMyoModelerDoc* CMyoModelerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMyoModelerDoc)));
	return (CMyoModelerDoc*)m_pDocument;
}
#endif //_DEBUG


// CMyoModelerView message handlers

int CMyoModelerView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	Core::getInstance()->m_ogl.OnCreate(this);
	Core::getInstance()->m_drawer.init();
	DragAcceptFiles();

	return 0;
}

void CMyoModelerView::OnDestroy()
{
	CView::OnDestroy();

	Core::getInstance()->m_ogl.OnDestroy();
}

void CMyoModelerView::OnDropFiles(HDROP hDropInfo)
{
	Core::getInstance()->m_handler.OnDropFiles(this, hDropInfo);

	CView::OnDropFiles(hDropInfo);
}

BOOL CMyoModelerView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;	//CView::OnEraseBkgnd(pDC);
}

void CMyoModelerView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	Core::getInstance()->m_ogl.OnSize(cx, cy);
}

void CMyoModelerView::OnLButtonDown(UINT nFlags, CPoint point)
{
	Core::getInstance()->m_handler.OnLButtonDown(this, nFlags, point);

	CView::OnLButtonDown(nFlags, point);
}

void CMyoModelerView::OnLButtonUp(UINT nFlags, CPoint point)
{
	Core::getInstance()->m_handler.OnLButtonUp(this, nFlags, point);

	CView::OnLButtonUp(nFlags, point);
}

void CMyoModelerView::OnRButtonDown(UINT nFlags, CPoint point)
{
	Core::getInstance()->m_handler.OnRButtonDown(this, nFlags, point);

	CView::OnRButtonDown(nFlags, point);
}

void CMyoModelerView::OnRButtonUp(UINT nFlags, CPoint point)
{
	Core::getInstance()->m_handler.OnRButtonUp(this, nFlags, point);

	CView::OnRButtonUp(nFlags, point);
}

void CMyoModelerView::OnMouseMove(UINT nFlags, CPoint point)
{
	Core::getInstance()->m_handler.OnMouseMove(this, nFlags, point);

	CView::OnMouseMove(nFlags, point);
}

void CMyoModelerView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	Core::getInstance()->m_handler.OnKeyDown(this, nChar, nRepCnt, nFlags);

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
