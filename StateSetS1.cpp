#include "StdAfx.h"
#include "StateSetS1.h"
#include "Drawer.h"
#include "MainFrm.h"
#include <KLIB/KUtil.h>
#include <KLIB/KDrawer.h>
#include "StateDeformTetra.h"

using namespace std;


const double StateSetS1::COLOR_POINTS [3] = {.8, .8, .2};

State* StateSetS1::next() {
	return StateDeformTetra::getInstance();
}

void StateSetS1::init() {
	Core& core = *Core::getInstance();
	core.m_s1Points.clear();
	m_isDrawFace = true;
}

void StateSetS1::draw() {
	Core& core = *Core::getInstance();
	KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
	if (m_isDrawFace) {
		glColor3dv(Drawer::COLOR_FACE);
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
			KPolygon& p = poly.m_polygons[i];
			for (int j = 0; j < 3; ++j) {
				KVector3d& n = p.m_normal[j];
				KVector3d& v = poly.m_vertices[p.m_vtx[j]].m_pos;
				glNormal3dv(n.getPtr());
				glVertex3dv(v.getPtr());
			}
		}
		glEnd();
	}
	glColor3dv(COLOR_POINTS);
	for (int j = 0; j < (int)core.m_s1Points.size(); ++j) {
		KDrawer::drawSphere(core.m_s1Points[j], 0.02);
	}
	if (!m_isDrawFace) {
		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glDisable(GL_LIGHTING);
		glColor4dv(Drawer::COLOR_CONTOUR);
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < (int)core.m_polyContour.m_polygons.size(); ++i) {
			KPolygon& p = core.m_polyContour.m_polygons[i];
			for (int j = 0; j < 3; ++j)
				glVertex3dv(core.m_polyContour.m_vertices[p.m_vtx[j]].m_pos.getPtr());
		}
		glEnd();
		glDisable(GL_BLEND);
	}
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	glColor3dv(Drawer::COLOR_CUTSTROKE);
	glLineWidth(5);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < (int)core.m_cutStroke.size(); ++i)
		glVertex3dv(core.m_cutStroke[i].getPtr());
	glEnd();
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}

void StateSetS1::postDraw(CWnd* hWnd, CDC* pDC) {
	Core& core = *Core::getInstance();
	CFont font;
	CFont *pOldFont;
	font.CreatePointFont ( 300, "Comic Sans MS", pDC );
	pOldFont = pDC->SelectObject ( &font );
	pDC->SetBkMode ( TRANSPARENT );
	pDC->SetTextColor ( RGB ( 255, 0, 0 ) );
	CRect rc;
	hWnd->GetClientRect ( &rc );
	pDC->DrawText ( "Put S1 points!", -1, &rc, DT_LEFT | DT_TOP | DT_SINGLELINE);
	pDC->SelectObject ( pOldFont );
}

void StateSetS1::OnLButtonDown(CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	KVector3d start, ori;
	core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
	KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
	KVector3d pos;
	int polyID;
	m_pointOld = point;
	if (KUtil::getIntersection(pos, polyID, poly, start, ori)) {
		core.m_s1Points.push_back(pos);
		m_isPutting = true;
	} else if (core.m_viewMode == Core::VIEWMODE_CUT) {
		start.addWeighted(ori, 0.5);
		core.m_cutStroke.push_back(start);
		m_isCutting = true;
		core.initPoly();
	}
	core.m_ogl.RedrawWindow();
}

void StateSetS1::OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	m_isPutting = false;
	if (!m_isCutting) return;
	m_isCutting = false;
	if (core.m_cutStroke.size() == 1) {
		core.m_cutStroke.clear();
		return;
	} else {
		core.calcPoly(core.m_cutStroke);
	}
	core.m_cutStroke.clear();
	core.m_ogl.RedrawWindow();
}

void StateSetS1::OnMouseMove  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isCutting || m_isPutting) {
		double dx = point.x - m_pointOld.x;
		double dy = point.y - m_pointOld.y;
		double dist = sqrt(dx * dx + dy * dy);
		KVector3d start, ori;
		core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
		if (m_isCutting) {
			if (dist < 20) return;
			m_pointOld = point;
			KVector3d pos(start);
			pos.addWeighted(ori, 0.5);
			core.m_cutStroke.push_back(pos);
			core.m_ogl.RedrawWindow();
		} else {
			if (dist < 50) return;
			m_pointOld = point;
			KPolygonModel& poly = core.m_viewMode == Core::VIEWMODE_CUT ? core.m_poly : core.m_polyLayers[core.m_currentLayer];
			KVector3d pos;
			int polyID;
			if (KUtil::getIntersection(pos, polyID, poly, start, ori)) {
				core.m_s1Points.push_back(pos);
				core.m_ogl.RedrawWindow();
			}
		}
	}
}

bool StateSetS1::load(const string& fname) {
	Core& core = *Core::getInstance();
	string ext = fname.substr(fname.size() - 3, 3);
	if (ext.compare(".s1")) {
		CString str;
		str.Format("Please drop *.s1 file");
		AfxMessageBox(str);
		return false;
	}
	FILE * fin = fopen(fname.c_str(), "rb");
	if (!fin) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return false;
	}
	
	int numPoints;
	fread(&numPoints, sizeof(int), 1, fin);
	vector<KVector3d> points(numPoints);
	fread(&points[0], sizeof(KVector3d), numPoints, fin);
	
	fclose(fin);
	
	core.m_s1Points = points;
	return true;
}

void StateSetS1::undo() {
	Core& core = *Core::getInstance();
	core.m_s1Points.pop_back();
}

bool StateSetS1::isUndoable() {
	Core& core = *Core::getInstance();
	return !core.m_s1Points.empty();
}

void StateSetS1::save() {
	Core& core = *Core::getInstance();
	CFileDialog dialog(FALSE, ".s1", NULL, OFN_OVERWRITEPROMPT, "S1 files(*.s1)|*.s1||");
	if (dialog.DoModal() != IDOK) return;
	string fname = dialog.GetPathName();
	FILE * fout = fopen(fname.c_str(), "wb");
	if (!fout) {
		CString str;
		str.Format("Failed to open file %s", fname.c_str());
		AfxMessageBox(str);
		return;
	}
	int numPoints = (int)core.m_s1Points.size();
	fwrite(&numPoints, sizeof(int), 1, fout);
	fwrite(&core.m_s1Points[0], sizeof(KVector3d), numPoints, fout);
	fclose(fout);
}

bool StateSetS1::isSavable() {
	Core& core = *Core::getInstance();
	return !core.m_s1Points.empty();
}

void StateSetS1::OnKeyDown(CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) {
	switch (nChar) {
		case VK_SPACE:
			m_isDrawFace = !m_isDrawFace;
			Core::getInstance()->m_ogl.RedrawWindow();
			break;
	}
}
