#include "StdAfx.h"
#include "StateLoadTetra.h"
#include "Drawer.h"
#include "StateDrawStroke.h"
#include "MainFrm.h"

#include <KLIB/KUtil.h>

using namespace std;

State* StateLoadTetra::next() {
	Core& core = *Core::getInstance();
	core.calcVolTetID();
	core.calcLaplacian();
	core.p_mainFrm->ShowControlBar(&core.p_mainFrm->m_wndToolBar_viewmode, true, true);
	return StateDrawStroke::getInstance();
}

void StateLoadTetra::init() {
	Core& core = *Core::getInstance();
	core.m_currentLayer = 0;
	core.m_polyLayers.clear();
	core.m_rbfDepth.clear();
	core.m_rbfPoints.clear();
	core.m_rbfValues.clear();
	core.m_tetra = KTetraModel();
	core.m_poly = KPolygonModel();
	core.m_viewMode = Core::VIEWMODE_CUT;
	core.m_showStreamLines = false;
	core.m_streamLines.clear();
	if (core.p_mainFrm != NULL) {
		core.p_mainFrm->ShowControlBar(&core.p_mainFrm->m_wndToolBar_common, true, true);
		core.p_mainFrm->ShowControlBar(&core.p_mainFrm->m_wndToolBar_setdepth, false, true);
		core.p_mainFrm->ShowControlBar(&core.p_mainFrm->m_wndToolBar_viewmode, false, true);
		core.p_mainFrm->ShowControlBar(&core.p_mainFrm->m_wndToolBar_drawstroke, false, true);
	}
	m_isDrawFace = true;
}

void StateLoadTetra::draw() {
	Core& core = *Core::getInstance();
	KPolygonModel& poly = core.m_poly;
	if (m_isDrawFace) {
		glColor3dv(Drawer::COLOR_FACE);
	} else {
		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glDisable(GL_LIGHTING);
		glColor4dv(Drawer::COLOR_CONTOUR);
	}
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < (int)poly.m_polygons.size(); ++i) {
		KPolygon& p = poly.m_polygons[i];
		for (int j = 0; j < 3; ++j) {
			KVector3d& n = p.m_normal[j];
			KVector3d& v = poly.m_vertices[p.m_vtx[j]].m_pos;
			glNormal3dv(n.getPtr());
			glVertex3dv(v.getPtr());
		}
	}
	glEnd();
	if (!m_isDrawFace) {
		glDisable(GL_BLEND);
	}
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	glColor3dv(Drawer::COLOR_CUTSTROKE);
	glLineWidth(5);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < (int)core.m_cutStroke.size(); ++i)
		glVertex3dv(core.m_cutStroke[i].getPtr());
	glEnd();
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}
void StateLoadTetra::postDraw(CWnd* hWnd, CDC* pDC) {
	Core& core = *Core::getInstance();
	if (!core.m_tetra.m_tetras.empty()) return;
	CFont font;
	CFont *pOldFont;
	font.CreatePointFont ( 300, "Comic Sans MS", pDC );
	pOldFont = pDC->SelectObject ( &font );
	pDC->SetBkMode ( TRANSPARENT );
	pDC->SetTextColor ( RGB ( 255, 0, 0 ) );
	CRect rc;
	hWnd->GetClientRect ( &rc );
	pDC->DrawText ( "Drop tetra model (*.ele) here!", -1, &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	pDC->SelectObject ( pOldFont );
}
void StateLoadTetra::OnLButtonDown(CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	KVector3d start, ori;
	if (!core.m_tetra.m_tetras.empty()) {
		core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
		KVector3d pos(start);
		pos.addWeighted(ori, 0.5);
		core.m_cutStroke.push_back(pos);
		m_pointOld = point;
		m_isCutting = true;
	}
}
void StateLoadTetra::OnLButtonUp  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (core.m_cutStroke.empty()) return;
	if (core.m_cutStroke.size() == 1) {
		core.initPoly();
	} else {
		core.calcPoly(core.m_cutStroke);
	}
	core.m_cutStroke.clear();
	core.m_ogl.RedrawWindow();
	m_isCutting = false;
}
void StateLoadTetra::OnMouseMove  (CView* view, UINT nFlags, CPoint& point) {
	Core& core = *Core::getInstance();
	if (m_isCutting) {
		double dx = point.x - m_pointOld.x;
		double dy = point.y - m_pointOld.y;
		double dist = sqrt(dx * dx + dy * dy);
		if (dist < 20) return;
		KVector3d start, ori;
		core.m_ogl.getScreenCoordToGlobalLine(point.x, point.y, start, ori);
		KVector3d pos(start);
		pos.addWeighted(ori, 0.5);
		core.m_cutStroke.push_back(pos);
		core.m_ogl.RedrawWindow();
	}
}

void StateLoadTetra::OnKeyDown(CView* view, UINT nChar, UINT nRepCnt, UINT nFlags) {
	Core& core = *Core::getInstance();
	switch (nChar) {
		case VK_SPACE:
			m_isDrawFace = !m_isDrawFace;
			Core::getInstance()->m_ogl.RedrawWindow();
			break;
	}
}

bool StateLoadTetra::load(const string& fname){
	Core& core = *Core::getInstance();
	string ext = fname.substr(fname.size() - 4, 4);
	if (ext.compare(".ele")) {
		CString str;
		str.Format("Please drop *.ele file");
		AfxMessageBox(str);
		return false;
	}
	core.m_fname = fname.substr(0, fname.size() - 4).c_str();
	if (!core.m_tetra.loadFile((LPCSTR)core.m_fname)) return false;
	KUtil::normalizeTetraModelIntoUnitBox(core.m_tetra, 3);
	core.m_tetra.calcNeighbor();
	core.initPoly();
	core.initPolyContour();
	core.calcTetVtxBoundary();
	core.initEyePosition();
	return true;
}
