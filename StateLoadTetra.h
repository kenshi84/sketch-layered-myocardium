#pragma once

#include "State.h"
#include "Core.h"

class StateLoadTetra : public State
{
	StateLoadTetra(void) {}
	~StateLoadTetra(void) {}
public:
	static StateLoadTetra* getInstance() {
		static StateLoadTetra p;
		return &p;
	}
	
	// interface of [State] begin
	State* next();
	bool isReady() {
		return !Core::getInstance()->m_tetra.m_tetras.empty();
	}
	void init();
	void draw();
	void postDraw(CWnd* hWnd, CDC* pDC);
	void OnLButtonDown(CView* view, UINT nFlags, CPoint& point);
	void OnLButtonUp  (CView* view, UINT nFlags, CPoint& point);
	void OnMouseMove  (CView* view, UINT nFlags, CPoint& point);
	void OnKeyDown    (CView* view, UINT nChar, UINT nRepCnt, UINT nFlags);
	bool load(const std::string& fname);
	void undo() {}
	bool isUndoable() { return false; }
	void save() {}
	bool isSavable() { return false; }
	// end

protected:
	bool m_isCutting;
	CPoint m_pointOld;
	bool m_isDrawFace;
};
